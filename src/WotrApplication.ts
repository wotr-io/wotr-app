import serverlessHttp from "serverless-http";
import { APIGatewayProxyEventV2, Context } from "aws-lambda";
import { findConfiguration } from "./domain/env/Environment";
import { buildApplicationContext } from "./integration/contexts/context";

const foundConfigMap = findConfiguration();
export const applicationContext = buildApplicationContext(foundConfigMap);

module.exports.handler = function (
  event: APIGatewayProxyEventV2,
  ctx: Context
) {
  if (!applicationContext.server) {
    throw new Error("Web server is disabled");
  }
  return serverlessHttp(applicationContext.server)(event, ctx);
};
