import { APIGatewayEvent } from "aws-lambda";
import { Request } from "express";
import { AuthorizationError } from "../domain/security/AuthorizationError";

export function findClaims(event: APIGatewayEvent): Claims {
  const { authorizer } = event.requestContext;
  if (!authorizer) {
    throw new Error("Required authorizer claims are missing.");
  }
  return authorizer.claims;
}

export function findTokenClaims(request: Request): Claims {
  const authorizationHeader = request.header("Authorization");
  if (!authorizationHeader) {
    throw new AuthorizationError("missing-authorization-header", []);
  }

  const [, payload] = authorizationHeader.split(".");

  const claimsString = Buffer.from(payload, "base64").toString("utf-8");
  const claims: { [key: string]: string } = JSON.parse(claimsString);

  return {
    aud: claims.aud,
    "custom:teams": claims["custom:teams"],
    "cognito:username": claims["cognito:username"],
    iss: claims.iss,
    email: claims.email,
    sub: claims.sub,
  };
}

export interface Claims {
  sub: string;
  aud: string;
  "custom:teams"?: string;
  iss: string;
  "cognito:username": string;
  email: string;
}
