import { Request, Response, NextFunction } from "express";
import { BusinessBoundaryError } from "../domain/errors/BusinessBoundaryError";
import { EntityNotFoundError } from "../domain/errors/EntityNotFoundError";
import { AuthorizationError } from "../domain/security/AuthorizationError";
import { ValidationError } from "../libs/validation/ValidationError";

export function internalServerErrorHandler(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (!err) {
    next();
  }
  console.error(err);
  res.status(500).json({
    code: "internal-server-error",
  });
}
export function endpointNotFoundFilter(req: Request, res: Response) {
  res.status(404).json({
    code: "endpoint-not-found",
  });
}

export function corsHeadersInterceptor(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.method === "OPTIONS") {
    return res.status(200).send();
  }

  res.set({
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "DELETE,GET,PATCH,POST,PUT",
    "Access-Control-Allow-Headers": "Content-Type,Authorization",
  });

  next();
}

export function globalErrorHandler(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (err instanceof AuthorizationError) {
    res.status(403).json({
      code: err.code,
      args: err.permissions,
    });
    return;
  }
  if (err instanceof BusinessBoundaryError) {
    res.status(422).json({
      code: err.code,
    });
    return;
  }
  if (err instanceof EntityNotFoundError) {
    const status = req.method === "GET" ? 404 : 422;
    res.status(status).json({
      code: "resource-not-found",
      resource: err.entity,
      id: err.identifier,
    });
    return;
  }
  if (err instanceof ValidationError) {
    res.status(400).json({
      code: "validation-error",
      errors: err.errors,
    });
    return;
  }

  next(err);
}
