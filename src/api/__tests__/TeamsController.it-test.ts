import { testContext } from "../../__tests__/test-context";
import supertest from "supertest";
import { someTeam } from "../../domain/teams/__tests__/team.fixtures";
import { Claims } from "../claims";
import { User } from "../../domain/users";
import { DetailedJsonTeam } from "../../domain/teams";
import nanoid from "nanoid";

const { usersRepository, teamsRepository } = testContext;
test("Test GET /teams", async () => {
  // given:
  const user = await usersRepository.save({
    username: "john.doe+" + nanoid(),
    avatarUrl: "https://avatars.com/john.doe/avatar",
    email: "john.doe@email.com",
  });
  const team = await teamsRepository.save({
    ...someTeam(),
    id: nanoid(),
  });

  // when:
  const response = await supertest(testContext.server)
    .get("/teams")
    .set("Authorization", toJwtToken(toClaims(user, team.id)));

  // then:
  const responseBody: DetailedJsonTeam[] = response.body;
  expect(response.status).toBe(200);
  expect(response.type).toBe("application/json");
  expect(responseBody.map((item) => item.id)).toEqual([team.id]);
});

test("Test POST /teams", async () => {
  // given:
  const user = await usersRepository.save({
    username: "john.doe+" + nanoid(),
    avatarUrl: "https://avatars.com/john.doe/avatar",
    email: "john.doe@email.com",
  });
  const team = await teamsRepository.save({
    ...someTeam(),
    id: nanoid(),
  });

  // when:
  const response = await supertest(testContext.server)
    .post("/teams")
    .set("Authorization", toJwtToken(toClaims(user, team.id)))
    .set("Content-Type", "application/json")
    .send({ name: "Team " + nanoid(3) });

  // then:
  const responseBody: DetailedJsonTeam = response.body;

  expect(response.status).toBe(201);
  expect(response.type).toBe("application/json");
  expect(responseBody.id).toBeDefined();

  const createdTeam = await teamsRepository.find(responseBody.id);
  expect(createdTeam.id).toEqual(responseBody.id);
});

function toClaims(user: User, ...teamIds: string[]): Claims {
  return {
    email: user.email,
    "cognito:username": user.username,
    aud: "aud",
    iss: "cognito-url",
    sub: "sub",
    "custom:teams": teamIds.join(","),
  };
}

function toJwtToken(claims: Claims): string {
  return [{}, claims, {}]
    .map((payload) => Buffer.from(JSON.stringify(payload)).toString("base64"))
    .join(".");
}
