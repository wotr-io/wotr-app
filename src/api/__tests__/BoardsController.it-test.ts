import { testContext } from "../../__tests__/test-context";
import supertest from "supertest";
import { Request } from "supertest";
import { someTeam } from "../../domain/teams/__tests__/team.fixtures";
import nanoid from "nanoid";
import { Claims } from "../claims";
import { User } from "../../domain/users";
import { Project } from "../../domain/projects";
import { someProject } from "../../domain/projects/__tests__/project.fixtures";
import { someTask } from "../../domain/tasks/__tests__/task.fixtures";
import { Task, TaskListingItem } from "../../domain/tasks";
import {
  someInProgressStatus,
  someTodoStatus,
} from "../../domain/tasks/boards/__tests__/board-definition.fixtures";
import { Board } from "../../domain/tasks/boards";

const {
  usersRepository,
  teamsRepository,
  teamPermissionsRepository,
  boardDefinitionsRepository,
  tasksRepository,
  boardRepository,
  projectsRepository,
} = testContext;

test("Test GET /teams/:teamId/boards/:boardId", async () => {
  // given:
  const user = await usersRepository.save({
    username: "john.doe+" + nanoid(),
    avatarUrl: "https://avatars.com/john.doe/avatar",
    email: "john.doe@email.com",
  });
  const team = await teamsRepository.save({
    ...someTeam(),
    id: nanoid(),
  });

  await teamPermissionsRepository.save({
    action: "*",
    resource: "*",
    teamId: team.id,
    username: user.username,
  });

  const project: Project = await projectsRepository.save({
    ...someProject(),
    id: nanoid(),
    teamId: team.id,
  });

  const task1: Task = await tasksRepository.save({
    ...someTask(team.id),
    id: "BLU-1",
    project,
    participants: [{ role: "author", user }],
  });
  const task2: Task = await tasksRepository.save({
    ...someTask(team.id),
    id: "BLU-2",
    project,
    participants: [{ role: "author", user }],
  });
  const task3: Task = await tasksRepository.save({
    ...someTask(team.id),
    id: "BLU-3",
    project,
    participants: [{ role: "author", user }],
  });

  const todoStatus = someTodoStatus();
  const inProgressStatus = someInProgressStatus();
  const boardDefinition = await boardDefinitionsRepository.save({
    id: nanoid(),
    author: user,
    name: "Some board",
    statuses: [todoStatus, inProgressStatus],
    teamId: team.id,
  });

  const board: Board = await boardRepository.save(boardDefinition, {
    id: nanoid(),
    name: "Some board",
    teamId: team.id,
    columns: [
      {
        status: todoStatus,
        tasks: [toTaskEntry(task1), toTaskEntry(task2)],
        version: nanoid(),
      },
      {
        status: inProgressStatus,
        tasks: [toTaskEntry(task3)],
        version: nanoid(),
      },
    ],
    version: nanoid(),
  });

  // when:
  const response = await supertest(testContext.server)
    .get("/teams/" + team.id + "/boards/" + board.id)
    .set("Authorization", toJwtToken(toClaims(user, team.id)))
    .use(printCalls());

  // then:
  expect(response.status).toBe(200);
  expect(response.type).toBe("application/json");
  expect(response.body).toEqual(board);
});

function printCalls() {
  return (req: Request) => {
    const endpointId = req.method + " " + req.url;
    const body = req.get("body");
    console.log(
      ["Making request to: " + endpointId, "Body: ", body].join("\n")
    );
    req.on("response", (response) => {
      console.log(
        [
          "Received response from: " + endpointId,
          "Status: " + response.status,
          "Body: ",
          JSON.stringify(response.body, undefined, 2),
        ].join("\n")
      );
    });
  };
}

function toClaims(user: User, ...teamIds: string[]): Claims {
  return {
    email: user.email,
    "cognito:username": user.username,
    aud: "aud",
    iss: "cognito-url",
    sub: "sub",
    "custom:teams": teamIds.join(","),
  };
}

function toJwtToken(claims: Claims): string {
  return [{}, claims, {}]
    .map((payload) => Buffer.from(JSON.stringify(payload)).toString("base64"))
    .join(".");
}

export function toTaskEntry(task: Task): TaskListingItem {
  return {
    id: task.id,
    type: task.type,
    createdAt: task.createdAt,
    participants: task.participants,
    summary: task.summary,
    resolved: false,
  };
}
