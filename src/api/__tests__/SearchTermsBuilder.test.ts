import { SearchTermsBuilder } from "../../api/SearchTermsBuilder";
import { ListTasksParameters } from "../../domain/tasks";

test("Test build empty search terms structure", () => {
  // given:
  const queryParams = {};

  // when:
  const listingParameters = new SearchTermsBuilder<ListTasksParameters>(
    queryParams
  )
    .stringTerm("phrase")
    .stringTerm("summary")
    .stringTerm("content")
    .stringTerm("type")
    .stringTerm("createdAt.gte")
    .stringTerm("createdAt.lte")
    .build();

  // then:
  expect(listingParameters).toBe(null);
});

test("Test build search terms structure", () => {
  // given:
  const queryParams = {
    phrase: "abc",
    type: "bug",
    "unknown-prop": "fake",
    content: "boom",
  };

  // when:
  const listingParameters = new SearchTermsBuilder<ListTasksParameters>(
    queryParams
  )
    .stringTerm("phrase")
    .stringTerm("summary")
    .stringTerm("content")
    .stringTerm("type")
    .stringTerm("createdAt.gte")
    .stringTerm("createdAt.lte")
    .build();

  // then:
  expect(listingParameters).toEqual({
    phrase: "abc",
    type: "bug",
    content: "boom",
  });
});
