import { Express } from "express";
import { findTokenClaims } from "./claims";
import {
  CreateBoardDefinitionRequest,
  CreateBoardRequest,
  JsonTaskTransition,
} from "../domain/tasks/boards";
import { asyncHandler } from "../libs/express/async-handler";
import { validatePayload } from "../libs/validation";
import { ExpressController } from "../libs/express";
import { TeamsAuthorizer } from "./auth/TeamsAuthorizer";
import { BoardDefinitionService } from "../domain/tasks/boards/BoardDefinitionService";
import { BoardService } from "../domain/tasks/boards/BoardService";
import {
  CreateBoardDefinitionRequestSchema,
  CreateBoardRequestSchema,
  MoveTaskRequestSchema,
} from "./schemas/BoardSchemas";

export class BoardsController implements ExpressController {
  constructor(
    private readonly teamsAuthorizer: TeamsAuthorizer,
    private readonly boardDefinitionService: BoardDefinitionService,
    private readonly boardService: BoardService
  ) {}

  public registerEndpoints(express: Express) {
    express.post<{ teamId: string }>(
      "/teams/:teamId/board-definitions",
      asyncHandler(async (req, res) => {
        const { teamId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const payload: CreateBoardDefinitionRequest =
          validateCreateBoardDefinitionRequest(req.body);

        const createdTask =
          await this.boardDefinitionService.createBoardDefinition(
            teamId,
            { name: payload.name },
            payload.statuses,
            user
          );
        return res.status(201).json(createdTask);
      })
    );

    express.get<{ teamId: string; boardDefinitionId: string }>(
      "/teams/:teamId/board-definitions/:boardDefinitionId",
      asyncHandler(async (req, res) => {
        const { teamId, boardDefinitionId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const boardDefinition =
          await this.boardDefinitionService.readBoardDefinition(
            teamId,
            boardDefinitionId,
            user
          );
        return res.status(200).json(boardDefinition);
      })
    );

    express.post<{ teamId: string; boardDefinitionId: string }>(
      "/teams/:teamId/board-definitions/:boardDefinitionId/boards",
      asyncHandler(async (req, res) => {
        const { teamId, boardDefinitionId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const payload: CreateBoardRequest = validateCreateBoardRequest(
          req.body
        );

        const createdTask = await this.boardService.createBoard(
          teamId,
          boardDefinitionId,
          { name: payload.name },
          payload.taskIds,
          user
        );
        return res.status(201).json(createdTask);
      })
    );

    express.get<{ teamId: string; boardId: string }>(
      "/teams/:teamId/boards/:boardId",
      asyncHandler(async (req, res) => {
        const { teamId, boardId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const board = await this.boardService.readBoard(teamId, boardId, user);
        return res.status(200).json(board);
      })
    );

    express.post<{ teamId: string; boardId: string }>(
      "/teams/:teamId/boards/:boardId/task-transitions",
      asyncHandler(async (req, res) => {
        const { teamId, boardId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const payload: JsonTaskTransition = validateJsonTaskTransition(
          req.body
        );

        const doneTransition = await this.boardService.transitionTask(
          teamId,
          boardId,
          payload,
          user
        );
        return res.status(200).json(doneTransition);
      })
    );

    express.delete<{ teamId: string; boardId: string }>(
      "/teams/:teamId/boards/:boardId",
      asyncHandler(async (req, res) => {
        const { teamId, boardId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        await this.boardService.deleteBoard(teamId, boardId, user);
        return res.status(204);
      })
    );

    express.get(
      "/teams/:teamId/boards",
      asyncHandler(async (req, res) => {
        const { teamId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const boardsListing = await this.boardService.listBoards(teamId, user);
        return res.status(200).json(boardsListing);
      })
    );

    express.get(
      "/teams/:teamId/board-definitions",
      asyncHandler(async (req, res) => {
        const { teamId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const boardDefinitionsListing =
          await this.boardDefinitionService.listBoardDefinitions(teamId, user);
        return res.status(200).json(boardDefinitionsListing);
      })
    );
  }
}

function validateCreateBoardDefinitionRequest(
  payload: CreateBoardDefinitionRequest
) {
  return validatePayload(payload, CreateBoardDefinitionRequestSchema);
}

function validateCreateBoardRequest(payload: CreateBoardRequest) {
  return validatePayload(payload, CreateBoardRequestSchema);
}

function validateJsonTaskTransition(payload: JsonTaskTransition) {
  return validatePayload(payload, MoveTaskRequestSchema);
}
