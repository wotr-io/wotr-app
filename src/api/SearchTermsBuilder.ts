type Term<U> = {
  param: keyof U;
  value: string | number | boolean;
};

export class SearchTermsBuilder<U> {
  private readonly queryParams: { [key: string]: string };
  private readonly terms: Array<Term<U>> = [];

  public constructor(queryParams: { [key: string]: string } | null) {
    this.queryParams = queryParams || {};
  }

  public numberTerm(param: keyof U): SearchTermsBuilder<U> {
    const value = this.queryParams[param as string];
    if (!value) {
      return this;
    }
    const numberValue = parseInt(value, 10);
    if (isNaN(numberValue)) {
      this.terms.push({ param, value });
      return this;
    }
    this.terms.push({ param, value: numberValue });
    return this;
  }

  public stringTerm(param: keyof U): SearchTermsBuilder<U> {
    const value = this.queryParams[param as string];
    if (!value) {
      return this;
    }
    this.terms.push({ param, value });
    return this;
  }

  public build(): U | null {
    if (this.terms.length === 0) {
      return null;
    }
    const reduced: any = this.terms.reduce((left, right) => {
      left[right.param] = right.value;
      return left;
    }, {} as { [key in keyof U]: string | number | boolean });

    return reduced;
  }
}
