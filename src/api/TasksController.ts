import { findTokenClaims } from "./claims";
import { ListTasksParameters } from "../domain/tasks";
import { validatePayload } from "../libs/validation";
import { SearchTermsBuilder } from "../api/SearchTermsBuilder";
import { Express } from "express";
import { ExpressController } from "../libs/express";
import { TeamsAuthorizer } from "./auth/TeamsAuthorizer";
import { TasksService } from "../domain/tasks/TasksService";
import { asyncHandler } from "../libs/express/async-handler";
import {
  CreateTaskRequest,
  CreateTaskRequestSchema,
  ListTasksParametersSchema,
  UpdateTaskRequest,
  UpdateTaskRequestSchema,
} from "./schemas/TaskSchemas";

export class TasksController implements ExpressController {
  constructor(
    private readonly teamsAuthorizer: TeamsAuthorizer,
    private readonly tasksService: TasksService
  ) {}

  public registerEndpoints(express: Express) {
    express.get(
      "/teams/:teamId/tasks/:taskId",
      asyncHandler<{
        teamId: string;
        taskId: string;
      }>(async (req, res) => {
        const { teamId, taskId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const task = await this.tasksService.findTaskById(teamId, taskId, user);
        return res.status(200).json(task);
      })
    );

    express.get<{ teamId: string }>(
      "/teams/:teamId/recent-tasks-dashboard",
      asyncHandler(async (req, res) => {
        const { teamId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const task = await this.tasksService.readRecentTasksDashboard(
          teamId,
          user
        );
        return res.status(200).json(task);
      })
    );

    express.get<{ teamId: string }>(
      "/teams/:teamId/tasks",
      asyncHandler<{ teamId: string }>(async (req, res) => {
        const { teamId } = req.params;
        const queryParams = req.query as { [key: string]: string };
        const listingParameters = new SearchTermsBuilder<ListTasksParameters>(
          queryParams
        )
          .stringTerm("phrase")
          .stringTerm("summary")
          .stringTerm("project")
          .stringTerm("content")
          .stringTerm("author")
          .stringTerm("type")
          .stringTerm("createdAt.gte")
          .stringTerm("createdAt.lte")
          .build();

        if (!!listingParameters) {
          validateParameters(listingParameters);
        }

        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const listing = await this.tasksService.listTasks(
          teamId,
          listingParameters,
          user
        );
        return res.status(200).json(listing);
      })
    );

    express.post<{
      teamId: string;
      projectId: string;
    }>(
      "/teams/:teamId/projects/:projectId/tasks",
      asyncHandler(async (req, res) => {
        const { teamId, projectId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );

        const payload = validateCreateTaskRequest(req.body);

        const createdTask = await this.tasksService.saveTask(
          teamId,
          projectId,
          payload,
          user
        );
        return res.status(201).json(createdTask);
      })
    );
    express.put<{
      teamId: string;
      taskId: string;
    }>(
      "/teams/:teamId/tasks/:taskId",
      asyncHandler(async (req, res) => {
        const { teamId, taskId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );

        const payload = validateUpdateTaskRequest(req.body);

        const updatedTask = await this.tasksService.updateTask(
          teamId,
          taskId,
          payload,
          user
        );
        return res.status(200).json(updatedTask);
      })
    );
  }
}

function validateCreateTaskRequest(request: CreateTaskRequest) {
  return validatePayload(request, CreateTaskRequestSchema);
}

function validateUpdateTaskRequest(request: UpdateTaskRequest) {
  return validatePayload(request, UpdateTaskRequestSchema);
}

function validateParameters(listingParameters: ListTasksParameters) {
  return validatePayload(listingParameters, ListTasksParametersSchema);
}
