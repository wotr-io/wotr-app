import { Express } from "express";
import { findTokenClaims } from "./claims";
import { asyncHandler } from "../libs/express/async-handler";
import { JsonTeam } from "../domain/teams";
import { validatePayload } from "../libs/validation";
import { ExpressController } from "../libs/express";
import { TeamsAuthorizer } from "./auth/TeamsAuthorizer";
import { TeamsService } from "../domain/teams/TeamsService";
import { TeamMembersService } from "../domain/teams/members/TeamMembersService";
import { CreateTeamRequestSchema } from "./schemas/TeamSchemas";

export class TeamsController implements ExpressController {
  constructor(
    private readonly teamsAuthorizer: TeamsAuthorizer,
    private readonly teamsService: TeamsService,
    private readonly teamMembersService: TeamMembersService
  ) {}

  public registerEndpoints(express: Express) {
    express.get(
      "/teams",
      asyncHandler(async (req, res) => {
        const user = this.teamsAuthorizer.toAuthenticatedUser(
          findTokenClaims(req)
        );
        const teams = await this.teamsService.listTeams(user);
        return res.status(200).json(teams);
      })
    );

    express.get(
      "/teams/:teamId",
      asyncHandler<{ teamId: string }>(async (req, res) => {
        const teamId = req.params.teamId;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const team = await this.teamsService.findTeam(teamId, user);
        return res.status(200).json(team);
      })
    );

    express.post(
      "/teams",
      asyncHandler(async (req, res) => {
        const user = this.teamsAuthorizer.toAuthenticatedUser(
          findTokenClaims(req)
        );
        const jsonTeam = validateCreateTeamRequest(req.body);
        const createdTeam = await this.teamsService.createTeam(jsonTeam, user);
        return res.status(201).json(createdTeam);
      })
    );

    express.get(
      "/teams/:teamId/members",
      asyncHandler<{ teamId: string }>(async (req, res) => {
        const teamId = req.params.teamId;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const team = await this.teamMembersService.listTeamMembers(
          teamId,
          user
        );
        return res.status(200).json(team);
      })
    );
  }
}

function validateCreateTeamRequest(team: JsonTeam) {
  return validatePayload(team, CreateTeamRequestSchema);
}
