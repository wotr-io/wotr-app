import { AuthorizationError } from "../../domain/security/AuthorizationError";
import { WotrUser } from "../../domain/security/WotrUser";
import { TeamPermissionRepository } from "../../domain/teams/permissions/TeamPermissionRepository";
import { Claims } from "../claims";

export class TeamsAuthorizer {
  constructor(
    private readonly teamPermissionsRepository: TeamPermissionRepository
  ) {}

  public toAuthenticatedUser(claims: Claims): WotrUser {
    const user: WotrUser = {
      sub: claims.sub,
      username: claims["cognito:username"],
      email: claims.email,
      teams: asTeams(claims["custom:teams"]),
      permissions: [],
    };
    return user;
  }

  public async toAuthorizedUser(
    claims: Claims,
    teamId: string
  ): Promise<WotrUser> {
    const teamAssignments = claims["custom:teams"]?.split(",") || [];

    if (!teamAssignments.includes(teamId)) {
      throw new AuthorizationError("missing-team-assignment", [teamId]);
    }
    const permissions =
      await this.teamPermissionsRepository.findByTeamIdAndUsername(
        teamId,
        claims["cognito:username"]
      );

    if (permissions.length === 0) {
      return Promise.reject(
        new AuthorizationError("missing-team-assignment", [teamId])
      );
    }

    const user: WotrUser = {
      sub: claims.sub,
      username: claims["cognito:username"],
      email: claims.email,
      teams: asTeams(claims["custom:teams"]),
      permissions,
    };
    return Promise.resolve(user);
  }
}

function asTeams(rawTeams: string | undefined | null): string[] {
  if (!rawTeams) {
    return [];
  }
  return rawTeams
    .split(",")
    .map((team) => team.trim())
    .filter((team) => !!team);
}
