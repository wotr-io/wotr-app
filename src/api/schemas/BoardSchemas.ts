import { JSONSchemaType } from "ajv";
import {
  JsonBoard,
  JsonBoardDefinition,
  JsonTaskStatusDefinition,
  JsonTaskTransition,
} from "../../domain/tasks/boards";

interface CreateBoardRequest extends JsonBoard {
  taskIds: string[];
}

interface CreateBoardDefinitionRequest extends JsonBoardDefinition {
  statuses: JsonTaskStatusDefinition[];
}

interface MoveTaskRequest extends JsonTaskTransition {}

export const CreateBoardDefinitionRequestSchema: JSONSchemaType<CreateBoardDefinitionRequest> =
  {
    type: "object",
    required: ["name", "statuses"],
    properties: {
      name: {
        type: "string",
        minLength: 1,
        maxLength: 50,
      },
      statuses: {
        type: "array",
        items: {
          type: "object",
          required: ["name", "color", "resolving"],
          properties: {
            name: {
              type: "string",
              minLength: 1,
              maxLength: 20,
            },
            resolving: {
              type: "boolean",
            },
            color: {
              type: "string",
              enum: ["grey-1", "green-1", "red-1", "yellow-1", "blue-1"],
            },
          },
        },
      },
    },
    additionalProperties: false,
  };

export const MoveTaskRequestSchema: JSONSchemaType<MoveTaskRequest> = {
  type: "object",
  required: ["taskId", "index", "sourceStatusId", "targetStatusId"],
  properties: {
    taskId: {
      type: "string",
      minLength: 1,
      maxLength: 10,
    },
    index: {
      type: "integer",
      minimum: 0,
      maximum: 100,
    },
    sourceStatusId: {
      type: "string",
    },
    targetStatusId: {
      type: "string",
    },
  },
  additionalProperties: false,
};

export const CreateBoardRequestSchema: JSONSchemaType<CreateBoardRequest> = {
  type: "object",
  required: ["name", "taskIds"],
  properties: {
    name: {
      type: "string",
      minLength: 1,
      maxLength: 50,
    },
    taskIds: {
      type: "array",
      items: {
        type: "string",
      },
    },
  },
  additionalProperties: false,
};
