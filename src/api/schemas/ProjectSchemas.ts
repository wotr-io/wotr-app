import { JSONSchemaType } from "ajv";
import { JsonProject } from "../../domain/projects";

export interface CreateProjectRequest extends JsonProject {}

export const CreateProjectRequestSchema: JSONSchemaType<CreateProjectRequest> =
  {
    type: "object",
    required: ["name", "slug"],
    properties: {
      name: {
        type: "string",
        minLength: 1,
        maxLength: 50,
        pattern: "^[A-Za-z0-9][\\w\\s]*[A-Za-z0-9]$",
      },
      slug: {
        type: "string",
        minLength: 2,
        maxLength: 4,
        pattern: "^[A-Z]{2,4}$",
      },
    },
    additionalProperties: false,
  };
