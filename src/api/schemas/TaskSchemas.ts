import { JSONSchemaType } from "ajv";
import { ListTasksParameters, TaskInput } from "../../domain/tasks";

export interface CreateTaskRequest extends TaskInput {}
export interface UpdateTaskRequest extends TaskInput {}

const TaskInputSchema: JSONSchemaType<TaskInput> = {
  type: "object",
  required: ["content", "summary", "type"],
  properties: {
    summary: {
      type: "string",
      minLength: 1,
      maxLength: 150,
    },
    content: {
      type: "string",
      minLength: 1,
      maxLength: 2048,
    },
    type: {
      type: "string",
      enum: ["user-story", "bug", "task"],
    },
  },
};

export const CreateTaskRequestSchema: JSONSchemaType<CreateTaskRequest> = {
  type: "object",
  required: TaskInputSchema.required,
  properties: {
    type: TaskInputSchema.properties!.type,
    summary: TaskInputSchema.properties!.summary,
    content: TaskInputSchema.properties!.content,
  },
};

export const UpdateTaskRequestSchema: JSONSchemaType<UpdateTaskRequest> = {
  type: "object",
  required: TaskInputSchema.required,
  properties: {
    type: TaskInputSchema.properties!.type,
    summary: TaskInputSchema.properties!.summary,
    content: TaskInputSchema.properties!.content,
  },
};

export const ListTasksParametersSchema: JSONSchemaType<ListTasksParameters> = {
  type: "object",
  required: [],
  properties: {
    phrase: {
      type: "string",
      minLength: 3,
      maxLength: 30,
      nullable: true,
    },
    summary: {
      type: "string",
      minLength: 3,
      maxLength: 30,
      nullable: true,
    },
    content: {
      type: "string",
      minLength: 3,
      maxLength: 30,
      nullable: true,
    },
    author: {
      type: "string",
      minLength: 3,
      maxLength: 30,
      nullable: true,
    },
    project: {
      type: "string",
      minLength: 2,
      maxLength: 10,
      nullable: true,
    },
    type: {
      type: "string",
      enum: ["user-story", "bug", "task"],
      nullable: true,
    },
    "createdAt.gte": {
      type: "string",
      format: "date-time",
      nullable: true,
    },
    "createdAt.lte": {
      type: "string",
      format: "date-time",
      nullable: true,
    },
  },
  additionalProperties: false,
};
