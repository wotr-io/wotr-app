import { JSONSchemaType } from "ajv";
import { JsonTeam } from "../../domain/teams";

export interface CreateTeamRequest extends JsonTeam {}

export const CreateTeamRequestSchema: JSONSchemaType<CreateTeamRequest> = {
  type: "object",
  required: ["name"],
  properties: {
    name: {
      type: "string",
      minLength: 1,
      maxLength: 50,
      pattern: "^[A-Za-z0-9][\\w\\s]*[A-Za-z0-9]$",
    },
  },
  additionalProperties: false,
};
