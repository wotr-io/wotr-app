import { Express } from "express";
import { JsonProject } from "../domain/projects";
import { asyncHandler } from "../libs/express/async-handler";
import { findTokenClaims } from "./claims";
import { validatePayload } from "../libs/validation";
import { ExpressController } from "../libs/express";
import { TeamsAuthorizer } from "./auth/TeamsAuthorizer";
import { ProjectsService } from "../domain/projects/ProjectsService";
import { CreateProjectRequestSchema } from "./schemas/ProjectSchemas";

export class ProjectsController implements ExpressController {
  constructor(
    private readonly teamsAuthorizer: TeamsAuthorizer,
    private readonly projectsService: ProjectsService
  ) {}

  public registerEndpoints(express: Express) {
    express.get(
      "/teams/:teamId/projects",
      asyncHandler<{ teamId: string }>(async (req, res) => {
        const { teamId } = req.params;
        await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const projects = await this.projectsService.listProjects(teamId);
        return res.status(200).json(projects);
      })
    );

    express.get(
      "/teams/:teamId/project-stats",
      asyncHandler<{ teamId: string }>(async (req, res) => {
        const { teamId } = req.params;
        await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const projectStatsListing =
          await this.projectsService.listProjectStatistics(teamId);
        return res.status(200).json(projectStatsListing);
      })
    );

    express.get(
      "/teams/:teamId/projects/:projectId",
      asyncHandler<{ teamId: string; projectId: string }>(async (req, res) => {
        const { teamId, projectId } = req.params;
        await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const project = await this.projectsService.findProject(
          teamId,
          projectId
        );
        return res.status(200).json(project);
      })
    );

    express.get(
      "/teams/:teamId/projects/:projectId/project-stats",
      asyncHandler<{ teamId: string; projectId: string }>(async (req, res) => {
        const { teamId, projectId } = req.params;
        await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );
        const projectStats = await this.projectsService.findProjectStats(
          teamId,
          projectId
        );
        return res.status(200).json(projectStats);
      })
    );

    express.post(
      "/teams/:teamId/projects",
      asyncHandler<{ teamId: string }>(async (req, res) => {
        const { teamId } = req.params;
        const user = await this.teamsAuthorizer.toAuthorizedUser(
          findTokenClaims(req),
          teamId
        );

        const jsonProject = validateCreateProjectRequest(req.body);
        const project = await this.projectsService.saveProject(
          teamId,
          jsonProject,
          user
        );
        return res.status(201).json(project);
      })
    );
  }
}

function validateCreateProjectRequest(project: JsonProject) {
  return validatePayload(project, CreateProjectRequestSchema);
}
