import { buildApplicationContext } from "../integration/contexts/context";
import { findConfiguration } from "../domain/env/Environment";
const configMap = findConfiguration("test");

export const testContext = (() => {
  console.debug(
    "Using tests configuration: " + JSON.stringify(configMap, undefined, 2)
  );
  const prodContext = buildApplicationContext(configMap);
  return { ...prodContext } as typeof prodContext;
})();
