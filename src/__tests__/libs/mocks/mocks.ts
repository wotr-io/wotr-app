import tsMocks = require("ts-mockito");

export interface Mock<T> {
  mock: T;
  instance: T;
}
export function mock<T>(clazz: new (...args: any[]) => T): Mock<T> {
  const objectMock = tsMocks.mock(clazz);
  const instance = tsMocks.instance(objectMock);
  return { mock: objectMock, instance };
}

export function spy<T>(obj: T): Mock<T> {
  const objectMock = tsMocks.spy(obj);
  const instance = tsMocks.instance(objectMock);
  return { mock: objectMock, instance };
}
