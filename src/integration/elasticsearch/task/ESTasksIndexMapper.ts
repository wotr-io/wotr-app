import { TaskIndexEntry } from "./index";
import { Task, TaskListingItem, TaskParticipant } from "../../../domain/tasks";
import { parseISO } from "date-fns";
import { User } from "../../../domain/users";
import { toTaskGid } from "../../persistence/task/DDBTaskRepository";

export function toTaskEntries(
  entries: TaskIndexEntry[],
  participants: User[]
): TaskListingItem[] {
  return entries.map((entry) => asTaskEntry(entry, participants));
}

export function toTaskEntry(
  entry: TaskIndexEntry,
  participants: User[]
): TaskListingItem {
  return asTaskEntry(entry, participants);
}

export function toIndexEntry(task: Task): TaskIndexEntry {
  return {
    gid: toTaskGid(task.project.teamId, task.id),
    resource: "task",
    task: {
      id: task.id,
      participants: task.participants.map((participant) => ({
        role: participant.role,
        username: participant.user.username,
      })),
      createdAt: parseISO(task.createdAt).getTime(),
      content: task.content,
      summary: task.summary,
      fts_content: task.content,
      fts_summary: task.summary,
      type: task.type,
      project: {
        id: task.project.id,
        slug: task.project.slug,
      },
      resolved: task.resolved,
      team: {
        id: task.project.teamId,
      },
    },
  };
}

function asTaskEntry(entry: TaskIndexEntry, allUsers: User[]): TaskListingItem {
  const task: TaskListingItem = {
    id: entry.task.id,
    resolved: entry.task.resolved,
    participants: toTaskParticipants(entry, allUsers),
    summary: entry.task.summary,
    type: entry.task.type,
    createdAt: new Date(entry.task.createdAt).toISOString(),
  };
  return task;
}

function toTaskParticipants(
  entry: TaskIndexEntry,
  allUsers: User[]
): TaskParticipant[] {
  return entry.task.participants.map((participant) => {
    const foundUser = allUsers.find(
      (user) => user.username === participant.username
    );
    return {
      role: participant.role,
      user: foundUser!,
    };
  });
}

export interface LastItemsAggregation {
  latest?: {
    hits?: {
      hits: Array<{
        _source: any;
      }>;
    };
  };
}
