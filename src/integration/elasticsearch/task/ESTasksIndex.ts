import {
  Task,
  ListTasksParameters,
  RecentTasksDashboard,
  TaskListingItem,
} from "../../../domain/tasks";
import { Client } from "@elastic/elasticsearch";
import { addDays, startOfDay, parseISO } from "date-fns";
import { toIndexEntry, toTaskEntries } from "./ESTasksIndexMapper";
import { TasksIndex } from "../../../domain/tasks/TasksIndex";
import { TaskIndexEntry } from "./index";
import { UserRepository } from "../../../domain/users/UserRepository";

export class ESTasksIndex implements TasksIndex {
  constructor(
    private readonly esClient: Client,
    private readonly indexName: string,
    private readonly userRepository: UserRepository
  ) {}

  public save(task: Task): Promise<Task> {
    const entry = toIndexEntry(task);
    return this.esClient
      .index({
        index: this.indexName,
        id: entry.gid,
        body: entry,
      })
      .then(() => task);
  }

  public listTasksByTeamId(teamId: string, [offset, limit]: [number, number]) {
    return this.esClient
      .search({
        index: this.indexName,
        body: {
          query: { term: { "task.team.id": teamId } },
        },
        from: offset,
        size: limit,
      })
      .then((response) => response.body.hits.hits.map((hit) => hit._source))
      .then((entries) => this.toTaskEntries(entries));
  }

  public listTasks(
    teamId: string,
    parameters: ListTasksParameters,
    [offset, limit]: [number, number]
  ) {
    const queries = toListTasksQueries(parameters);
    return this.esClient
      .search({
        index: this.indexName,
        body: {
          query: {
            bool: {
              filter: [{ term: { "task.team.id": teamId } }],
              must: queries,
            },
          },
        },
        from: offset,
        size: limit,
      })
      .then((response) => response.body.hits.hits.map((hit) => hit._source))
      .then((entries) => this.toTaskEntries(entries));
  }

  public readRecentTasksDashboard(
    teamId: string,
    now: Date
  ): Promise<RecentTasksDashboard> {
    const todayStart = startOfDay(now).getTime();
    const yesterdayStart = addDays(todayStart, -1).getTime();
    const query = {
      aggs: {
        today_tasks: lastTasksForDates(teamId, [todayStart, now.getTime()]),
        yesterday_tasks: lastTasksForDates(teamId, [
          yesterdayStart,
          todayStart,
        ]),
        older_tasks: lastTasksForDates(teamId, [0, yesterdayStart]),
      },
    };

    return this.esClient
      .search({
        index: this.indexName,
        size: 0,
        body: query,
      })
      .then((response) => this.toRecentTasksDashboard(response.body));
  }

  private async toRecentTasksDashboard(
    body: any
  ): Promise<RecentTasksDashboard> {
    const { today_tasks, yesterday_tasks, older_tasks } = body.aggregations;

    const [todayTasks, yesterdayTasks, olderTasks] = await Promise.all([
      this.toTaskEntriesList(today_tasks),
      this.toTaskEntriesList(yesterday_tasks),
      this.toTaskEntriesList(older_tasks),
    ]);

    return {
      todayTasks,
      yesterdayTasks,
      olderTasks,
    };
  }

  private toTaskEntriesList(
    aggregation: LastItemsAggregation
  ): Promise<TaskListingItem[]> {
    if (!aggregation.latest || !aggregation.latest.hits) {
      return Promise.resolve([]);
    }
    const entries = aggregation.latest.hits.hits.map((hit) => hit._source);
    return this.toTaskEntries(entries);
  }

  private async toTaskEntries(entries: TaskIndexEntry[]) {
    const uniqueParticipantsUsernames =
      toAllUniqueParticipantsUsernames(entries);
    const uniqueParticipants = await this.userRepository.findByIds(
      uniqueParticipantsUsernames
    );
    return toTaskEntries(entries, uniqueParticipants);
  }
}

function lastTasksForDates(
  teamId: string,
  [dateFrom, dateTo]: [number, number]
) {
  return {
    filter: {
      bool: {
        filter: [
          { term: { "task.team.id": teamId } },
          { range: { "task.createdAt": { gte: dateFrom, lte: dateTo } } },
        ],
      },
    },
    aggs: {
      latest: {
        top_hits: {
          sort: [{ "task.createdAt": { order: "desc" } }],
          _source: {
            includes: [
              "task.id",
              "task.summary",
              "task.author",
              "task.type",
              "task.team",
              "task.participants",
              "task.resolved",
              "task.createdAt",
            ],
          },
          size: 5,
        },
      },
    },
  };
}

function toListTasksQueries(parameters: ListTasksParameters) {
  const queries: any[] = [];
  if (!!parameters.author) {
    queries.push({
      nested: {
        path: "task.participants",
        query: {
          bool: {
            filter: [{ term: { "task.participants.role": "author" } }],
            must: [
              { term: { "task.participants.username": parameters.author } },
            ],
          },
        },
      },
    });
  }
  if (!!parameters.project) {
    queries.push({ term: { "task.project.slug": parameters.project } });
  }
  if (!!parameters.phrase) {
    queries.push({
      bool: {
        should: [
          {
            match: { "task.content": { query: parameters.phrase, boost: 1.5 } },
          },
          {
            match: { "task.summary": { query: parameters.phrase, boost: 2.0 } },
          },
          { match: { "task.fts_content": { query: parameters.phrase } } },
          { match: { "task.fts_summary": { query: parameters.phrase } } },
        ],
      },
    });
  }
  if (!!parameters.content) {
    queries.push({ match: { "task.fts_content": parameters.content } });
  }
  if (!!parameters.summary) {
    queries.push({ match: { "task.fts_summary": parameters.summary } });
  }
  if (!!parameters.type) {
    queries.push({ term: { "task.type": parameters.type } });
  }
  if (!!parameters["createdAt.gte"] || !!parameters["createdAt.lte"]) {
    const min = !!parameters["createdAt.gte"]
      ? parseISO(parameters["createdAt.gte"]).getTime()
      : undefined;
    const max = !!parameters["createdAt.lte"]
      ? parseISO(parameters["createdAt.lte"]).getTime()
      : undefined;
    queries.push({
      range: {
        "task.createdAt": {
          gte: min,
          lte: max,
        },
      },
    });
  }
  return queries;
}

function toAllUniqueParticipantsUsernames(entries: TaskIndexEntry[]): string[] {
  const allUsernames = entries
    .map((entry) =>
      entry.task.participants.map((participant) => participant.username)
    )
    .reduce((left, right) => left.concat(right), []);
  const uniqueUsernames = [...new Set(allUsernames)];

  return uniqueUsernames;
}

export interface LastItemsAggregation {
  latest?: {
    hits?: {
      hits: Array<{
        _source: any;
      }>;
    };
  };
}
