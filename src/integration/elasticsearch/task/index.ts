import { TaskType } from "../../../domain/tasks";

export interface TaskIndexEntry {
  gid: string;
  resource: "task";
  task: {
    team: { id: string };
    project: { id: string; slug: string };
    createdAt: number;
    content: string;
    summary: string;
    fts_content: string;
    fts_summary: string;
    id: string;
    type: TaskType;
    resolved: boolean;
    participants: TaskParticipantIndexEntry[];
  };
}

export interface TaskParticipantIndexEntry {
  username: string;
  role: string;
}
