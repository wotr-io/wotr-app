import {
  Task,
  ListTasksParameters,
  TaskListingItem,
} from "../../../../domain/tasks";
import { parseISO } from "date-fns";
import nanoid = require("nanoid");
import { testContext } from "../../../../__tests__/test-context";
import { User } from "../../../../domain/users";
import { someTask } from "../../../../domain/tasks/__tests__/task.fixtures";

const { tasksIndex, esClient, usersRepository } = testContext;

describe("TaskIndex tests", () => {
  const teamId = nanoid();

  const user: User = {
    username: "john.doe",
    email: "john.doe@email.test",
    avatarUrl: "http://avatars.com/avatar/john.doe",
  };

  const task1: Task = {
    ...someTask(teamId),
    type: "bug",
    summary: "As a user I want to create projects",
    id: "BLU-1",
    createdAt: "2012-12-12T12:00:00.000Z",
  };
  const task2: Task = {
    ...someTask(teamId),
    id: "BLU-2",
    type: "bug",
    summary: "As a user I want to list tasks",
    createdAt: "2012-12-11T12:00:00.000Z",
  };
  const task3: Task = {
    ...someTask(teamId),
    id: "BLU-3",
    summary: "As a user I want to create projects",
    createdAt: "2012-12-10T12:00:00.000Z",
  };
  const task4: Task = {
    ...someTask(teamId),
    id: "BLU-4",
    createdAt: "2012-12-09T12:00:00.000Z",
  };

  beforeAll(() => {
    return Promise.all([
      saveTasks([task1, task2, task3, task4]),
      usersRepository.save(user),
    ]);
  });

  test("Find full tasks dashboard", async () => {
    // given:
    const time = parseISO("2012-12-12T16:00:00.000Z");

    // when:
    const dashboard = await tasksIndex.readRecentTasksDashboard(teamId, time);

    // then:
    const dashboardReport = [
      dashboard.todayTasks.map((task) => task.id),
      dashboard.yesterdayTasks.map((task) => task.id),
      dashboard.olderTasks.map((task) => task.id),
    ];

    expect(dashboardReport).toEqual([
      [task1.id],
      [task2.id],
      [task3.id, task4.id],
    ]);
  });

  test("Test find tasks", async () => {
    // given:
    const parameters: ListTasksParameters = {
      type: "bug",
      phrase: "create project",
      "createdAt.gte": "2012-12-12T00:00:00.000Z",
      "createdAt.lte": "2012-12-13T00:00:00.000Z",
    };

    // when:
    const tasks = await tasksIndex.listTasks(teamId, parameters, [0, 50]);

    // then:
    const expectedEntry: TaskListingItem = {
      id: task1.id,
      summary: task1.summary,
      participants: task1.participants,
      createdAt: task1.createdAt,
      type: task1.type,
      resolved: false,
    };
    expect(tasks).toEqual([expectedEntry]);
  });
});

async function saveTasks(tasks: Task[]) {
  await Promise.all(tasks.map((task) => tasksIndex.save(task)));
  return esClient.indices.refresh({
    index: testContext.configMap.elasticsearch.indexName,
  });
}
