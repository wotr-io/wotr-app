import { ConfigMap } from "../../domain/env";
import { buildWebContext } from "./web-context";
import { buildProjectsContext } from "./projects-context";
import { buildTasksContext } from "./tasks-context";
import { buildTeamsContext } from "./teams-context";

export function buildApplicationContext(configMap: ConfigMap) {
  const teamsContext = buildTeamsContext(configMap);
  const tasksContext = buildTasksContext(
    configMap,
    teamsContext.teamsAuthorizer
  );
  const projectsContext = buildProjectsContext(
    configMap,
    teamsContext.teamsRepository,
    teamsContext.teamsAuthorizer
  );

  const server = buildWebContext(configMap, [
    teamsContext.teamsController,
    tasksContext.tasksController,
    tasksContext.boardsController,
    projectsContext.projectsController,
  ]);
  return {
    configMap,
    ...teamsContext,
    ...tasksContext,
    ...projectsContext,
    ...server,
  };
}
