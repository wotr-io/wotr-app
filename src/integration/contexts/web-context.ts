import * as bodyParser from "body-parser";
import express from "express";
import { ExpressController } from "../../libs/express";
import {
  endpointNotFoundFilter,
  internalServerErrorHandler,
  globalErrorHandler,
  corsHeadersInterceptor,
} from "../../api/filters";

import { ConfigMap } from "../../domain/env";

export function buildWebContext(
  configMap: ConfigMap,
  controllers: ExpressController[]
): { server?: express.Express } {
  if (!configMap.web.enabled) {
    return {};
  }
  const server = express();
  server.use(bodyParser.json());
  server.use(corsHeadersInterceptor);

  for (const controller of controllers) {
    controller.registerEndpoints(server);
  }

  server.use(globalErrorHandler);
  server.use(internalServerErrorHandler);
  server.use(endpointNotFoundFilter);

  return { server };
}
