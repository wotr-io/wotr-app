import { TasksService } from "../../domain/tasks/TasksService";
import { TaskRepository } from "../../domain/tasks/TaskRepository";
import { Client } from "@elastic/elasticsearch";
import { TasksIndex } from "../../domain/tasks/TasksIndex";
import { BoardDefinitionRepository } from "../../domain/tasks/boards/BoardDefinitionRepository";
import { BoardDefinitionService } from "../../domain/tasks/boards/BoardDefinitionService";
import { BoardRepository } from "../../domain/tasks/boards/BoardRepository";
import { BoardService } from "../../domain/tasks/boards/BoardService";
import { xrayMonitored } from "../../libs/xray/xray-proxy";
import { TeamRepository } from "../../domain/teams/TeamRepository";
import { DDBProjectRepository } from "../persistence/project/DDBProjectRepository";
import { ProjectRepository } from "../../domain/projects/ProjectRepository";
import { dynamodbClient } from "../xray/aws-resources";
import { ConfigMap } from "../../domain/env";
import { DDBTeamRepository } from "../persistence/team/DDBTeamRepository";
import { DDBUserRepository } from "../persistence/user/DDBUserRepository";
import { DDBTaskRepository } from "../persistence/task/DDBTaskRepository";
import { UserRepository } from "../../domain/users/UserRepository";
import { DDBBoardDefinitionRepository } from "../persistence/task/board/DDBBoardDefinitionRepository";
import { DDBBoardRepository } from "../persistence/task/board/DDBBoardRepository";
import { ESTasksIndex } from "../elasticsearch/task/ESTasksIndex";
import { TasksController } from "../../api/TasksController";
import { TeamsAuthorizer } from "../../api/auth/TeamsAuthorizer";
import { BoardsController } from "../../api/BoardsController";

export function buildTasksContext(
  configMap: ConfigMap,
  teamsAuthorizer: TeamsAuthorizer
) {
  const ddb = dynamodbClient(configMap);
  const projectsRepository: ProjectRepository = new DDBProjectRepository(
    ddb,
    configMap.dynamodb
  );
  const teamsRepository: TeamRepository = new DDBTeamRepository(
    ddb,
    configMap.dynamodb
  );
  const esClient = xrayMonitored(
    new Client({
      node: configMap.elasticsearch.url,
    }),
    configMap.xray
  );

  const usersRepository: UserRepository = new DDBUserRepository(
    ddb,
    configMap.dynamodb
  );
  const tasksIndex: TasksIndex = new ESTasksIndex(
    esClient,
    configMap.elasticsearch.indexName,
    usersRepository
  );
  const tasksRepository: TaskRepository = new DDBTaskRepository(
    ddb,
    usersRepository,
    projectsRepository,
    configMap.dynamodb
  );
  const tasksService = new TasksService(
    teamsRepository,
    projectsRepository,
    tasksRepository,
    tasksIndex,
    usersRepository
  );

  const boardDefinitionsRepository: BoardDefinitionRepository =
    new DDBBoardDefinitionRepository(ddb, usersRepository, configMap.dynamodb);
  const boardDefinitionService = new BoardDefinitionService(
    teamsRepository,
    usersRepository,
    boardDefinitionsRepository
  );

  const boardRepository: BoardRepository = new DDBBoardRepository(
    ddb,
    boardDefinitionsRepository,
    tasksRepository,
    configMap.dynamodb
  );

  const boardService = new BoardService(
    teamsRepository,
    boardRepository,
    boardDefinitionsRepository,
    tasksRepository,
    tasksIndex
  );

  const tasksController = new TasksController(teamsAuthorizer, tasksService);
  const boardsController = new BoardsController(
    teamsAuthorizer,
    boardDefinitionService,
    boardService
  );
  return {
    boardService,
    boardRepository,
    boardDefinitionService,
    boardDefinitionsRepository,
    tasksService,
    tasksRepository,
    tasksIndex,
    esClient,
    tasksController,
    boardsController,
  };
}
