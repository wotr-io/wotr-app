import { cognitoClient, dynamodbClient } from "../xray/aws-resources";
import { DefaultCognitoClient } from "../../domain/teams/permissions/CognitoClient";
import { TeamPermissions } from "../../domain/teams/permissions/TeamPermissions";
import { TeamsAuthorizer } from "../../api/auth/TeamsAuthorizer";
import { TeamRepository } from "../../domain/teams/TeamRepository";
import { TeamsService } from "../../domain/teams/TeamsService";
import { TeamPermissionRepository } from "../../domain/teams/permissions/TeamPermissionRepository";
import { TeamMembersService } from "../../domain/teams/members/TeamMembersService";
import { TeamMemberRepository } from "../../domain/teams/members/TeamMemberRepository";
import { UserRepository } from "../../domain/users/UserRepository";
import { ConfigMap } from "../../domain/env";
import { DDBTeamRepository } from "../persistence/team/DDBTeamRepository";
import { DDBUserRepository } from "../persistence/user/DDBUserRepository";
import { DDBTeamPermissionRepository } from "../persistence/team/permissions/DDBTeamPermissionRepository";
import { DDBTeamMemberRepository } from "../persistence/team/members/DDBTeamMemberRepository";
import { TeamsController } from "../../api/TeamsController";

export function buildTeamsContext(configMap: ConfigMap) {
  const ddb = dynamodbClient(configMap);
  const teamsRepository: TeamRepository = new DDBTeamRepository(
    ddb,
    configMap.dynamodb
  );
  const teamPermissionsRepository: TeamPermissionRepository =
    new DDBTeamPermissionRepository(ddb, configMap.dynamodb);

  const teamsPermissions = new TeamPermissions(
    configMap.cognito.userpoolArn
      ? new DefaultCognitoClient(cognitoClient(configMap))
      : {
          adminUpdateUserAttributes() {
            return Promise.resolve({});
          },
          adminGetUser(params) {
            return Promise.resolve({
              Username: params.Username,
              UserAttributes: [
                {
                  Name: "custom:teams",
                  Value: "",
                },
              ],
            });
          },
        },
    configMap.cognito.userpoolId,
    teamPermissionsRepository
  );
  const teamsService = new TeamsService(teamsRepository, teamsPermissions, {
    table: configMap.dynamodb.tableName,
    avatarsUrl: configMap.teams.avatarsUrlTemplate,
  });
  const usersRepository: UserRepository = new DDBUserRepository(
    ddb,
    configMap.dynamodb
  );

  const teamMembersRepository: TeamMemberRepository =
    new DDBTeamMemberRepository(ddb, configMap.dynamodb);
  const teamMembersService = new TeamMembersService(
    teamsRepository,
    teamMembersRepository,
    usersRepository
  );

  const teamsAuthorizer = new TeamsAuthorizer(teamPermissionsRepository);

  const teamsController = new TeamsController(
    teamsAuthorizer,
    teamsService,
    teamMembersService
  );

  return {
    teamsService,
    teamsRepository,
    usersRepository,
    teamMembersService,
    teamMembersRepository,
    teamPermissionsRepository,
    teamsAuthorizer,
    teamsController,
  };
}
