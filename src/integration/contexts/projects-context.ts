import { dynamodbClient } from "../xray/aws-resources";
import { ConfigMap } from "../../domain/env";
import { TeamRepository } from "../../domain/teams/TeamRepository";
import { DDBProjectRepository } from "../persistence/project/DDBProjectRepository";
import { ProjectsService } from "../../domain/projects/ProjectsService";
import { ProjectRepository } from "../../domain/projects/ProjectRepository";
import { ProjectsController } from "../../api/ProjectsController";
import { TeamsAuthorizer } from "../../api/auth/TeamsAuthorizer";

export function buildProjectsContext(
  configMap: ConfigMap,
  teamsRepository: TeamRepository,
  teamsAuthorizer: TeamsAuthorizer
) {
  const ddb = dynamodbClient(configMap);
  const projectsRepository: ProjectRepository = new DDBProjectRepository(
    ddb,
    configMap.dynamodb
  );
  const projectsService = new ProjectsService(
    projectsRepository,
    teamsRepository
  );
  const projectsController = new ProjectsController(
    teamsAuthorizer,
    projectsService
  );
  return {
    projectsService,
    projectsRepository,
    projectsController,
  };
}
