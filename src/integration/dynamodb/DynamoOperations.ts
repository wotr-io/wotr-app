import { DynamoDB } from "aws-sdk";
import { DDBProperties } from "../../domain/env";
import { EntityNotFoundError } from "../../domain/errors/EntityNotFoundError";
import { Record } from "./index";

const converterOptions: DynamoDB.Converter.ConverterOptions = {
  convertEmptyValues: true,
};

export class DynamoOperations<T extends Record> {
  constructor(
    public readonly dynamoDb: DynamoDB,
    public readonly resource: T["resource"],
    public readonly databaseConfig: DDBProperties
  ) {}

  public save(entity: T): Promise<T> {
    const record = marshallRecord(entity);
    return this.dynamoDb
      .putItem({
        TableName: this.databaseConfig.tableName,
        Item: record,
      })
      .promise()
      .then(() => entity);
  }

  public find(gid: string): Promise<T> {
    return this.findByGid(gid).then((record) => {
      if (!record.Item) {
        return Promise.reject(new EntityNotFoundError(this.resource, gid));
      }
      return Promise.resolve(unmarshallRecord(record.Item) as T);
    });
  }

  public tryFind(gid: string): Promise<T | null> {
    return this.findByGid(gid).then((record) => {
      if (!record.Item) {
        return Promise.resolve(null);
      }
      return Promise.resolve(unmarshallRecord(record.Item) as T);
    });
  }

  public async findByGids(gids: string[]): Promise<T[]> {
    if (gids.length === 0) {
      return [];
    }
    return this.dynamoDb
      .batchGetItem({
        RequestItems: {
          [this.databaseConfig.tableName]: {
            Keys: gids.map((gid) => ({
              gid: { S: gid },
              resource: { S: this.resource },
            })),
          },
        },
      })
      .promise()
      .then((results) =>
        results
          .Responses![this.databaseConfig.tableName].map(
            (result) => unmarshallRecord(result) as T
          )
          .sort(textComparator(gids))
      );
  }

  public delete(gid: string): Promise<void> {
    return this.dynamoDb
      .deleteItem({
        TableName: this.databaseConfig.tableName,
        Key: {
          gid: { S: gid },
          resource: { S: this.resource },
        },
      })
      .promise()
      .then(() => void 0);
  }

  public deleteByIds(gids: string[]): Promise<any> {
    return this.dynamoDb
      .batchWriteItem({
        RequestItems: {
          [this.databaseConfig.tableName]: gids.map((gid) => ({
            DeleteRequest: {
              Key: {
                gid: { S: gid },
                resource: { S: this.resource },
              },
            },
          })),
        },
      })
      .promise();
  }

  private findByGid(gid: string) {
    return this.dynamoDb
      .getItem({
        TableName: this.databaseConfig.tableName,
        Key: {
          gid: { S: gid },
          resource: { S: this.resource },
        },
      })
      .promise();
  }
}

export function marshallRecord(data: {
  [key: string]: any;
}): DynamoDB.AttributeMap {
  return DynamoDB.Converter.marshall(data, converterOptions);
}

export function unmarshallRecord<T>(data: DynamoDB.AttributeMap): T {
  return DynamoDB.Converter.unmarshall(data, converterOptions) as T;
}

const textComparator =
  (gids: string[]) =>
  (t1: Record, t2: Record): number => {
    const i1 = gids.indexOf(t1.gid);
    const i2 = gids.indexOf(t2.gid);
    if (i1 > i2) {
      return 1;
    }
    if (i1 < i2) {
      return -1;
    }
    return 0;
  };
