import * as crypto from "crypto";

export interface Record {
  gid: string;
  resource: string;
  parentGid?: string;
  targetGid?: string;
}

export function hashGid(data: string) {
  return crypto.createHash("sha1").update(data).digest("hex");
}
