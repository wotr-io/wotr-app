import { EntityNotFoundError } from "../../../domain/errors/EntityNotFoundError";
import { DynamoOperations } from "../../../integration/dynamodb/DynamoOperations";
import { Record } from "../../../integration/dynamodb";
import { DynamoDB } from "aws-sdk";
import { testContext } from "../../../__tests__/test-context";

type AnyRecord = { [key: string]: any } & Record;

const { configMap } = testContext;

describe("TeamRepository tests", () => {
  const itemsRepository = new DynamoOperations(
    new DynamoDB({
      endpoint: configMap.dynamodb.endpoint,
    }),
    "item",
    configMap.dynamodb
  );

  test("Test retrieve item with tryFind", async () => {
    const gid = "item:__id__";
    // given:
    const record: AnyRecord = {
      gid,
      resource: "item",
      name: "Wotr.io Team",
      permissions: [
        {
          action: "read",
          resource: "team",
          username: "__user-id__",
        },
      ],
    };
    await itemsRepository.save(record);

    // when:
    const insertedRecord = await itemsRepository.tryFind(gid);

    // then:
    return expect(insertedRecord).toEqual(record);
  });

  test("Test retrieve nonexistent item with find", async () => {
    // given:
    const nonexistentId = "__nonexistent-team-id__";

    // when:
    const result = itemsRepository.find(nonexistentId);

    // then:
    await expect(result).rejects.toBeDefined();
    await result.catch((error: EntityNotFoundError) => {
      expect(error).toBeInstanceOf(EntityNotFoundError);
      expect(error.entity).toEqual("item");
      expect(error.identifier).toEqual(nonexistentId);
    });
  });

  test("Test retrieve multiple items", async () => {
    // given:
    const record1: AnyRecord = {
      gid: "item:__id-1__",
      resource: "item",
      name: "Wotr.io Team 1",
    };
    const record2: AnyRecord = {
      gid: "item:__id-2__",
      resource: "item",
      name: "Wotr.io Team 2",
    };
    await itemsRepository.save(record1);
    await itemsRepository.save(record2);

    // when:
    const teams = await itemsRepository.findByGids([record1.gid, record2.gid]);

    // then:
    expect(teams).toEqual([record1, record2]);
  });
});
