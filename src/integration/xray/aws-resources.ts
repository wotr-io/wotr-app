import { CognitoIdentityServiceProvider, DynamoDB } from "aws-sdk";
import AWSXRay = require("aws-xray-sdk-core");
import { ConfigMap } from "../../domain/env";

export function dynamodbClient(configMap: ConfigMap): DynamoDB {
  const ddb = new DynamoDB({
    endpoint: configMap.dynamodb.endpoint,
  });
  return configMap.xray.enabled ? AWSXRay.captureAWSClient(ddb) : ddb;
}

export function cognitoClient(configMap: ConfigMap) {
  return configMap.xray.enabled
    ? AWSXRay.captureAWSClient(new CognitoIdentityServiceProvider())
    : new CognitoIdentityServiceProvider();
}
