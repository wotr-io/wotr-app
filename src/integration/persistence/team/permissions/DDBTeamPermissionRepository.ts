import { Record, hashGid } from "../../../dynamodb";
import { TeamPermission } from "../../../../domain/teams/permissions";
import {
  DynamoOperations,
  unmarshallRecord,
} from "../../../dynamodb/DynamoOperations";
import { DynamoDB } from "aws-sdk";
import { toTeamGid } from "../DDBTeamRepository";
import { DDBProperties } from "../../../../domain/env";
import { TeamPermissionRepository } from "../../../../domain/teams/permissions/TeamPermissionRepository";

interface TeamPermissionRecord extends Record {
  resource: "team-permission";
  allowedResource: string;
  allowedAction: string;
  username: string;
  teamId: string;
}

export class DDBTeamPermissionRepository implements TeamPermissionRepository {
  private readonly dynamoOperations: DynamoOperations<TeamPermissionRecord>;
  private readonly ddbProperties: DDBProperties;

  constructor(dynamoDb: DynamoDB, ddbProperties: DDBProperties) {
    this.dynamoOperations = new DynamoOperations<TeamPermissionRecord>(
      dynamoDb,
      "team-permission",
      ddbProperties
    );
    this.ddbProperties = ddbProperties;
  }

  public save(item: TeamPermission): Promise<TeamPermission> {
    const record = toRecord(item);
    return this.dynamoOperations.save(record).then(toTeamPermission);
  }

  public findByTeamIdAndUsername(
    teamId: string,
    username: string
  ): Promise<TeamPermission[]> {
    return this.dynamoOperations.dynamoDb
      .query({
        TableName: this.ddbProperties.tableName,
        IndexName: this.ddbProperties.parentTargetIndex,
        KeyConditionExpression:
          "parentGid = :username AND targetGid = :teamGid",
        FilterExpression: "#resource = :resource",
        ExpressionAttributeValues: {
          ":teamGid": { S: toTeamGid(teamId) },
          ":username": { S: toUserGid(username) },
          ":resource": { S: "team-permission" },
        },
        ExpressionAttributeNames: {
          "#resource": "resource",
        },
      })
      .promise()
      .then((results) =>
        results.Items!.map((record) =>
          toTeamPermission(unmarshallRecord(record))
        )
      );
  }
}

function toTeamPermissionGid(permission: TeamPermission): string {
  const hash = hashPermission(permission);
  return `team-permission:${hash}`;
}

function hashPermission(permission: TeamPermission): string {
  const permissionId = [
    permission.teamId,
    permission.username,
    permission.action,
    permission.resource,
  ].join("_");
  return hashGid(permissionId);
}

function toRecord(permission: TeamPermission): TeamPermissionRecord {
  return {
    gid: toTeamPermissionGid(permission),
    parentGid: toUserGid(permission.username),
    targetGid: toTeamGid(permission.teamId),
    resource: "team-permission",
    teamId: permission.teamId,
    username: permission.username,
    allowedAction: permission.action,
    allowedResource: permission.resource,
  };
}

function toUserGid(username: string): string {
  return `user:${username}`;
}

function toTeamPermission(record: TeamPermissionRecord): TeamPermission {
  return {
    username: record.username,
    teamId: record.teamId,
    action: record.allowedAction,
    resource: record.allowedResource,
  };
}
