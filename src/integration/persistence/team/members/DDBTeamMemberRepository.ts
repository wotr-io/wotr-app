import { Record, hashGid } from "../../../dynamodb";
import {
  DynamoOperations,
  unmarshallRecord,
} from "../../../dynamodb/DynamoOperations";
import { DynamoDB } from "aws-sdk";
import { toTeamGid } from "../DDBTeamRepository";
import { DDBProperties } from "../../../../domain/env";
import { TeamMember } from "../../../../domain/teams/members";
import { TeamMemberRepository } from "../../../../domain/teams/members/TeamMemberRepository";

interface TeamMemberRecord extends Record {
  resource: "team-member";
  parentGid: string;
  targetGid: string;
  teamId: string;
  username: string;
}

export class DDBTeamMemberRepository implements TeamMemberRepository {
  private readonly dynamoOperations: DynamoOperations<TeamMemberRecord>;
  private readonly config: DDBProperties;

  constructor(dynamoDb: DynamoDB, config: DDBProperties) {
    this.dynamoOperations = new DynamoOperations<TeamMemberRecord>(
      dynamoDb,
      "team-member",
      config
    );
    this.config = config;
  }

  public listTeamMembers(teamId: string): Promise<TeamMember[]> {
    return this.dynamoOperations.dynamoDb
      .query({
        TableName: this.config.tableName,
        IndexName: this.config.parentResourceIndex,
        KeyConditionExpression:
          "parentGid = :teamGid and #resource = :resource",
        ExpressionAttributeValues: {
          ":teamGid": { S: toTeamGid(teamId) },
          ":resource": { S: "team-member" },
        },
        ExpressionAttributeNames: {
          "#resource": "resource",
        },
      })
      .promise()
      .then((result) =>
        result
          .Items!.map((item) => unmarshallRecord<TeamMemberRecord>(item))
          .map((record) => toTeamMember(record))
      );
  }

  public save(teamId: string, member: TeamMember): Promise<TeamMember> {
    const record = toTeamMemberRecord(teamId, member);
    return this.dynamoOperations
      .save(record)
      .then((savedRecord) => toTeamMember(savedRecord));
  }
}

function toTeamMemberGid(teamId: string, username: string) {
  const teamMemberGid = [teamId, username].join("_");
  return `team-member:${hashGid(teamMemberGid)}`;
}

function toTeamMemberRecord(
  teamId: string,
  member: TeamMember
): TeamMemberRecord {
  return {
    gid: toTeamMemberGid(teamId, member.username),
    resource: "team-member",
    parentGid: toTeamGid(teamId),
    targetGid: `user:${member.username}`,
    teamId,
    username: member.username,
  };
}

function toTeamMember(record: TeamMemberRecord): TeamMember {
  return {
    username: record.username,
  };
}
