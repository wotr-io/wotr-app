import { DynamoDB } from "aws-sdk";
import { Team } from "../../../domain/teams";
import { DynamoOperations } from "../../dynamodb/DynamoOperations";
import { Record } from "../../dynamodb";
import { DDBProperties } from "../../../domain/env";
import { TeamRepository } from "../../../domain/teams/TeamRepository";

interface TeamRecord extends Record {
  resource: "team";
  id: string;
  name: string;
}

export class DDBTeamRepository implements TeamRepository {
  private readonly dynamoOperations: DynamoOperations<TeamRecord>;

  constructor(dynamoDb: DynamoDB, config: DDBProperties) {
    this.dynamoOperations = new DynamoOperations<TeamRecord>(
      dynamoDb,
      "team",
      config
    );
  }

  public save(item: Team): Promise<Team> {
    const record = toRecord(item);
    return this.dynamoOperations.save(record).then(toTeam);
  }

  public find(id: string): Promise<Team> {
    return this.dynamoOperations.find(toTeamGid(id)).then(toTeam);
  }

  public tryFind(id: string): Promise<Team | null> {
    return this.dynamoOperations
      .tryFind(toTeamGid(id))
      .then((record) => (!!record ? toTeam(record) : null));
  }

  public findByIds(ids: string[]): Promise<Team[]> {
    const gids = ids.map((id) => toTeamGid(id));
    return this.dynamoOperations
      .findByGids(gids)
      .then((records) => records.map((record) => toTeam(record)));
  }

  public delete(id: string): Promise<void> {
    return this.dynamoOperations.delete(toTeamGid(id));
  }
}

export function toTeamGid(id: string): string {
  return `team:${id}`;
}

function toRecord(team: Team): TeamRecord {
  return {
    gid: toTeamGid(team.id),
    resource: "team",
    id: team.id,
    name: team.name,
  };
}

function toTeam(record: TeamRecord): Team {
  return {
    id: record.id,
    name: record.name,
  };
}
