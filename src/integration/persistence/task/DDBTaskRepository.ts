import { Record, hashGid } from "../../dynamodb";
import {
  TaskType,
  Task,
  TaskParticipant,
  TaskListingItem,
} from "../../../domain/tasks";
import {
  DynamoOperations,
  marshallRecord,
} from "../../dynamodb/DynamoOperations";
import { DynamoDB } from "aws-sdk";
import { Project } from "../../../domain/projects";
import { toProjectGid } from "../project/DDBProjectRepository";
import { BusinessBoundaryError } from "../../../domain/errors/BusinessBoundaryError";
import { UserRepository } from "../../../domain/users/UserRepository";
import { User } from "../../../domain/users";
import { DDBProperties } from "../../../domain/env";
import { ProjectRepository } from "../../../domain/projects/ProjectRepository";
import { TaskRepository } from "../../../domain/tasks/TaskRepository";

interface TaskRecord extends Record {
  resource: "task";
  parentGid: string;
  id: string;
  projectId: string;
  participants: TaskParticipantRecord[];
  summary: string;
  content: string;
  type: TaskType;
  resolved: boolean;
  createdAt: string;
}

interface TaskParticipantRecord {
  username: string;
  role: string;
}

export class DDBTaskRepository implements TaskRepository {
  private readonly dynamoOperations: DynamoOperations<TaskRecord>;
  private readonly usersRepository: UserRepository;
  private readonly projectsRepository: ProjectRepository;

  constructor(
    dynamoDb: DynamoDB,
    usersRepository: UserRepository,
    projectsRepository: ProjectRepository,
    config: DDBProperties
  ) {
    this.dynamoOperations = new DynamoOperations<TaskRecord>(
      dynamoDb,
      "task",
      config
    );
    this.usersRepository = usersRepository;
    this.projectsRepository = projectsRepository;
  }

  public save(item: Task): Promise<Task> {
    const record = toTaskRecord(item);
    const expectedTotalTasksCount = parseTaskId(item.id).index - 1;

    return this.dynamoOperations.dynamoDb
      .transactWriteItems({
        TransactItems: [
          {
            Update: {
              ConditionExpression: "totalTasks = :expectedTotalTasksCount",
              TableName: this.dynamoOperations.databaseConfig.tableName,
              UpdateExpression: "ADD totalTasks :one",
              ExpressionAttributeValues: {
                ":one": { N: "1" },
                ":expectedTotalTasksCount": {
                  N: expectedTotalTasksCount.toString(),
                },
              },
              Key: {
                gid: { S: toProjectGid(item.project.id) },
                resource: { S: "project-stats" },
              },
            },
          },
          {
            Put: {
              TableName: this.dynamoOperations.databaseConfig.tableName,
              Item: marshallRecord(record),
            },
          },
        ],
      })
      .promise()
      .then(() => item, captureTransactionError);
  }

  public markResolved(item: Task) {
    return this.dynamoOperations.dynamoDb
      .updateItem({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        Key: {
          gid: { S: toTaskGid(item.project.teamId, item.id) },
          resource: { S: "task" },
        },
        UpdateExpression: "SET #resolved = :value",
        ExpressionAttributeNames: {
          "#resolved": "resolved",
        },
        ExpressionAttributeValues: {
          ":value": { BOOL: item.resolved },
        },
      })
      .promise()
      .then(() => item, captureTransactionError);
  }

  public update(item: Task): Promise<Task> {
    return this.dynamoOperations.dynamoDb
      .updateItem({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        Key: {
          gid: { S: toTaskGid(item.project.teamId, item.id) },
          resource: { S: "task" },
        },
        UpdateExpression:
          "SET #content = :content, #summary = :summary, #type = :type",
        ExpressionAttributeNames: {
          "#content": "content",
          "#summary": "summary",
          "#type": "type",
        },
        ExpressionAttributeValues: {
          ":content": { S: item.content },
          ":summary": { S: item.summary },
          ":type": { S: item.type },
        },
      })
      .promise()
      .then(() => item, captureTransactionError);
  }

  public async find(teamId: string, taskId: string): Promise<Task> {
    const gid = toTaskGid(teamId, taskId);
    const taskRecord = await this.dynamoOperations.find(gid);
    const usernames = taskRecord.participants.map(
      (participant) => participant.username
    );
    const users = await this.usersRepository.findByIds(usernames);
    const project = await this.projectsRepository.find(taskRecord.projectId);
    return toTask(project, taskRecord, users);
  }

  public async findEntriesByIds(
    teamId: string,
    taskIds: string[]
  ): Promise<TaskListingItem[]> {
    const taskGids = taskIds.map((taskId) => toTaskGid(teamId, taskId));
    const taskRecords = await this.dynamoOperations.findByGids(taskGids);
    if (taskRecords.length === 0) {
      return [];
    }

    const usernames = [
      ...new Set(
        taskRecords
          .map((taskRecord) =>
            taskRecord.participants.map((participant) => participant.username)
          )
          .reduce((left, right) => left.concat(right), [])
      ),
    ];
    const users = await this.usersRepository.findByIds(usernames);
    return taskRecords.map((record) => toTaskEntry(record, users));
  }

  public async tryFind(project: Project, taskId: string): Promise<Task | null> {
    const gid = toTaskGid(project.teamId, taskId);
    const taskRecord = await this.dynamoOperations.tryFind(gid);
    if (!taskRecord) {
      return null;
    }
    const usernames = taskRecord.participants.map(
      (participant) => participant.username
    );
    const users = await this.usersRepository.findByIds(usernames);
    return toTask(project, taskRecord, users);
  }

  public delete(project: Project, taskId: string): Promise<void> {
    const gid = toTaskGid(project.teamId, taskId);
    return this.dynamoOperations.delete(gid);
  }
}

function toTask(project: Project, record: TaskRecord, users: User[]): Task {
  return {
    id: record.id,
    project,
    resolved: record.resolved,
    summary: record.summary,
    participants: toTaskParticipants(record, users),
    content: record.content,
    type: record.type,
    createdAt: record.createdAt,
  };
}
function toTaskEntry(record: TaskRecord, users: User[]): TaskListingItem {
  return {
    id: record.id,
    summary: record.summary,
    participants: toTaskParticipants(record, users),
    type: record.type,
    resolved: record.resolved,
    createdAt: record.createdAt,
  };
}

function toTaskParticipants(
  record: TaskRecord,
  users: User[]
): TaskParticipant[] {
  return record.participants.map((participant) => {
    const foundUser = users.find(
      (user) => user.username === participant.username
    );
    return {
      user: foundUser!,
      role: participant.role,
    };
  });
}

function captureTransactionError(err: any): Promise<Task> {
  if (err.code && err.code === "TransactionCanceledException") {
    console.log(err, err.code);
    return Promise.reject(
      new BusinessBoundaryError("resources-conflict", ["tasks"])
    );
  }
  return Promise.reject(err);
}

export function toTaskGid(teamId: string, taskId: string): string {
  const hash = hashGid([teamId, taskId].join("_"));
  return `task:${hash}`;
}

function parseTaskId(taskId: string): {
  projectSlug: string;
  index: number;
  raw: string;
} {
  const [projectSlug, counter] = taskId.split("-");
  return {
    projectSlug,
    raw: taskId,
    index: parseInt(counter, 10),
  };
}

function toTaskRecord(task: Task): TaskRecord {
  return {
    gid: toTaskGid(task.project.teamId, task.id),
    parentGid: toProjectGid(task.project.id),
    resource: "task",
    id: task.id,
    type: task.type,
    projectId: task.project.id,
    summary: task.summary,
    participants: toTaskParticipantRecords(task),
    content: task.content,
    resolved: task.resolved,
    createdAt: task.createdAt,
  };
}
function toTaskParticipantRecords(task: Task): TaskParticipantRecord[] {
  return task.participants.map((participant) => ({
    username: participant.user.username,
    role: participant.role,
  }));
}
