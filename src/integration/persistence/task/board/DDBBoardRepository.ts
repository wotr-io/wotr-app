import { Record, hashGid } from "../../../dynamodb";
import {
  DynamoOperations,
  marshallRecord,
  unmarshallRecord,
} from "../../../dynamodb/DynamoOperations";
import { DynamoDB } from "aws-sdk";
import {
  BoardDefinition,
  Board,
  BoardColumn,
  BoardListItem,
} from "../../../../domain/tasks/boards";
import { toTeamGid } from "../../team/DDBTeamRepository";
import { BoardDefinitionRepository } from "../../../../domain/tasks/boards/BoardDefinitionRepository";
import {
  TransactWriteItemList,
  TransactWriteItem,
} from "aws-sdk/clients/dynamodb";
import { TaskListingItem } from "../../../../domain/tasks";
import nanoid = require("nanoid");
import { DDBProperties } from "../../../../domain/env";
import { TaskRepository } from "../../../../domain/tasks/TaskRepository";
import { toTaskGid } from "../DDBTaskRepository";
import { toBoardDefnitionGid } from "./DDBBoardDefinitionRepository";
import { BoardRepository } from "../../../../domain/tasks/boards/BoardRepository";

interface BoardRecord extends Record {
  resource: "board";
  parentGid: string;
  id: string;
  name: string;
  teamId: string;
  targetGid: string;
  boardDefinitionId: string;
  version: string;
}

interface BoardColumnRecord extends Record {
  resource: "board-column";
  parentGid: string;
  id: string;
  statusId: string;
  tasks: Array<{
    id: string;
    gid: string;
  }>;
  version: string;
}

interface BoardTaskAssignmentRecord extends Record {
  resource: "board-task-assignment";
  parentGid: string;
  boardId: string;
  targetGid: string;
  taskId: string;
  statusId: string;
  version: string;
}

const MAX_TRANSACT_WRITE_ITEMS_COUNT = 25;

export class DDBBoardRepository implements BoardRepository {
  private readonly dynamoOperations: DynamoOperations<BoardRecord>;
  private readonly boardDefnitionsRepository: BoardDefinitionRepository;
  private readonly tasksRepository: TaskRepository;

  constructor(
    dynamoDb: DynamoDB,
    boardDefinitionsRepository: BoardDefinitionRepository,
    tasksRepository: TaskRepository,
    config: DDBProperties
  ) {
    this.dynamoOperations = new DynamoOperations<BoardRecord>(
      dynamoDb,
      "board",
      config
    );
    this.boardDefnitionsRepository = boardDefinitionsRepository;
    this.tasksRepository = tasksRepository;
  }

  public async save(definition: BoardDefinition, board: Board): Promise<Board> {
    const boardRecord = toBoardRecord(definition, board);
    const columnRecords = board.columns.map((column) =>
      toBoardColumnRecord(board, column)
    );
    const taskAssignmentsRecords = toTaskAssignmentsRecords(board);

    await this.saveBoardRecord(boardRecord, columnRecords);
    await this.saveTaskAssignmentRecords(taskAssignmentsRecords);
    return board;
  }

  public async listByTeamId(teamId: string): Promise<BoardListItem[]> {
    const teamGid = toTeamGid(teamId);
    const boardRecords = await this.dynamoOperations.dynamoDb
      .query({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        IndexName: this.dynamoOperations.databaseConfig.parentResourceIndex,
        KeyConditionExpression:
          "#parentGid = :parentGid and #resource = :resource",
        ExpressionAttributeNames: {
          "#parentGid": "parentGid",
          "#resource": "resource",
        },
        ExpressionAttributeValues: {
          ":resource": { S: "board" },
          ":parentGid": { S: teamGid },
        },
      })
      .promise()
      .then((response) =>
        response.Items!.map((item) => unmarshallRecord<BoardRecord>(item))
      );

    return boardRecords.map((boardRecord) => ({
      id: boardRecord.id,
      name: boardRecord.name,
    }));
  }

  public async find(boardId: string): Promise<Board> {
    const boardRecord = await this.dynamoOperations.find(toBoardGid(boardId));
    const boardDefinition = await this.boardDefnitionsRepository.find(
      boardRecord.boardDefinitionId
    );
    const boardColumnRecords = await this.findBoardColumnRecords(boardRecord);

    const taskIds = allTaskIds(boardColumnRecords);

    if (taskIds.length === 0) {
      return toBoard([], boardRecord);
    }

    const allTasks = await this.tasksRepository.findEntriesByIds(
      boardRecord.teamId,
      taskIds
    );

    const boardColumns: BoardColumn[] = boardDefinition.statuses
      .map((status) =>
        boardColumnRecords.find(
          (columnRecord) => columnRecord.statusId === status.id
        )
      )
      .map((columnRecord) =>
        toBoardColumn(boardDefinition, columnRecord!, allTasks)
      );
    return toBoard(boardColumns, boardRecord);
  }

  public async saveTaskTransition(
    board: Board,
    taskId: string,
    [sourceColumn, targetColumn]: [BoardColumn, BoardColumn]
  ): Promise<any> {
    const sourceColumnRecord = toBoardColumnRecord(board, sourceColumn);
    const targetColumnRecord = toBoardColumnRecord(board, targetColumn);
    const taskAssignmentRecord = await this.findBoardTaskAssignment(
      board,
      taskId
    );

    return this.dynamoOperations.dynamoDb
      .transactWriteItems({
        TransactItems: [
          {
            Update: this.toBoardColumnUpdate(sourceColumnRecord),
          },
          {
            Update: this.toBoardColumnUpdate(targetColumnRecord),
          },
          {
            Update: {
              Key: {
                gid: { S: taskAssignmentRecord.gid },
                resource: { S: taskAssignmentRecord.resource },
              },
              TableName: this.dynamoOperations.databaseConfig.tableName,
              UpdateExpression:
                "SET #statusId = :statusId, #version = :nextVersion",
              ConditionExpression: "#version = :version",
              ExpressionAttributeNames: {
                "#version": "version",
                "#statusId": "statusId",
              },
              ExpressionAttributeValues: {
                ":version": { S: taskAssignmentRecord.version },
                ":nextVersion": { S: nanoid() },
                ":statusId": {
                  S: targetColumnRecord.statusId,
                },
              },
            },
          },
        ],
      })
      .promise();
  }

  public saveBoardColumn(
    board: Board,
    column: BoardColumn
  ): Promise<BoardColumn> {
    const columnUpdate = this.toBoardColumnUpdate(
      toBoardColumnRecord(board, column)
    );
    return this.dynamoOperations.dynamoDb
      .updateItem(columnUpdate)
      .promise()
      .then(() => column);
  }

  private toBoardColumnUpdate(
    columnRecord: BoardColumnRecord
  ): DynamoDB.Update {
    const marshalled = marshallRecord(columnRecord);
    return {
      Key: {
        gid: { S: columnRecord.gid },
        resource: { S: columnRecord.resource },
      },
      TableName: this.dynamoOperations.databaseConfig.tableName,
      UpdateExpression: "SET #tasks = :tasks, #version = :nextVersion",
      ConditionExpression: "#version = :version",
      ExpressionAttributeNames: {
        "#version": "version",
        "#tasks": "tasks",
      },
      ExpressionAttributeValues: {
        ":version": { S: columnRecord.version },
        ":tasks": marshalled.tasks,
        ":nextVersion": { S: nanoid() },
      },
    };
  }

  private findBoardTaskAssignment(board: Board, taskId: string) {
    return this.dynamoOperations.dynamoDb
      .getItem({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        Key: {
          gid: { S: toBoardTaskAssignmentGid(board.id, taskId) },
          resource: { S: "board-task-assignment" },
        },
      })
      .promise()
      .then((response) =>
        unmarshallRecord<BoardTaskAssignmentRecord>(response.Item!)
      );
  }

  public async deleteBoard(boardId: string): Promise<any> {
    const boardRecord = await this.dynamoOperations.find(toBoardGid(boardId));
    const boardColumnRecords = await this.findBoardColumnRecords(boardRecord);

    await this.deleteBoardRecord(boardRecord, boardColumnRecords);
    await this.deleteTaskAssignmentRecords(boardRecord, boardColumnRecords);
  }

  private deleteBoardRecord(
    boardRecord: BoardRecord,
    columnRecords: BoardColumnRecord[]
  ): Promise<any> {
    const columnRecordsDeleteRequests: TransactWriteItemList =
      columnRecords.map((columnRecord) => ({
        Delete: {
          ConditionExpression: "#version = :version",
          ExpressionAttributeNames: {
            "#version": "version",
          },
          ExpressionAttributeValues: {
            ":version": { S: columnRecord.version },
          },
          TableName: this.dynamoOperations.databaseConfig.tableName,
          Key: {
            gid: { S: columnRecord.gid },
            resource: { S: columnRecord.resource },
          },
        },
      }));

    return this.dynamoOperations.dynamoDb
      .transactWriteItems({
        TransactItems: columnRecordsDeleteRequests.concat([
          {
            Delete: {
              ConditionExpression: "#version = :version",
              ExpressionAttributeNames: {
                "#version": "version",
              },
              ExpressionAttributeValues: {
                ":version": { S: boardRecord.version },
              },
              TableName: this.dynamoOperations.databaseConfig.tableName,
              Key: {
                gid: { S: boardRecord.gid },
                resource: { S: boardRecord.resource },
              },
            },
          },
        ]),
      })
      .promise();
  }

  private saveBoardRecord(
    record: BoardRecord,
    columnRecords: BoardColumnRecord[]
  ): Promise<any> {
    const columnRecordsPutRequests: TransactWriteItemList = columnRecords.map(
      (columnRecord) => ({
        Put: {
          TableName: this.dynamoOperations.databaseConfig.tableName,
          Item: marshallRecord(columnRecord),
        },
      })
    );
    return this.dynamoOperations.dynamoDb
      .transactWriteItems({
        TransactItems: columnRecordsPutRequests.concat([
          {
            Put: {
              TableName: this.dynamoOperations.databaseConfig.tableName,
              Item: marshallRecord(record),
            },
          },
        ]),
      })
      .promise();
  }

  private findBoardColumnRecords(boardRecord: BoardRecord) {
    return this.dynamoOperations.dynamoDb
      .query({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        IndexName: this.dynamoOperations.databaseConfig.parentResourceIndex,
        KeyConditionExpression:
          "#parentGid = :parentGid and #resource = :resource",
        ExpressionAttributeNames: {
          "#parentGid": "parentGid",
          "#resource": "resource",
        },
        ExpressionAttributeValues: {
          ":parentGid": { S: boardRecord.gid },
          ":resource": { S: "board-column" },
        },
      })
      .promise()
      .then((response) =>
        response.Items!.map((item) => unmarshallRecord<BoardColumnRecord>(item))
      );
  }

  private deleteTaskAssignmentRecords(
    boardRecord: BoardRecord,
    boardColumnRecords: BoardColumnRecord[]
  ): Promise<any> {
    const boardTaskAssignmentGid = boardColumnRecords
      .map((boardColumnRecord) =>
        boardColumnRecord.tasks.map((taskIdentifer) =>
          toBoardTaskAssignmentGid(boardRecord.id, taskIdentifer.id)
        )
      )
      .reduce((left, right) => left.concat(right), []);

    const taskAssignmentsGidPacks = divideIntoPacks(
      boardTaskAssignmentGid,
      MAX_TRANSACT_WRITE_ITEMS_COUNT
    );

    const taskAssignmentsWriteRequests: TransactWriteItem[][] =
      taskAssignmentsGidPacks.map((taskAssignmentsGidPack) =>
        taskAssignmentsGidPack.map((taskAssignmentGid) => ({
          Delete: {
            Key: {
              gid: { S: taskAssignmentGid },
              resource: { S: "board-task-assignment" },
            },
            TableName: this.dynamoOperations.databaseConfig.tableName,
          },
        }))
      );
    return Promise.all(
      taskAssignmentsWriteRequests.map((requests) =>
        this.dynamoOperations.dynamoDb
          .transactWriteItems({
            TransactItems: requests,
          })
          .promise()
      )
    );
  }

  private saveTaskAssignmentRecords(
    taskAssignmentsRecords: BoardTaskAssignmentRecord[]
  ): Promise<any> {
    const taskAssignmentsPacks = divideIntoPacks(
      taskAssignmentsRecords,
      MAX_TRANSACT_WRITE_ITEMS_COUNT
    );
    const taskAssignmentsWriteRequests: TransactWriteItem[][] =
      taskAssignmentsPacks.map((taskAssignmentsPack) =>
        taskAssignmentsPack.map((assignmentRecord) => ({
          Put: {
            Item: marshallRecord(assignmentRecord),
            TableName: this.dynamoOperations.databaseConfig.tableName,
          },
        }))
      );
    return Promise.all(
      taskAssignmentsWriteRequests.map((requests) =>
        this.dynamoOperations.dynamoDb
          .transactWriteItems({
            TransactItems: requests,
          })
          .promise()
      )
    );
  }
}

function allTaskIds(boardColumnRecords: BoardColumnRecord[]) {
  return boardColumnRecords
    .map((columnRecord) =>
      columnRecord.tasks.map((taskIdentifier) => taskIdentifier.id)
    )
    .reduce((left, right) => left.concat(right));
}

function toBoard(boardColumns: BoardColumn[], boardRecord: BoardRecord): Board {
  return {
    columns: boardColumns,
    id: boardRecord.id,
    teamId: boardRecord.teamId,
    name: boardRecord.name,
    version: boardRecord.version,
  };
}

function toBoardColumn(
  boardDefinition: BoardDefinition,
  columnRecord: BoardColumnRecord,
  allTasks: TaskListingItem[]
): BoardColumn {
  const foundStatus = boardDefinition.statuses.find(
    (status) => status.id === columnRecord.statusId
  );
  const tasks = columnRecord.tasks.map(
    (identifier) => allTasks.find((task) => task.id === identifier.id)!
  );
  return {
    status: foundStatus!,
    tasks,
    version: columnRecord.version,
  };
}

function toBoardRecord(
  boardDefinition: BoardDefinition,
  board: Board
): BoardRecord {
  return {
    gid: toBoardGid(board.id),
    id: board.id,
    resource: "board",
    parentGid: toTeamGid(boardDefinition.teamId),
    teamId: boardDefinition.teamId,
    name: board.name,
    boardDefinitionId: boardDefinition.id,
    targetGid: toBoardDefnitionGid(boardDefinition.id),
    version: board.version,
  };
}
function toBoardColumnRecord(
  board: Board,
  column: BoardColumn
): BoardColumnRecord {
  return {
    gid: toBoardColumnGid(board, column),
    resource: "board-column",
    id: nanoid(),
    parentGid: toBoardGid(board.id),
    statusId: column.status.id,
    tasks: column.tasks.map((task) => ({
      id: task.id,
      gid: toTaskGid(board.teamId, task.id),
    })),
    version: column.version,
  };
}

function toBoardColumnGid(board: Board, column: BoardColumn): string {
  return `board-column:${hashGid([board.id, column.status.id].join("_"))}`;
}

function toTaskAssignmentsRecords(board: Board): BoardTaskAssignmentRecord[] {
  return board.columns
    .map((column) =>
      column.tasks.map((task) =>
        toBoardTaskAssignmentRecord(board, column, task)
      )
    )
    .reduce((left, right) => left.concat(right), []);
}

function toBoardTaskAssignmentRecord(
  board: Board,
  column: BoardColumn,
  task: TaskListingItem
): BoardTaskAssignmentRecord {
  return {
    gid: toBoardTaskAssignmentGid(board.id, task.id),
    resource: "board-task-assignment",
    boardId: board.id,
    parentGid: toBoardGid(board.id),
    taskId: task.id,
    targetGid: toTaskGid(board.teamId, task.id),
    statusId: column.status.id,
    version: nanoid(),
  };
}

function toBoardTaskAssignmentGid(boardId: string, taskId: string): string {
  return `board-task-assignment:${hashGid([boardId, taskId].join("_"))}`;
}

function divideIntoPacks<T>(items: T[], quantity: number): T[][] {
  const packsNumber = Math.ceil(items.length / quantity);
  const indices = Array.from(Array(packsNumber).keys());
  return indices
    .map((index) => [index * quantity, (index + 1) * quantity])
    .map(([start, end]) => items.slice(start, end));
}

function toBoardGid(boardId: string) {
  return `board:${boardId}`;
}
