import { Team } from "../../../../../domain/teams";
import { Project } from "../../../../../domain/projects";
import { Task, TaskListingItem } from "../../../../../domain/tasks";
import nanoid = require("nanoid");
import { User } from "../../../../../domain/users";
import { Board, BoardColumn } from "../../../../../domain/tasks/boards";
import { someTeam } from "../../../../../domain/teams/__tests__/team.fixtures";
import { EntityNotFoundError } from "../../../../../domain/errors/EntityNotFoundError";
import { testContext } from "../../../../../__tests__/test-context";
import { someProject } from "../../../../../domain/projects/__tests__/project.fixtures";
import {
  someInProgressStatus,
  someTodoStatus,
} from "../../../../../domain/tasks/boards/__tests__/board-definition.fixtures";
import { someUser } from "../../../../../domain/users/__tests__/users.fixtures";
import { freezeTimeAt } from "../../../../../libs/time/__tests__/test-time";

describe("Board repository tests", () => {
  const {
    teamsRepository,
    usersRepository,
    tasksRepository,
    boardRepository,
    boardDefinitionsRepository,
    projectsRepository,
  } = testContext;

  test("Test save & read board", async () => {
    // given:
    freezeTimeAt(new Date("2012-12-12:12:00:00Z"));
    const team: Team = await teamsRepository.save({
      ...someTeam(),
      id: nanoid(),
    });
    const project: Project = await projectsRepository.save({
      ...someProject(),
      id: nanoid(),
      teamId: team.id,
    });
    const user: User = await usersRepository.save(someUser("john.doe"));

    const task1: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-1",
      project,
      participants: [{ role: "author", user }],
    });
    const task2: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-2",
      project,
      participants: [{ role: "author", user }],
    });
    const task3: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-3",
      project,
      participants: [{ role: "author", user }],
    });

    const todoStatus = someTodoStatus();
    const inProgressStatus = someInProgressStatus();
    const boardDefinition = await boardDefinitionsRepository.save({
      id: nanoid(),
      author: user,
      name: "Some board",
      statuses: [todoStatus, inProgressStatus],
      teamId: team.id,
    });

    const board: Board = {
      id: nanoid(),
      name: "Some board",
      teamId: team.id,
      columns: [
        {
          status: todoStatus,
          tasks: [toTaskEntry(task1), toTaskEntry(task2)],
          version: nanoid(),
        },
        {
          status: inProgressStatus,
          tasks: [toTaskEntry(task3)],
          version: nanoid(),
        },
      ],
      version: nanoid(),
    };

    // when:
    await boardRepository.save(boardDefinition, board);

    // then:
    const foundBoard = await boardRepository.find(board.id);
    expect(foundBoard).toEqual(board);
  });

  test("Test save & delete board", async () => {
    // given:
    const team: Team = await teamsRepository.save({
      ...someTeam(),
      id: nanoid(),
    });
    const project: Project = await projectsRepository.save({
      ...someProject(),
      id: nanoid(),
      teamId: team.id,
    });
    const user: User = await usersRepository.save(someUser("john.doe"));

    const task1: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-1",
      project,
      participants: [{ role: "author", user }],
    });

    const todoStatus = someTodoStatus();
    const boardDefinition = await boardDefinitionsRepository.save({
      id: nanoid(),
      author: user,
      name: "Some board",
      statuses: [todoStatus],
      teamId: team.id,
    });

    const board: Board = {
      id: nanoid(),
      name: "Some board",
      teamId: team.id,
      columns: [
        {
          status: todoStatus,
          tasks: [toTaskEntry(task1)],
          version: nanoid(),
        },
      ],
      version: nanoid(),
    };
    await boardRepository.save(boardDefinition, board);

    // when:
    await boardRepository.deleteBoard(board.id);
    // then:
    const foundBoardPromise = boardRepository.find(board.id);
    await expect(foundBoardPromise).rejects.toBeInstanceOf(EntityNotFoundError);
  });

  test("Test list boards", async () => {
    // given:
    const team: Team = await teamsRepository.save({
      ...someTeam(),
      id: nanoid(),
    });
    const project: Project = await projectsRepository.save({
      ...someProject(),
      id: nanoid(),
      teamId: team.id,
    });
    const user: User = await usersRepository.save(someUser("john.doe"));

    const task1: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-1",
      project,
      participants: [{ role: "author", user }],
    });

    const todoStatus = someTodoStatus();
    const boardDefinition = await boardDefinitionsRepository.save({
      id: nanoid(),
      author: user,
      name: "Some board",
      statuses: [todoStatus],
      teamId: team.id,
    });

    const board1: Board = {
      id: nanoid(),
      teamId: team.id,
      name: "board-1",
      columns: [
        {
          status: todoStatus,
          tasks: [toTaskEntry(task1)],
          version: nanoid(),
        },
      ],
      version: nanoid(),
    };
    const board2: Board = {
      id: nanoid(),
      teamId: team.id,
      name: "board-2",
      columns: [
        {
          status: todoStatus,
          tasks: [toTaskEntry(task1)],
          version: nanoid(),
        },
      ],
      version: nanoid(),
    };
    await boardRepository.save(boardDefinition, board1);
    await boardRepository.save(boardDefinition, board2);

    // when:
    const foundBoards = await boardRepository
      .listByTeamId(team.id)
      .then((items) =>
        items.sort((item1, item2) => item1.name.localeCompare(item2.name))
      );
    // then:
    expect(foundBoards).toEqual([
      {
        id: board1.id,
        name: board1.name,
      },
      {
        id: board2.id,
        name: board2.name,
      },
    ]);
  });

  test("Test move task between columns", async () => {
    // given:
    const team: Team = await teamsRepository.save({
      ...someTeam(),
      id: nanoid(),
    });
    const project: Project = await projectsRepository.save({
      ...someProject(),
      id: nanoid(),
      teamId: team.id,
    });
    const user: User = await usersRepository.save(someUser("john.doe"));

    const task1: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-1",
      project,
      participants: [{ role: "author", user }],
    });
    const task2: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-2",
      project,
      participants: [{ role: "author", user }],
    });

    const todoStatus = someTodoStatus();
    const inProgressStatus = someInProgressStatus();
    const boardDefinition = await boardDefinitionsRepository.save({
      id: nanoid(),
      author: user,
      name: "Some board",
      statuses: [todoStatus, inProgressStatus],
      teamId: team.id,
    });

    const todoColumn: BoardColumn = {
      status: todoStatus,
      tasks: [toTaskEntry(task1), toTaskEntry(task2)],
      version: nanoid(),
    };
    const inProgressColumn: BoardColumn = {
      status: inProgressStatus,
      tasks: [],
      version: nanoid(),
    };
    const board: Board = await boardRepository.save(boardDefinition, {
      id: nanoid(),
      name: "Some board",
      teamId: team.id,
      columns: [todoColumn, inProgressColumn],
      version: nanoid(),
    });

    // when:
    await boardRepository.saveTaskTransition(board, task2.id, [
      { ...todoColumn, tasks: [toTaskEntry(task1)] },
      { ...inProgressColumn, tasks: [toTaskEntry(task2)] },
    ]);

    // then:
    const foundBoard = await boardRepository.find(board.id);
    const columns = foundBoard.columns.map((column) => [
      column.status.id,
      column.tasks.map((task) => task.id),
    ]);
    expect(columns).toEqual([
      [todoStatus.id, [task1.id]],
      [inProgressStatus.id, [task2.id]],
    ]);
  });

  test("Test save board column", async () => {
    // given:
    const team: Team = await teamsRepository.save({
      ...someTeam(),
      id: nanoid(),
    });
    const project: Project = await projectsRepository.save({
      ...someProject(),
      id: nanoid(),
      teamId: team.id,
    });
    const user: User = await usersRepository.save(someUser("john.doe"));

    const task1: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-1",
      project,
      participants: [{ role: "author", user }],
    });
    const task2: Task = await tasksRepository.save({
      ...someTask(team.id),
      id: "BLU-2",
      project,
      participants: [{ role: "author", user }],
    });

    const todoStatus = someTodoStatus();
    const inProgressStatus = someInProgressStatus();
    const boardDefinition = await boardDefinitionsRepository.save({
      id: nanoid(),
      author: user,
      name: "Some board",
      statuses: [todoStatus, inProgressStatus],
      teamId: team.id,
    });

    const todoColumn: BoardColumn = {
      status: todoStatus,
      tasks: [toTaskEntry(task1), toTaskEntry(task2)],
      version: nanoid(),
    };
    const inProgressColumn: BoardColumn = {
      status: inProgressStatus,
      tasks: [],
      version: nanoid(),
    };
    const board: Board = await boardRepository.save(boardDefinition, {
      id: nanoid(),
      name: "Some board",
      teamId: team.id,
      columns: [todoColumn, inProgressColumn],
      version: nanoid(),
    });

    // when:
    await boardRepository.saveBoardColumn(board, {
      ...todoColumn,
      tasks: [toTaskEntry(task2), toTaskEntry(task1)],
    });

    // then:
    const foundBoard = await boardRepository.find(board.id);
    const columns = foundBoard.columns.map((column) => [
      column.status.id,
      column.tasks.map((task) => task.id),
    ]);
    expect(columns).toEqual([
      [todoStatus.id, [task2.id, task1.id]],
      [inProgressStatus.id, []],
    ]);
  });
});

function someTask(teamId: string): Task {
  return {
    id: "BLU-1",
    participants: [
      {
        role: "author",
        user: {
          username: "john.doe",
          email: "john.doe@email.test",
          avatarUrl: "http://avatars.com/avatar/john.doe",
        },
      },
    ],
    content: "some content",
    createdAt: "2012-12-12:12:00:00.000Z",
    project: {
      id: "project:id",
      slug: "BLU",
      name: "Project BLU",
      teamId,
    },
    summary: "As a user I want to create task",
    type: "user-story",
    resolved: false,
  };
}

export function toTaskEntry(task: Task): TaskListingItem {
  return {
    id: task.id,
    type: task.type,
    createdAt: task.createdAt,
    participants: task.participants,
    summary: task.summary,
    resolved: false,
  };
}
