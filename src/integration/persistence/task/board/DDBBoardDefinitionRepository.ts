import { Record } from "../../../dynamodb";
import {
  DynamoOperations,
  unmarshallRecord,
} from "../../../dynamodb/DynamoOperations";
import { DynamoDB } from "aws-sdk";
import { UserRepository } from "../../../../domain/users/UserRepository";
import { User } from "../../../../domain/users";
import {
  StatusDefinitionColor,
  BoardDefinition,
  TaskStatusDefinition,
} from "../../../../domain/tasks/boards";
import { toTeamGid } from "../../team/DDBTeamRepository";
import { DDBProperties } from "../../../../domain/env";
import { toUserGid } from "../../user/DDBUserRepository";
import { BoardDefinitionRepository } from "../../../../domain/tasks/boards/BoardDefinitionRepository";

interface BoardDefinitionRecord extends Record {
  resource: "board-definition";
  parentGid: string;
  id: string;
  name: string;
  teamId: string;
  author: string;
  authorGid: string;
  statuses: TaskStatusDefinitionRecord[];
}

interface TaskStatusDefinitionRecord {
  id: string;
  name: string;
  color: StatusDefinitionColor;
  resolving: boolean;
}

export class DDBBoardDefinitionRepository implements BoardDefinitionRepository {
  private readonly dynamoOperations: DynamoOperations<BoardDefinitionRecord>;
  private readonly usersRepository: UserRepository;

  constructor(
    dynamoDb: DynamoDB,
    usersRepository: UserRepository,
    config: DDBProperties
  ) {
    this.dynamoOperations = new DynamoOperations<BoardDefinitionRecord>(
      dynamoDb,
      "board-definition",
      config
    );
    this.usersRepository = usersRepository;
  }

  public save(item: BoardDefinition): Promise<BoardDefinition> {
    const record = toBoardDefinitionRecord(item);
    return this.dynamoOperations.save(record).then(() => item);
  }

  public async findByTeamId(teamId: string): Promise<BoardDefinition[]> {
    const teamGid = toTeamGid(teamId);
    const boardDefinitionRecords = await this.dynamoOperations.dynamoDb
      .query({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        IndexName: this.dynamoOperations.databaseConfig.parentResourceIndex,
        KeyConditionExpression:
          "#parentGid = :parentGid and #resource = :resource",
        ExpressionAttributeNames: {
          "#parentGid": "parentGid",
          "#resource": "resource",
        },
        ExpressionAttributeValues: {
          ":resource": { S: "board-definition" },
          ":parentGid": { S: teamGid },
        },
      })
      .promise()
      .then((response) =>
        response.Items!.map((item) =>
          unmarshallRecord<BoardDefinitionRecord>(item)
        )
      );

    const userIds = [
      ...new Set(
        boardDefinitionRecords.map(
          (boardDefinitionRecord) => boardDefinitionRecord.author
        )
      ),
    ];
    const allUsers = await this.usersRepository.findByIds(userIds);
    return boardDefinitionRecords.map((boardDefinitionRecord) => {
      const author = allUsers.find(
        (user) => user.username === boardDefinitionRecord.author
      );
      return toBoardDefinition(boardDefinitionRecord, author!);
    });
  }

  public find(id: string): Promise<BoardDefinition> {
    return this.dynamoOperations
      .find(toBoardDefnitionGid(id))
      .then((record) =>
        this.usersRepository
          .find(record.author)
          .then((user) => toBoardDefinition(record, user))
      );
  }
}

function toBoardDefinitionRecord(
  definition: BoardDefinition
): BoardDefinitionRecord {
  return {
    gid: toBoardDefnitionGid(definition.id),
    id: definition.id,
    resource: "board-definition",
    parentGid: toTeamGid(definition.teamId),
    teamId: definition.teamId,
    authorGid: toUserGid(definition.author.username),
    author: definition.author.username,
    name: definition.name,
    statuses: definition.statuses.map((status) =>
      toTaskDefinitionStatusRecord(status)
    ),
  };
}

function toBoardDefinition(
  definition: BoardDefinitionRecord,
  user: User
): BoardDefinition {
  return {
    id: definition.id,
    author: user,
    name: definition.name,
    teamId: definition.teamId,
    statuses: definition.statuses.map((status) =>
      toTaskDefinitionStatus(status)
    ),
  };
}

function toTaskDefinitionStatusRecord(
  status: TaskStatusDefinition
): TaskStatusDefinitionRecord {
  return {
    id: status.id,
    color: status.color,
    name: status.name,
    resolving: status.resolving,
  };
}
function toTaskDefinitionStatus(
  status: TaskStatusDefinitionRecord
): TaskStatusDefinition {
  return {
    id: status.id,
    color: status.color,
    name: status.name,
    resolving: status.resolving,
  };
}

export function toBoardDefnitionGid(definitionId: string) {
  return `board-definition:${definitionId}`;
}
