import { DynamoOperations } from "../../dynamodb/DynamoOperations";
import { Record } from "../../dynamodb";
import { DynamoDB } from "aws-sdk";
import { User } from "../../../domain/users";
import { DDBProperties } from "../../../domain/env";
import { UserRepository } from "../../../domain/users/UserRepository";

interface UserRecord extends Record {
  resource: "user";
  username: string;
  email: string;
  avatarUrl: string;
}

export class DDBUserRepository implements UserRepository {
  private readonly dynamoOperations: DynamoOperations<UserRecord>;

  constructor(dynamoDb: DynamoDB, config: DDBProperties) {
    this.dynamoOperations = new DynamoOperations<UserRecord>(
      dynamoDb,
      "user",
      config
    );
  }

  public save(item: User): Promise<User> {
    const record = toUserRecord(item);
    return this.dynamoOperations.save(record).then(() => item);
  }
  public find(username: string): Promise<User> {
    const gid = toUserGid(username);
    return this.dynamoOperations.find(gid).then((record) => toUser(record));
  }
  public tryFind(username: string): Promise<User | null> {
    const gid = toUserGid(username);
    return this.dynamoOperations
      .tryFind(gid)
      .then((record) => (!!record ? toUser(record) : null));
  }
  public findByIds(usernames: string[]): Promise<User[]> {
    const gids = usernames.map((username) => toUserGid(username));
    return this.dynamoOperations
      .findByGids(gids)
      .then((records) => records.map((record) => toUser(record)));
  }
  public delete(username: string): Promise<void> {
    const gid = toUserGid(username);
    return this.dynamoOperations.delete(gid);
  }
}

export function toUserGid(username: string): string {
  return `user:${username}`;
}

export function toUser(record: UserRecord): User {
  return {
    username: record.username,
    email: record.email,
    avatarUrl: record.avatarUrl,
  };
}

export function toUserRecord(user: User): UserRecord {
  return {
    gid: toUserGid(user.username),
    resource: "user",
    username: user.username,
    email: user.email,
    avatarUrl: user.avatarUrl,
  };
}
