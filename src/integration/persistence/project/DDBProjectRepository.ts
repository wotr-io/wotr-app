import { Project, ProjectStats } from "../../../domain/projects";
import { DynamoDB } from "aws-sdk";
import { Record, hashGid } from "../../dynamodb";
import {
  DynamoOperations,
  marshallRecord,
  unmarshallRecord,
} from "../../dynamodb/DynamoOperations";
import { toTeamGid } from "../team/DDBTeamRepository";
import { ProjectSlugConflictError } from "../../../domain/projects/ProjectSlugConflictError";
import { DDBProperties } from "../../../domain/env";
import { ProjectRepository } from "../../../domain/projects/ProjectRepository";

interface ProjectRecord extends Record {
  resource: "project";
  id: string;
  name: string;
  slug: string;
  teamId: string;
}

interface ProjectSlugRecord extends Record {
  resource: "project-slug";
  projectId: string;
  slug: string;
}

interface ProjectStatsRecord extends Record {
  resource: "project-stats";
  projectId: string;
  totalTasks: number;
}

export class DDBProjectRepository implements ProjectRepository {
  private readonly dynamoOperations: DynamoOperations<ProjectRecord>;
  private readonly slugsDynamoOperations: DynamoOperations<ProjectSlugRecord>;

  constructor(dynamoDb: DynamoDB, config: DDBProperties) {
    this.dynamoOperations = new DynamoOperations<ProjectRecord>(
      dynamoDb,
      "project",
      config
    );
    this.slugsDynamoOperations = new DynamoOperations<ProjectSlugRecord>(
      dynamoDb,
      "project-slug",
      config
    );
  }

  public findByTeamId(teamId: string): Promise<Project[]> {
    return this.dynamoOperations.dynamoDb
      .query({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        IndexName: this.dynamoOperations.databaseConfig.parentResourceIndex,
        KeyConditionExpression:
          "parentGid = :teamGid AND #resource = :resource",
        ExpressionAttributeValues: {
          ":teamGid": { S: toTeamGid(teamId) },
          ":resource": { S: "project" },
        },
        ExpressionAttributeNames: {
          "#resource": "resource",
        },
      })
      .promise()
      .then((results) =>
        results.Items!.map((record) =>
          toProject(unmarshallRecord<ProjectRecord>(record))
        )
      );
  }

  public findStatsByTeamId(teamId: string): Promise<ProjectStats[]> {
    return this.dynamoOperations.dynamoDb
      .query({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        IndexName: this.dynamoOperations.databaseConfig.parentResourceIndex,
        KeyConditionExpression:
          "parentGid = :teamGid AND #resource = :resource",
        ExpressionAttributeValues: {
          ":teamGid": { S: toTeamGid(teamId) },
          ":resource": { S: "project-stats" },
        },
        ExpressionAttributeNames: {
          "#resource": "resource",
        },
      })
      .promise()
      .then((results) =>
        results.Items!.map((record) =>
          toProjectStats(unmarshallRecord<ProjectStatsRecord>(record))
        )
      );
  }

  public findStatsByProjectId(projectId: string): Promise<ProjectStats> {
    return this.dynamoOperations.dynamoDb
      .getItem({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        Key: {
          gid: { S: toProjectGid(projectId) },
          resource: { S: "project-stats" },
        },
      })
      .promise()
      .then((results) =>
        toProjectStats(unmarshallRecord<ProjectStatsRecord>(results.Item!))
      );
  }

  public projectExists(teamId: string, slug: string): Promise<boolean> {
    return this.dynamoOperations.dynamoDb
      .getItem({
        TableName: this.dynamoOperations.databaseConfig.tableName,
        Key: {
          gid: {
            S: toProjectSlugGid(teamId, slug),
          },
          resource: { S: "project-slug" },
        },
      })
      .promise()
      .then((response) => !!response.Item);
  }

  public save(project: Project): Promise<Project> {
    const projectRecord: ProjectRecord = toProjectRecord(project);
    const slugRecord: ProjectSlugRecord = toProjectSlugRecord(project);
    const statsRecord: ProjectStatsRecord = toProjectStatsRecord(project, {
      totalTasks: 0,
      projectId: project.id,
    });
    return this.dynamoOperations.dynamoDb
      .transactWriteItems({
        TransactItems: [
          {
            Put: {
              TableName: this.dynamoOperations.databaseConfig.tableName,
              Item: marshallRecord(slugRecord),
              ConditionExpression: "attribute_not_exists(gid)",
            },
          },
          {
            Put: {
              TableName: this.dynamoOperations.databaseConfig.tableName,
              Item: marshallRecord(projectRecord),
            },
          },
          {
            Put: {
              TableName: this.dynamoOperations.databaseConfig.tableName,
              Item: marshallRecord(statsRecord),
            },
          },
        ],
      })
      .promise()
      .then(() => project, captureTransactionError);
  }

  public find(id: string): Promise<Project> {
    return this.dynamoOperations.find(toProjectGid(id)).then(toProject);
  }

  public async findBySlug(teamId: string, slug: string): Promise<Project> {
    const slugGid = toProjectSlugGid(teamId, slug);
    const slugRecord = await this.slugsDynamoOperations.find(slugGid);
    return this.dynamoOperations.find(slugRecord.targetGid!).then(toProject);
  }

  public tryFind(id: string): Promise<Project | null> {
    return this.dynamoOperations
      .tryFind(toProjectGid(id))
      .then((record) => (!!record ? toProject(record) : null));
  }

  public findByIds(ids: string[]): Promise<Project[]> {
    const gids = ids.map((id) => toProjectGid(id));
    return this.dynamoOperations
      .findByGids(gids)
      .then((records) => records.map((record) => toProject(record)));
  }

  public delete(id: string): Promise<void> {
    return this.dynamoOperations.delete(toProjectGid(id));
  }
}

function toProjectRecord(project: Project): ProjectRecord {
  return {
    gid: toProjectGid(project.id),
    resource: "project",
    parentGid: toTeamGid(project.teamId),
    id: project.id,
    name: project.name,
    slug: project.slug,
    teamId: project.teamId,
  };
}

function toProject(record: ProjectRecord): Project {
  return {
    id: record.id,
    teamId: record.teamId,
    name: record.name,
    slug: record.slug,
  };
}

function toProjectStatsRecord(
  project: Project,
  stats: ProjectStats
): ProjectStatsRecord {
  return {
    gid: toProjectGid(project.id),
    resource: "project-stats",
    totalTasks: stats.totalTasks,
    projectId: project.id,
    parentGid: toTeamGid(project.teamId),
  };
}

function toProjectStats(record: ProjectStatsRecord): ProjectStats {
  return {
    projectId: record.projectId,
    totalTasks: record.totalTasks,
  };
}

function toProjectSlugRecord(project: Project): ProjectSlugRecord {
  return {
    gid: toProjectSlugGid(project.teamId, project.slug),
    resource: "project-slug",
    parentGid: toTeamGid(project.teamId),
    targetGid: toProjectGid(project.id),
    projectId: project.id,
    slug: project.slug,
  };
}

function toProjectSlugGid(teamId: string, projectSlug: string) {
  const projectSlugId = [teamId, projectSlug].join("_");
  return `project-slug:${hashGid(projectSlugId)}`;
}

export function toProjectGid(id: string): string {
  return `project:${id}`;
}

function captureTransactionError(err: any): Promise<Project> {
  if (err.code && err.code === "TransactionCanceledException") {
    return Promise.reject(new ProjectSlugConflictError());
  }
  return Promise.reject(err);
}
