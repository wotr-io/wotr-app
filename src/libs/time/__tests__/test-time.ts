import { time } from "../../../libs/time/time";

export function freezeTimeAt(date: Date) {
  console.log("Freezing the time at: " + date.toISOString());
  time.__frozenTime = date;
}

export function resetTime() {
  time.__frozenTime = null;
}
