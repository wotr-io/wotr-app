interface Time {
  __frozenTime: Date | null;
  now(): Date;
}
export const time: Time = {
  __frozenTime: null,
  now(): Date {
    if (!!this.__frozenTime) {
      console.log(
        "Using the fixed time frozen at: " + this.__frozenTime.toISOString()
      );
      return this.__frozenTime;
    }
    return new Date();
  },
};
