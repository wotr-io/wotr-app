import { NextFunction, Request, Response } from "express";

export function asyncHandler<T = any>(
  handler: (req: Request<T>, res: Response, next: NextFunction) => Promise<any>
) {
  return (req: Request<T>, res: Response, next: NextFunction) => {
    handler(req, res, next).catch(next);
  };
}
