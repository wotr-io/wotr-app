import { Express } from "express";

export interface ExpressController {
  registerEndpoints(express: Express);
}
