import Ajv from "ajv";
import { ValidationError } from "./ValidationError";

export function validatePayload<T>(obj: T, schema: any): T {
  const ajv = new Ajv();
  const validationFn = ajv.compile(schema);
  const isValid = validationFn(obj);
  if (!isValid) {
    throw new ValidationError(validationFn.errors || []);
  }
  return obj;
}
