import { ErrorObject } from "ajv";

export class ValidationError extends Error {
  public readonly errors: ErrorObject[];

  constructor(errors: ErrorObject[]) {
    super("Validation error");
    this.errors = errors;
    Object.setPrototypeOf(this, ValidationError.prototype);
  }
}
