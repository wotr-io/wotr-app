import { ApiResponse, Client } from "@elastic/elasticsearch";
import AWSXRay = require("aws-xray-sdk-core");
import { XRayProperties } from "../../domain/env";

export function xrayMonitored(
  obj: Client,
  xrayProperties: XRayProperties
): Client {
  if (!xrayProperties.enabled) {
    return obj;
  }
  const xrayProxy = new Proxy(obj, {
    get(target, prop, handler) {
      const value = Reflect.get(target, prop, handler);

      return (...args) => {
        if (typeof value !== "function") {
          return value;
        }

        return AWSXRay.captureAsyncFunc(
          `ElasticSearch:${prop.toString()}`,
          (subsegment) => {
            const result = value(...args);
            if (!subsegment) {
              return result;
            }
            if (!(result instanceof Promise)) {
              subsegment.close();
              return result;
            }

            if (!!args[0] && typeof args[0] !== "function") {
              subsegment.addMetadata("RequestBody", args[0]);
            }
            return result
              .then(
                (response: ApiResponse<any>) => {
                  subsegment.addMetadata("ResponseStatus", response.statusCode);
                  return response;
                },
                (err) => {
                  subsegment.addError(err);
                  return Promise.reject(err);
                }
              )
              .finally(() => subsegment.close());
          }
        );
      };
    },
  });
  return xrayProxy;
}
