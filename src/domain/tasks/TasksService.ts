import { TaskRepository } from "./TaskRepository";
import { ProjectRepository } from "../projects/ProjectRepository";
import { WotrUser } from "../security/WotrUser";
import { TeamRepository } from "../teams/TeamRepository";
import {
  Task,
  TaskInput,
  ListTasksParameters,
  RecentTasksDashboard,
  TasksListing,
} from ".";
import { EntityNotFoundError } from "../errors/EntityNotFoundError";
import { checkPermission } from "../teams/permissions";
import { time } from "../../libs/time/time";
import { TasksIndex } from "./TasksIndex";
import { UserRepository } from "../users/UserRepository";
import { Team } from "../teams";
import { Project } from "../projects";

export class TasksService {
  constructor(
    private readonly teamsRepository: TeamRepository,
    private readonly projectsRepository: ProjectRepository,
    private readonly tasksRepository: TaskRepository,
    private readonly tasksIndex: TasksIndex,
    private readonly usersRepository: UserRepository
  ) {}

  public async listTasks(
    teamId: string,
    parameters: ListTasksParameters | null,
    user: WotrUser
  ): Promise<TasksListing> {
    console.log(
      `Listing tasks for the team: ${teamId} by user: ${user.username} with parameters: `,
      parameters
    );
    const tasks = !!parameters
      ? await this.tasksIndex.listTasks(teamId, parameters, [0, 20])
      : await this.tasksIndex.listTasksByTeamId(teamId, [0, 20]);
    return { tasks };
  }

  public async saveTask(
    teamId: string,
    projectId: string,
    jsonTask: TaskInput,
    loggedUser: WotrUser
  ): Promise<Task> {
    checkPermission("task", "create", loggedUser);
    const team = await this.teamsRepository.find(teamId);
    const project = await this.projectsRepository.find(projectId);

    if (team.id !== project.teamId) {
      throw new EntityNotFoundError("project", projectId);
    }
    console.log(
      `Saving new task: "${jsonTask.summary}" in project: ${project.id} and team: ${teamId}, created by the user: ${loggedUser.username}`
    );
    const stats = await this.projectsRepository.findStatsByProjectId(projectId);
    const user = await this.usersRepository.find(loggedUser.username);
    const task: Task = {
      id: `${project.slug}-${stats.totalTasks + 1}`,
      participants: [{ role: "author", user }],
      content: jsonTask.content,
      project,
      summary: jsonTask.summary,
      type: jsonTask.type,
      createdAt: time.now().toISOString(),
      resolved: false,
    };
    const savedTask = await this.tasksRepository.save(task);
    await this.tasksIndex.save(task);
    console.log(`Task ${task.id} was successfully saved.`);
    return savedTask;
  }

  public async updateTask(
    teamId: string,
    taskId: string,
    taskInput: TaskInput,
    loggedUser: WotrUser
  ): Promise<Task> {
    checkPermission("task", "update", loggedUser);
    const [projectSlug] = taskId.split("-");
    const team = await this.teamsRepository.find(teamId);
    const project = await this.projectsRepository.findBySlug(
      teamId,
      projectSlug
    );
    checkProjectTeam(team, project);
    console.log(
      `Updating task: ${taskId} in project: ${project.id} and team: ${teamId} by user: ${loggedUser.username}`
    );

    const foundTask = await this.tasksRepository.find(team.id, taskId);
    const updatedTask: Task = {
      ...foundTask,
      type: taskInput.type,
      summary: taskInput.summary,
      content: taskInput.content,
    };
    const savedTask = await this.tasksRepository.update(updatedTask);
    await this.tasksIndex.save(updatedTask);
    return savedTask;
  }

  public async findTaskById(
    teamId: string,
    taskId: string,
    user: WotrUser
  ): Promise<Task> {
    checkPermission("task", "read", user);
    const [projectSlug] = taskId.split("-");
    console.log(
      `Find task by identifier: ${taskId} in the team: ${teamId} by the user: ${user.username}`
    );
    const team = await this.teamsRepository.find(teamId);
    const project = await this.projectsRepository.findBySlug(
      teamId,
      projectSlug
    );

    if (team.id !== project.teamId) {
      throw new EntityNotFoundError("project-slug", projectSlug);
    }
    return this.tasksRepository.find(team.id, taskId);
  }

  public async readRecentTasksDashboard(
    teamId: string,
    user: WotrUser
  ): Promise<RecentTasksDashboard> {
    console.log(
      `Reading recent tasks dashboard for team: ${teamId} and user: ${user.username}`
    );
    return this.tasksIndex.readRecentTasksDashboard(teamId, new Date());
  }
}
function checkProjectTeam(team: Team, project: Project) {
  if (team.id !== project.teamId) {
    throw new EntityNotFoundError("project-slug", project.slug);
  }
}
