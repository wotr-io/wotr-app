import { Project } from "../projects";
import { User } from "../users";

export interface Task {
  id: string;
  project: Project;
  participants: TaskParticipant[];
  summary: string;
  content: string;
  type: TaskType;
  createdAt: string;
  resolved: boolean;
}

export type TaskType = "user-story" | "task" | "bug";

export interface TaskInput {
  summary: string;
  content: string;
  type: TaskType;
}

export interface ListTasksParameters {
  phrase?: string;
  author?: string;
  summary?: string;
  project?: string;
  content?: string;
  type?: TaskType;
  "createdAt.gte"?: string;
  "createdAt.lte"?: string;
}

export interface TaskParticipant {
  user: User;
  role: string;
}

export interface RecentTasksDashboard {
  todayTasks: TaskListingItem[];
  yesterdayTasks: TaskListingItem[];
  olderTasks: TaskListingItem[];
}

export interface TaskListingItem {
  id: string;
  summary: string;
  participants: TaskParticipant[];
  type: TaskType;
  resolved: boolean;
  createdAt: string;
}

export interface TasksListing {
  tasks: TaskListingItem[];
}
