import { Team } from "../../teams";
import { Project, ProjectStats } from "../../projects";
import { Task, TaskInput } from "..";
import { WotrUser } from "../../security/WotrUser";
import nanoid = require("nanoid");
import { time } from "../../../libs/time/time";
import { User } from "../../users";
import { testContext } from "../../../__tests__/test-context";
import { toTaskGid } from "../../../integration/persistence/task/DDBTaskRepository";
import { someLoggedUser, someUser } from "../../users/__tests__/users.fixtures";
import {
  freezeTimeAt,
  resetTime,
} from "../../../libs/time/__tests__/test-time";

const {
  teamsRepository,
  tasksService,
  tasksRepository,
  projectsRepository,
  usersRepository,
  esClient,
  configMap,
} = testContext;

test("Test save task", async () => {
  // given:
  freezeTimeAt(new Date("2012-12-12:12:00:00Z"));
  const team: Team = someTeam();
  await teamsRepository.save(team);

  const loggedUser: WotrUser = { ...someLoggedUser(), teams: [team.id] };

  const user: User = someUser("john.doe");
  await usersRepository.save(user);

  const project: Project = someProject(team);
  await projectsRepository.save(project);

  const jsonTask: TaskInput = {
    summary: "Some summary",
    content: "Some content",
    type: "user-story",
  };

  // when:
  const createdTask = await tasksService.saveTask(
    team.id,
    project.id,
    jsonTask,
    loggedUser
  );

  // then:
  const expectedTask: Task = {
    id: "BLU-1",
    participants: [{ role: "author", user }],
    content: "Some content",
    summary: "Some summary",
    createdAt: "2012-12-12T12:00:00.000Z",
    project,
    type: "user-story",
    resolved: false,
  };
  expect(createdTask).toEqual(expectedTask);

  const savedTask = await tasksRepository.find(team.id, createdTask.id);
  expect(createdTask).toEqual(savedTask);

  // and:
  const stats = await projectsRepository.findStatsByProjectId(project.id);
  const expectedStats: ProjectStats = {
    projectId: project.id,
    totalTasks: 1,
  };
  expect(stats).toEqual(expectedStats);

  // and:
  const savedDocument = await esClient.get({
    index: configMap.elasticsearch.indexName,
    id: toTaskGid(team.id, expectedTask.id),
  });
  expect(savedDocument.body._source.task.id).toEqual(savedTask.id);
});

test("Test find task by id", async () => {
  // given:
  freezeTimeAt(new Date("2012-12-12:12:00:00Z"));
  const team: Team = await teamsRepository.save({
    ...someTeam(),
    id: nanoid(),
  });

  const loggedUser: WotrUser = { ...someLoggedUser(), teams: [team.id] };
  const user: User = await usersRepository.save(someUser("john.doe"));

  const project: Project = await projectsRepository.save(someProject(team));

  const task: Task = {
    id: "BLU-1",
    createdAt: time.now().toISOString(),
    participants: [{ role: "author", user }],
    project,
    summary: "Some summary",
    content: "Some content",
    type: "user-story",
    resolved: false,
  };
  await tasksRepository.save(task);

  // when:
  const foundTask = await tasksService.findTaskById(
    team.id,
    task.id,
    loggedUser
  );

  // then:
  const expectedTask: Task = {
    id: "BLU-1",
    participants: [{ role: "author", user }],
    content: "Some content",
    summary: "Some summary",
    createdAt: "2012-12-12T12:00:00.000Z",
    project,
    type: "user-story",
    resolved: false,
  };
  expect(foundTask).toEqual(expectedTask);
});

test("Test update task", async () => {
  // given:
  freezeTimeAt(new Date("2012-12-12:12:00:00Z"));
  const team: Team = await teamsRepository.save({
    ...someTeam(),
    id: nanoid(),
  });
  const loggedUser: WotrUser = { ...someLoggedUser(), teams: [team.id] };
  const user: User = await usersRepository.save(someUser("john.doe"));

  const project: Project = await projectsRepository.save(someProject(team));

  const task: Task = {
    id: "BLU-1",
    createdAt: time.now().toISOString(),
    participants: [{ role: "author", user }],
    project,
    summary: "Some summary",
    content: "Some content",
    type: "user-story",
    resolved: false,
  };
  await tasksRepository.save(task);

  const updatedJsonTask: TaskInput = {
    content: "Updated content",
    summary: "Updated summary",
    type: "task",
  };

  // when:
  const updatedTask = await tasksService.updateTask(
    team.id,
    task.id,
    updatedJsonTask,
    loggedUser
  );

  // then:
  const expectedTask: Task = {
    ...task,
    type: updatedJsonTask.type,
    content: updatedJsonTask.content,
    summary: updatedJsonTask.summary,
  };
  expect(updatedTask).toEqual(expectedTask);

  const savedTask = await tasksRepository.find(team.id, task.id);
  expect(savedTask).toEqual(expectedTask);
});

afterAll(() => {
  resetTime();
});

function someProject(team: Team): Project {
  return {
    id: "project:BLU:" + nanoid(),
    name: "Project Blu",
    slug: "BLU",
    teamId: team.id,
  };
}

function someTeam(): Team {
  return {
    id: nanoid(),
    name: "Team Black",
  };
}
