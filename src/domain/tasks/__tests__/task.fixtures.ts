import { Task } from "../../../domain/tasks";

export function someTask(teamId: string): Task {
  return {
    id: "BLU-1",
    resolved: false,
    participants: [
      {
        role: "author",
        user: {
          username: "john.doe",
          email: "john.doe@email.test",
          avatarUrl: "http://avatars.com/avatar/john.doe",
        },
      },
    ],
    content: "some content",
    createdAt: "2012-12-12:12:00:00.000Z",
    project: {
      id: "project:id",
      slug: "BLU",
      name: "Project BLU",
      teamId,
    },
    summary: "As a user I want to create task",
    type: "user-story",
  };
}
