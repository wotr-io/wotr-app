import { BoardRepository } from "./BoardRepository";
import { WotrUser } from "../../security/WotrUser";
import {
  BoardsListing,
  JsonBoard,
  BoardDefinition,
  Board,
  JsonTaskTransition,
  BoardColumn,
} from ".";
import { checkPermission } from "../../teams/permissions";
import { TeamRepository } from "../../teams/TeamRepository";
import { EntityNotFoundError } from "../../errors/EntityNotFoundError";
import { TaskRepository } from "../TaskRepository";
import { BoardDefinitionRepository } from "./BoardDefinitionRepository";
import { BusinessBoundaryError } from "../../errors/BusinessBoundaryError";
import { Team } from "../../teams";
import { Task, TaskListingItem } from "..";
import { TasksIndex } from "../TasksIndex";
import nanoid = require("nanoid");

export class BoardService {
  constructor(
    private readonly teamRepository: TeamRepository,
    private readonly boardRepository: BoardRepository,
    private readonly boardDefinitionRepository: BoardDefinitionRepository,
    private readonly tasksRepository: TaskRepository,
    private readonly tasksIndex: TasksIndex
  ) {}

  public async listBoards(
    teamId: string,
    loggedUser: WotrUser
  ): Promise<BoardsListing> {
    checkPermission("board", "list", loggedUser);
    console.log(
      `Listing boards in team: ${teamId} by user: ${loggedUser.username}`
    );

    const items = await this.boardRepository.listByTeamId(teamId);
    return { items };
  }

  public async createBoard(
    teamId: string,
    boardDefinitionId: string,
    jsonBoard: JsonBoard,
    taskIds: string[],
    loggedUser: WotrUser
  ) {
    checkPermission("board", "create", loggedUser);
    checkTasksIds(taskIds);
    console.log(
      `Creating board in team: ${teamId} for definition: ${boardDefinitionId} by user: ${loggedUser.username}`,
      jsonBoard,
      taskIds
    );
    const team = await this.teamRepository.find(teamId);
    const boardDefinition = await this.boardDefinitionRepository.find(
      boardDefinitionId
    );
    checkBoardDefinitionTeam(team, boardDefinition);
    const tasks = await this.tasksRepository.findEntriesByIds(teamId, taskIds);
    checkBoardTasks(tasks, taskIds);

    const board: Board = {
      id: nanoid(),
      name: jsonBoard.name,
      teamId: team.id,
      columns: boardDefinition.statuses.map((status) => ({
        status,
        tasks: status.id === "todo" ? tasks : [],
        version: nanoid(),
      })),
      version: nanoid(),
    };

    return this.boardRepository.save(boardDefinition, board);
  }

  public async readBoard(
    teamId: string,
    boardId: string,
    loggedUser: WotrUser
  ) {
    checkPermission("board", "read", loggedUser);
    console.log(
      `Reading board: ${boardId} in team: ${teamId} by user: ${loggedUser.username}`
    );
    const team = await this.teamRepository.find(teamId);
    const board = await this.boardRepository.find(boardId);
    checkBoardIsOwnedByTeam(team, board);
    return board;
  }

  public async transitionTask(
    teamId: string,
    boardId: string,
    jsonTaskTransition: JsonTaskTransition,
    loggedUser: WotrUser
  ): Promise<JsonTaskTransition> {
    checkPermission("board", "transition-task", loggedUser);
    console.log(
      `Moving task: ${jsonTaskTransition.taskId} in board: ${boardId} in team: ${teamId} by user: ${loggedUser.username}`,
      jsonTaskTransition
    );
    const team = await this.teamRepository.find(teamId);
    const task = await this.tasksRepository.find(
      team.id,
      jsonTaskTransition.taskId
    );
    const board = await this.boardRepository.find(boardId);
    checkBoardIsOwnedByTeam(team, board);

    const sourceColumn = findStatus(board, jsonTaskTransition.sourceStatusId);
    checkTaskPresentInColumn(sourceColumn, task);

    const targetColumn = findStatus(board, jsonTaskTransition.targetStatusId);
    if (sourceColumn === targetColumn) {
      await this.boardRepository.saveBoardColumn(
        board,
        reorderColumnTask(targetColumn, task, jsonTaskTransition.index)
      );
      return jsonTaskTransition;
    }
    await this.boardRepository.saveTaskTransition(board, task.id, [
      removeColumnTask(sourceColumn, task),
      insertColumnTask(targetColumn, task, jsonTaskTransition.index),
    ]);

    if (targetColumn.status.resolving) {
      const resolvedTask: Task = {
        ...task,
        resolved: true,
      };
      await Promise.all([
        this.tasksRepository.markResolved(resolvedTask),
        this.tasksIndex.save(resolvedTask),
      ]);
    }

    return jsonTaskTransition;
  }

  public async deleteBoard(
    teamId: string,
    boardId: string,
    loggedUser: WotrUser
  ) {
    checkPermission("board", "delete", loggedUser);
    console.log(
      `Deleting board: ${boardId} in team: ${teamId} by user: ${loggedUser.username}`
    );
    const team = await this.teamRepository.find(teamId);
    const board = await this.boardRepository.find(boardId);
    checkBoardIsOwnedByTeam(team, board);

    return this.boardRepository.deleteBoard(board.id);
  }
}

function findStatus(board: Board, statusId: string) {
  const foundStatus = board.columns.find(
    (column) => column.status.id === statusId
  );
  if (!foundStatus) {
    throw new BusinessBoundaryError("missing-board-status", [statusId]);
  }
  return foundStatus;
}

function checkBoardIsOwnedByTeam(team: Team, board: Board) {
  if (team.id !== board.teamId) {
    throw new EntityNotFoundError("board", board.id);
  }
}

function checkBoardTasks(tasks: TaskListingItem[], requestedTaskIds: string[]) {
  const foundTaskIds = tasks.map((task) => task.id);
  if (tasks.length !== requestedTaskIds.length) {
    const notFoundIds = requestedTaskIds.filter(
      (taskId) => !foundTaskIds.includes(taskId)
    );
    throw new BusinessBoundaryError("missing-requested-tasks", notFoundIds);
  }
}

function checkBoardDefinitionTeam(
  team: Team,
  boardDefinition: BoardDefinition
) {
  if (team.id !== boardDefinition.teamId) {
    throw new EntityNotFoundError("board-definition", boardDefinition.id);
  }
}

function checkTasksIds(taskIds: string[]) {
  if (taskIds.length === 0) {
    throw new BusinessBoundaryError("missing-tasks-list", []);
  }
  if (taskIds.length > 100) {
    throw new BusinessBoundaryError("exceeded-tasks-limit", [100]);
  }
  const uniqueTaskIds = [...new Set(taskIds)];
  if (taskIds.length !== uniqueTaskIds.length) {
    throw new BusinessBoundaryError("duplicated-tasks", []);
  }
}

function insertColumnTask(
  column: BoardColumn,
  task: Task,
  index: number
): BoardColumn {
  if (index < 0 || index > column.tasks.length) {
    throw new BusinessBoundaryError("illegal-transition-index", [
      index,
      column.tasks.length,
    ]);
  }

  const updatedTasks: TaskListingItem[] = column.tasks
    .slice(0, index)
    .concat([task])
    .concat(column.tasks.slice(index, column.tasks.length));

  return {
    ...column,
    tasks: updatedTasks,
  };
}

function removeColumnTask(column: BoardColumn, task: Task): BoardColumn {
  const updatedTasks = column.tasks.filter(
    (currentTask) => currentTask.id !== task.id
  );
  if (updatedTasks.length === column.tasks.length) {
    throw new BusinessBoundaryError("missing-task-in-column", [
      column.status.id,
      task.id,
    ]);
  }
  return {
    ...column,
    tasks: updatedTasks,
  };
}

function reorderColumnTask(
  column: BoardColumn,
  task: Task,
  targetTaskIndex: number
): BoardColumn {
  if (targetTaskIndex < 0 || targetTaskIndex >= column.tasks.length) {
    throw new BusinessBoundaryError("illegal-transition-index", [
      targetTaskIndex,
      column.tasks.length,
    ]);
  }
  const currentTaskIndex = column.tasks.findIndex(
    (entry) => entry.id === task.id
  );
  if (currentTaskIndex === targetTaskIndex) {
    throw new BusinessBoundaryError("illegal-transition-index", [
      targetTaskIndex,
    ]);
  }
  const updatedTasks = column.tasks.filter(
    (currentTask) => currentTask.id !== task.id
  );

  const reorderedTasks: TaskListingItem[] = updatedTasks
    .slice(0, targetTaskIndex)
    .concat([task])
    .concat(updatedTasks.slice(targetTaskIndex, updatedTasks.length));

  return {
    ...column,
    tasks: reorderedTasks,
  };
}

function checkTaskPresentInColumn(column: BoardColumn, task: Task) {
  const foundTask = column.tasks.find(
    (currentTask) => currentTask.id === task.id
  );
  if (!foundTask) {
    throw new BusinessBoundaryError("missing-task-in-column", [
      column.status.id,
      task.id,
    ]);
  }
}
