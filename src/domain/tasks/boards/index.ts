import { User } from "../../users";
import { Task, TaskListingItem } from "..";

export interface CreateBoardDefinitionRequest {
  name: string;
  statuses: JsonTaskStatusDefinition[];
}

export interface JsonBoard {
  name: string;
}

export interface BoardsListing {
  items: BoardListItem[];
}
export interface BoardDefinitionListing {
  items: BoardDefinition[];
}

export interface CreateBoardRequest {
  name: string;
  taskIds: string[];
}

export interface BoardListItem {
  id: string;
  name: string;
}

export interface Board {
  id: string;
  name: string;
  teamId: string;
  columns: BoardColumn[];
  version: string;
}

export interface BoardColumn {
  status: TaskStatusDefinition;
  tasks: TaskListingItem[];
  version: string;
}

export interface JsonBoardDefinition {
  name: string;
}

export interface BoardDefinition {
  id: string;
  name: string;
  author: User;
  teamId: string;
  statuses: TaskStatusDefinition[];
}

export interface TaskStatusDefinition {
  id: string;
  name: string;
  color: StatusDefinitionColor;
  resolving: boolean;
}

export interface JsonTaskStatusDefinition {
  name: string;
  color: StatusDefinitionColor;
  resolving: boolean;
}

export type StatusDefinitionColor =
  | "grey-1"
  | "green-1"
  | "red-1"
  | "yellow-1"
  | "blue-1";

export interface JsonTaskTransition {
  taskId: string;
  index: number;
  sourceStatusId: string;
  targetStatusId: string;
}
