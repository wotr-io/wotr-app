import { BoardDefinition } from ".";

export interface BoardDefinitionRepository {
  save(item: BoardDefinition): Promise<BoardDefinition>;
  find(id: string): Promise<BoardDefinition>;
  findByTeamId(teamId: string): Promise<BoardDefinition[]>;
}
