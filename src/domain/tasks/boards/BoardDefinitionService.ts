import { BoardDefinitionRepository } from "./BoardDefinitionRepository";
import { TeamRepository } from "../../teams/TeamRepository";
import {
  JsonBoardDefinition,
  BoardDefinition,
  JsonTaskStatusDefinition,
  TaskStatusDefinition,
  BoardDefinitionListing,
} from ".";
import { WotrUser } from "../../security/WotrUser";
import { checkPermission } from "../../teams/permissions";
import nanoid = require("nanoid");
import { UserRepository } from "../../users/UserRepository";
import { BusinessBoundaryError } from "../../errors/BusinessBoundaryError";
import { EntityNotFoundError } from "../../errors/EntityNotFoundError";

export class BoardDefinitionService {
  constructor(
    private readonly teamsRepository: TeamRepository,
    private readonly usersRepository: UserRepository,
    private readonly boardDefinitionRepository: BoardDefinitionRepository
  ) {}

  public async createBoardDefinition(
    teamId: string,
    jsonBoardDefinition: JsonBoardDefinition,
    statuses: JsonTaskStatusDefinition[],
    loggedUser: WotrUser
  ): Promise<BoardDefinition> {
    checkPermission("board-definition", "create", loggedUser);
    const team = await this.teamsRepository.find(teamId);
    const user = await this.usersRepository.find(loggedUser.username);
    checkResolvingStatus(statuses);
    console.log(
      `Creating board definition in team: ${teamId} by: ${user.username}`,
      jsonBoardDefinition
    );

    const boardDefinition: BoardDefinition = {
      id: nanoid(),
      name: jsonBoardDefinition.name,
      author: user,
      teamId: team.id,
      statuses: [todoStatus()].concat(
        statuses.map((status) => toTaskStatusDefinition(status))
      ),
    };

    return this.boardDefinitionRepository.save(boardDefinition);
  }

  public async readBoardDefinition(
    teamId: string,
    definitionId: string,
    loggedUser: WotrUser
  ): Promise<BoardDefinition> {
    checkPermission("board-definition", "read", loggedUser);
    console.log(
      `Reading board definition: ${definitionId} in team: ${teamId} by: ${loggedUser.username}`
    );
    const team = await this.teamsRepository.find(teamId);
    const boardDefinition = await this.boardDefinitionRepository.find(
      definitionId
    );
    if (boardDefinition.teamId !== team.id) {
      throw new EntityNotFoundError("board-definition", definitionId);
    }
    return boardDefinition;
  }

  public async listBoardDefinitions(
    teamId: string,
    loggedUser: WotrUser
  ): Promise<BoardDefinitionListing> {
    checkPermission("board-definition", "list", loggedUser);
    console.log(
      `Listing board definitions in team: ${teamId} by: ${loggedUser.username}`
    );
    const team = await this.teamsRepository.find(teamId);
    const boardDefinitions = await this.boardDefinitionRepository.findByTeamId(
      teamId
    );

    return { items: boardDefinitions };
  }
}

function todoStatus(): TaskStatusDefinition {
  return {
    id: "todo",
    name: "To do",
    resolving: false,
    color: "grey-1",
  };
}

function toTaskStatusDefinition(
  status: JsonTaskStatusDefinition
): TaskStatusDefinition {
  return {
    id: nanoid(),
    name: status.name,
    resolving: status.resolving,
    color: status.color,
  };
}

function checkResolvingStatus(statuses: JsonTaskStatusDefinition[]) {
  if (!statuses.some((status) => status.resolving)) {
    throw new BusinessBoundaryError("resolving-status-missing", []);
  }
}
