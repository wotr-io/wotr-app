import { someTodoStatus, someDoneStatus } from "./board-definition.fixtures";
import { BoardColumn } from "../../../../domain/tasks/boards";

export function someBoard() {
  return {
    id: "some-board-id",
    columns: [someTodoColumn(), someDoneColumn()],
    name: "Board #1",
    teamId: "team-1",
    version: "version-1",
  };
}

export function someDoneColumn(): BoardColumn {
  return {
    status: someDoneStatus(),
    tasks: [],
    version: "version-1",
  };
}

export function someTodoColumn(): BoardColumn {
  return {
    status: someTodoStatus(),
    tasks: [],
    version: "version-1",
  };
}
