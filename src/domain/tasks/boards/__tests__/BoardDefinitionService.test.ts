import { BoardDefinitionService } from "../../../../domain/tasks/boards/BoardDefinitionService";
import { UserRepository } from "../../../../domain/users/UserRepository";
import { TeamRepository } from "../../../../domain/teams/TeamRepository";
import { BoardDefinitionRepository } from "../../../../domain/tasks/boards/BoardDefinitionRepository";
import {
  JsonBoardDefinition,
  JsonTaskStatusDefinition,
  BoardDefinition,
} from "../../../../domain/tasks/boards";
import {
  someDoneStatus,
  someInProgressStatus,
  someBoardDefinition,
} from "./board-definition.fixtures";
import { WotrUser } from "../../../../domain/security/WotrUser";
import { when, anyString, anything } from "ts-mockito";
import { BusinessBoundaryError } from "../../../../domain/errors/BusinessBoundaryError";
import nanoid = require("nanoid");
import { EntityNotFoundError } from "../../../../domain/errors/EntityNotFoundError";
import { DDBTeamRepository } from "../../../../integration/persistence/team/DDBTeamRepository";
import { DDBUserRepository } from "../../../../integration/persistence/user/DDBUserRepository";
import { DDBBoardDefinitionRepository } from "../../../../integration/persistence/task/board/DDBBoardDefinitionRepository";
import { mock, Mock } from "../../../../__tests__/libs/mocks/mocks";
import { someTeam } from "../../../teams/__tests__/team.fixtures";
import {
  someLoggedUser,
  someUser,
} from "../../../users/__tests__/users.fixtures";

let teamsRepository: Mock<TeamRepository>;
let usersRepository: Mock<UserRepository>;
let boardDefinitionRepository: Mock<BoardDefinitionRepository>;

let boardDefinitionService: BoardDefinitionService;

beforeEach(() => {
  teamsRepository = mock(DDBTeamRepository);
  usersRepository = mock(DDBUserRepository);
  boardDefinitionRepository = mock(DDBBoardDefinitionRepository);

  boardDefinitionService = new BoardDefinitionService(
    teamsRepository.instance,
    usersRepository.instance,
    boardDefinitionRepository.instance
  );
});

test("Test save board definition", async () => {
  // given:
  const team = someTeam();
  const loggedUser: WotrUser = { ...someLoggedUser(), teams: [team.id] };
  const user = someUser(loggedUser.username);

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(usersRepository.mock.find(anyString())).thenResolve(user);

  when(boardDefinitionRepository.mock.save(anything())).thenCall(
    identityFunction()
  );

  const jsonBoardDefinition: JsonBoardDefinition = {
    name: "Board definition 1",
  };
  const inProgressStatus: JsonTaskStatusDefinition = someInProgressStatus();
  const doneStatus: JsonTaskStatusDefinition = someDoneStatus();

  // when:
  const boardDefinition = await boardDefinitionService.createBoardDefinition(
    team.id,
    jsonBoardDefinition,
    [inProgressStatus, doneStatus],
    loggedUser
  );

  // then:
  expect([
    boardDefinition.name,
    boardDefinition.author,
    boardDefinition.teamId,
  ]).toEqual([jsonBoardDefinition.name, user, team.id]);
  const createdStatuses = boardDefinition.statuses.map((status) => [
    status.name,
    status.color,
    status.resolving,
  ]);
  expect(createdStatuses).toEqual([
    ["To do", "grey-1", false],
    [inProgressStatus.name, inProgressStatus.color, inProgressStatus.resolving],
    [doneStatus.name, doneStatus.color, doneStatus.resolving],
  ]);
});

test("Test save board definition without resolving status", async () => {
  // given:
  const team = someTeam();
  const loggedUser: WotrUser = { ...someLoggedUser(), teams: [team.id] };
  const user = someUser(loggedUser.username);

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(usersRepository.mock.find(anyString())).thenResolve(user);

  const jsonBoardDefinition: JsonBoardDefinition = {
    name: "Board definition 1",
  };
  const inProgressStatus: JsonTaskStatusDefinition = someInProgressStatus();

  // when:
  const boardDefinitionPromise = boardDefinitionService.createBoardDefinition(
    team.id,
    jsonBoardDefinition,
    [inProgressStatus],
    loggedUser
  );

  // then:
  await expect(boardDefinitionPromise).rejects.toEqual(
    new BusinessBoundaryError("resolving-status-missing", [])
  );
});

test("Test read board definition", async () => {
  // given:
  const team = someTeam();
  const loggedUser: WotrUser = { ...someLoggedUser(), teams: [team.id] };
  const boardDefinition: BoardDefinition = {
    ...someBoardDefinition(),
    teamId: team.id,
  };
  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(boardDefinitionRepository.mock.find(anyString())).thenResolve(
    boardDefinition
  );

  // when:
  const foundBoardDefinition = await boardDefinitionService.readBoardDefinition(
    team.id,
    boardDefinition.id,
    loggedUser
  );

  // then:
  expect(foundBoardDefinition).toEqual(boardDefinition);
});

test("Test read board definition of different team", async () => {
  // given:
  const team = someTeam();
  const loggedUser: WotrUser = { ...someLoggedUser(), teams: [team.id] };
  const boardDefinition: BoardDefinition = {
    ...someBoardDefinition(),
    teamId: nanoid(),
  };
  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(boardDefinitionRepository.mock.find(anyString())).thenResolve(
    boardDefinition
  );

  // when:
  const foundBoardDefinitionPromise =
    boardDefinitionService.readBoardDefinition(
      team.id,
      boardDefinition.id,
      loggedUser
    );

  // then:
  await expect(foundBoardDefinitionPromise).rejects.toEqual(
    new EntityNotFoundError("board-definition", boardDefinition.id)
  );
});

function identityFunction(): (...args: any[]) => any {
  return (arg) => arg;
}
