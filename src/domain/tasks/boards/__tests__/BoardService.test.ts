import { UserRepository } from "../../../../domain/users/UserRepository";
import { TeamRepository } from "../../../../domain/teams/TeamRepository";
import { BoardDefinitionRepository } from "../../../../domain/tasks/boards/BoardDefinitionRepository";
import { Board, BoardColumn } from "../../../../domain/tasks/boards";
import { WotrUser } from "../../../../domain/security/WotrUser";
import { when, anyString, anything, capture, verify } from "ts-mockito";
import { BoardService } from "../../../../domain/tasks/boards/BoardService";
import { BoardRepository } from "../../../../domain/tasks/boards/BoardRepository";
import { TaskRepository } from "../../../../domain/tasks/TaskRepository";
import { Task } from "../../../../domain/tasks";
import { someTodoColumn, someDoneColumn, someBoard } from "./board.fixtures";
import { BusinessBoundaryError } from "../../../../domain/errors/BusinessBoundaryError";
import { TasksIndex } from "../../../../domain/tasks/TasksIndex";
import { DDBTeamRepository } from "../../../../integration/persistence/team/DDBTeamRepository";
import { DDBUserRepository } from "../../../../integration/persistence/user/DDBUserRepository";
import { DDBTaskRepository } from "../../../../integration/persistence/task/DDBTaskRepository";
import { DDBBoardDefinitionRepository } from "../../../../integration/persistence/task/board/DDBBoardDefinitionRepository";
import { DDBBoardRepository } from "../../../../integration/persistence/task/board/DDBBoardRepository";
import { ESTasksIndex } from "../../../../integration/elasticsearch/task/ESTasksIndex";
import { mock, Mock } from "../../../../__tests__/libs/mocks/mocks";
import { someTeam } from "../../../teams/__tests__/team.fixtures";
import { someTask } from "../../__tests__/task.fixtures";
import { someLoggedUser } from "../../../users/__tests__/users.fixtures";

let teamsRepository: Mock<TeamRepository>;
let usersRepository: Mock<UserRepository>;
let boardDefinitionRepository: Mock<BoardDefinitionRepository>;
let boardRepository: Mock<BoardRepository>;
let taskRepository: Mock<TaskRepository>;
let tasksIndex: Mock<TasksIndex>;

let boardService: BoardService;

beforeEach(() => {
  teamsRepository = mock(DDBTeamRepository);
  usersRepository = mock(DDBUserRepository);
  boardDefinitionRepository = mock(DDBBoardDefinitionRepository);
  boardRepository = mock(DDBBoardRepository);
  taskRepository = mock(DDBTaskRepository);
  tasksIndex = mock(ESTasksIndex);

  boardService = new BoardService(
    teamsRepository.instance,
    boardRepository.instance,
    boardDefinitionRepository.instance,
    taskRepository.instance,
    tasksIndex.instance
  );
});

const team = someTeam();
const loggedUser: WotrUser = { ...someLoggedUser(), teams: [team.id] };

const [task1, task2, task3, task4] = someBunchOfTasks(team.id);
const todoColumn: BoardColumn = {
  ...someTodoColumn(),
  tasks: [task1, task2],
};
const doneColumn: BoardColumn = {
  ...someDoneColumn(),
  tasks: [task3, task4],
};
test("Test transition task with index #0", async () => {
  // given:
  const board: Board = {
    ...someBoard(),
    columns: [todoColumn, doneColumn],
    teamId: team.id,
  };

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(taskRepository.mock.find(anyString(), anyString())).thenResolve(task2);
  when(boardRepository.mock.find(anything())).thenResolve(board);

  // when:
  const taskTransition = await boardService.transitionTask(
    team.id,
    board.id,
    {
      index: 0,
      sourceStatusId: todoColumn.status.id,
      targetStatusId: doneColumn.status.id,
      taskId: task2.id,
    },
    loggedUser
  );

  // then:
  expect(taskTransition).toBeDefined();
  const [savedBoard, sourceColumn, targetColumn] = captureLastTaskTransition();
  expect(savedBoard).toEqual(board);

  const expectedSourceColumn: BoardColumn = {
    ...todoColumn,
    tasks: [task1],
  };
  expect(sourceColumn).toEqual(expectedSourceColumn);

  const expectedTargetColumn: BoardColumn = {
    ...doneColumn,
    tasks: [task2, task3, task4],
  };
  expect(targetColumn).toEqual(expectedTargetColumn);
});

test("Test transition task to column with resolving status", async () => {
  // given:
  const board: Board = {
    ...someBoard(),
    columns: [todoColumn, doneColumn],
    teamId: team.id,
  };

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(taskRepository.mock.find(anyString(), anyString())).thenResolve(task2);
  when(boardRepository.mock.find(anything())).thenResolve(board);

  // when:
  const taskTransition = await boardService.transitionTask(
    team.id,
    board.id,
    {
      index: 0,
      sourceStatusId: todoColumn.status.id,
      targetStatusId: doneColumn.status.id,
      taskId: task2.id,
    },
    loggedUser
  );

  // then:
  verify(taskRepository.mock.markResolved(anything())).called();
  verify(tasksIndex.mock.save(anything())).called();
});
test("Test transition task with index #1", async () => {
  // given:
  const board: Board = {
    ...someBoard(),
    columns: [todoColumn, doneColumn],
    teamId: team.id,
  };

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(taskRepository.mock.find(anyString(), anyString())).thenResolve(task2);
  when(boardRepository.mock.find(anything())).thenResolve(board);

  // when:
  const taskTransition = await boardService.transitionTask(
    team.id,
    board.id,
    {
      index: 1,
      sourceStatusId: todoColumn.status.id,
      targetStatusId: doneColumn.status.id,
      taskId: task2.id,
    },
    loggedUser
  );

  // then:
  expect(taskTransition).toBeDefined();
  const [savedBoard, sourceColumn, targetColumn] = captureLastTaskTransition();
  expect(savedBoard).toEqual(board);

  const expectedSourceColumn: BoardColumn = {
    ...todoColumn,
    tasks: [task1],
  };
  expect(sourceColumn).toEqual(expectedSourceColumn);

  const expectedTargetColumn: BoardColumn = {
    ...doneColumn,
    tasks: [task3, task2, task4],
  };
  expect(targetColumn).toEqual(expectedTargetColumn);
});

test("Test transition task in the same column", async () => {
  // given:
  const board: Board = {
    ...someBoard(),
    columns: [todoColumn, doneColumn],
    teamId: team.id,
  };

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(taskRepository.mock.find(anyString(), anyString())).thenResolve(task2);
  when(boardRepository.mock.find(anything())).thenResolve(board);

  // when:
  const taskTransition = await boardService.transitionTask(
    team.id,
    board.id,
    {
      index: 0,
      sourceStatusId: todoColumn.status.id,
      targetStatusId: todoColumn.status.id,
      taskId: task2.id,
    },
    loggedUser
  );

  // then:
  expect(taskTransition).toBeDefined();
  const [savedBoard, targetColumn] = capture(
    boardRepository.mock.saveBoardColumn
  ).first();
  expect(savedBoard).toEqual(board);

  const expectedTargetColumn: BoardColumn = {
    ...todoColumn,
    tasks: [task2, task1],
  };
  expect(targetColumn).toEqual(expectedTargetColumn);
});

test("Test transition task to the same place", async () => {
  // given:
  const board: Board = {
    ...someBoard(),
    columns: [todoColumn, doneColumn],
    teamId: team.id,
  };

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(taskRepository.mock.find(anyString(), anyString())).thenResolve(task2);
  when(boardRepository.mock.find(anything())).thenResolve(board);

  // when:
  const transitionResult = boardService.transitionTask(
    team.id,
    board.id,
    {
      index: 1,
      sourceStatusId: todoColumn.status.id,
      targetStatusId: todoColumn.status.id,
      taskId: task2.id,
    },
    loggedUser
  );

  // then:
  await expect(transitionResult).rejects.toEqual(
    new BusinessBoundaryError("illegal-transition-index", [1])
  );
});

test("Test transition task to the negative index", async () => {
  // given:
  const board: Board = {
    ...someBoard(),
    columns: [todoColumn, doneColumn],
    teamId: team.id,
  };

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(taskRepository.mock.find(anyString(), anyString())).thenResolve(task2);
  when(boardRepository.mock.find(anything())).thenResolve(board);

  // when:
  const transitionResult = boardService.transitionTask(
    team.id,
    board.id,
    {
      index: -1,
      sourceStatusId: todoColumn.status.id,
      targetStatusId: todoColumn.status.id,
      taskId: task2.id,
    },
    loggedUser
  );

  // then:
  await expect(transitionResult).rejects.toEqual(
    new BusinessBoundaryError("illegal-transition-index", [])
  );
});

test("Test transition task outiside of the list", async () => {
  // given:
  const board: Board = {
    ...someBoard(),
    columns: [todoColumn, doneColumn],
    teamId: team.id,
  };

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(taskRepository.mock.find(anyString(), anyString())).thenResolve(task2);
  when(boardRepository.mock.find(anything())).thenResolve(board);

  // when:
  const transitionResult = boardService.transitionTask(
    team.id,
    board.id,
    {
      index: 3,
      sourceStatusId: todoColumn.status.id,
      targetStatusId: todoColumn.status.id,
      taskId: task2.id,
    },
    loggedUser
  );

  // then:
  await expect(transitionResult).rejects.toEqual(
    new BusinessBoundaryError("illegal-transition-index", [])
  );
});

test("Test transition task not present in source column", async () => {
  // given:
  const board: Board = {
    ...someBoard(),
    columns: [todoColumn, doneColumn],
    teamId: team.id,
  };

  const task5: Task = {
    ...someTask(team.id),
    id: "RED-1",
  };

  when(teamsRepository.mock.find(anyString())).thenResolve(team);
  when(taskRepository.mock.find(anyString(), anyString())).thenResolve(task5);
  when(boardRepository.mock.find(anything())).thenResolve(board);

  // when:
  const transitionResult = boardService.transitionTask(
    team.id,
    board.id,
    {
      index: 0,
      sourceStatusId: todoColumn.status.id,
      targetStatusId: todoColumn.status.id,
      taskId: task5.id,
    },
    loggedUser
  );

  // then:
  await expect(transitionResult).rejects.toEqual(
    new BusinessBoundaryError("missing-task-in-column", [])
  );
});

function captureLastTaskTransition(): [Board, BoardColumn, BoardColumn] {
  const [board, , [sourceColumn, targetColumn]] = capture(
    boardRepository.mock.saveTaskTransition
  ).first();
  return [board, sourceColumn, targetColumn];
}

function someBunchOfTasks(teamId: string): [Task, Task, Task, Task] {
  const t1: Task = {
    ...someTask(teamId),
    id: "BLUE-1",
  };
  const t2: Task = {
    ...someTask(teamId),
    id: "BLUE-2",
  };
  const t3: Task = {
    ...someTask(teamId),
    id: "BLUE-2",
  };
  const t4: Task = {
    ...someTask(teamId),
    id: "BLUE-2",
  };
  return [t1, t2, t3, t4];
}
