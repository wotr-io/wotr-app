import {
  TaskStatusDefinition,
  BoardDefinition,
} from "../../../../domain/tasks/boards";
import { someUser } from "../../../users/__tests__/users.fixtures";

export function someBoardDefinition(): BoardDefinition {
  return {
    id: "aQXVQ6ylpiK8MLiqIKMN5",
    name: "Sprint board definition",
    author: someUser("john.doe"),
    statuses: [someInProgressStatus(), someDoneStatus()],
    teamId: "j2bxaiRNBevV4ThL8KWSe",
  };
}

export function someInProgressStatus(): TaskStatusDefinition {
  return {
    id: "in-progress",
    name: "In progress",
    color: "blue-1",
    resolving: false,
  };
}

export function someTodoStatus(): TaskStatusDefinition {
  return {
    id: "todo",
    name: "todo",
    color: "blue-1",
    resolving: false,
  };
}

export function someDoneStatus(): TaskStatusDefinition {
  return {
    id: "done",
    name: "Done",
    color: "green-1",
    resolving: true,
  };
}
