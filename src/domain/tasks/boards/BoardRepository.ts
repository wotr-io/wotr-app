import { BoardDefinition, Board, BoardColumn, BoardListItem } from ".";

export interface BoardRepository {
  save(definition: BoardDefinition, board: Board): Promise<Board>;
  find(boardId: string): Promise<Board>;
  listByTeamId(teamId: string): Promise<BoardListItem[]>;
  saveTaskTransition(
    board: Board,
    taskId: string,
    [sourceColumn, targetColumn]: [BoardColumn, BoardColumn]
  ): Promise<any>;
  saveBoardColumn(board: Board, column: BoardColumn): Promise<BoardColumn>;
  deleteBoard(boardId: string): Promise<any>;
}
