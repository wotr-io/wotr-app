import { Task, TaskListingItem } from ".";
import { Project } from "../projects";

export interface TaskRepository {
  save(item: Task): Promise<Task>;
  markResolved(item: Task): Promise<any>;
  update(item: Task): Promise<Task>;
  find(teamId: string, taskId: string): Promise<Task>;
  tryFind(project: Project, taskId: string): Promise<Task | null>;
  findEntriesByIds(
    teamId: string,
    taskIds: string[]
  ): Promise<TaskListingItem[]>;
}
