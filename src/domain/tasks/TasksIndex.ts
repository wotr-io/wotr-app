import {
  Task,
  ListTasksParameters,
  RecentTasksDashboard,
  TaskListingItem,
} from ".";

export interface TasksIndex {
  save(task: Task): Promise<Task>;
  listTasksByTeamId(teamId: string, [offset, limit]: [number, number]);
  listTasks(
    teamId: string,
    parameters: ListTasksParameters,
    [offset, limit]: [number, number]
  ): Promise<TaskListingItem[]>;
  readRecentTasksDashboard(
    teamId: string,
    now: Date
  ): Promise<RecentTasksDashboard>;
}
