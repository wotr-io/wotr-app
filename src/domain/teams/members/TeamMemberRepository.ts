import { TeamMember } from "./index";

export interface TeamMemberRepository {
  listTeamMembers(teamId: string): Promise<TeamMember[]>;
  save(teamId: string, member: TeamMember): Promise<TeamMember>;
}
