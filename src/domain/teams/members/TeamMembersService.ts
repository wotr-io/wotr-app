import { TeamMemberRepository } from "./TeamMemberRepository";
import { WotrUser } from "../../security/WotrUser";
import { checkPermission } from "../permissions";
import { TeamRepository } from "../TeamRepository";
import { UserRepository } from "../../users/UserRepository";
import { User } from "../../users";

export class TeamMembersService {
  constructor(
    private readonly teamsRepository: TeamRepository,
    private readonly teamMembersRepository: TeamMemberRepository,
    private readonly usersRepository: UserRepository
  ) {}

  public async listTeamMembers(
    teamId: string,
    user: WotrUser
  ): Promise<User[]> {
    checkPermission("team-member", "list", user);
    console.log(
      `Listing team members in team: ${teamId} by user: ${user.username}`
    );
    const team = await this.teamsRepository.find(teamId);
    const teamMembers = await this.teamMembersRepository.listTeamMembers(
      team.id
    );

    const usernames = teamMembers.map((teamMember) => teamMember.username);
    return this.usersRepository.findByIds(usernames);
  }
}
