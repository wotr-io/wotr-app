/**
 * @apiDefine JsonTeam
 * @apiParam (Request Body) {String} name self-explainatory
 */
export interface JsonTeam {
  name: string;
}

/**
 * @apiDefine DetailedJsonTeam
 * @apiSuccess (Response Body) {String} id self-explainatory
 * @apiSuccess (Response Body) {String} name self-explainatory
 * @apiSuccess (Response Body) {String} avatar Team avatar link
 */
export interface DetailedJsonTeam extends JsonTeam {
  id: string;
  avatar: string;
}

export interface Team {
  id: string;
  name: string;
}
