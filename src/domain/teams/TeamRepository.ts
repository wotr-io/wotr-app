import { Team } from ".";

export interface TeamRepository {
  save(item: Team): Promise<Team>;
  find(id: string): Promise<Team>;
  tryFind(id: string): Promise<Team | null>;
  findByIds(ids: string[]): Promise<Team[]>;
  delete(id: string): Promise<void>;
}
