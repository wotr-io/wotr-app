import { anything, capture, when } from "ts-mockito";
import { Mock, mock } from "../../../../__tests__/libs/mocks/mocks";
import { TeamPermissions } from "../../permissions/TeamPermissions";
import { DefaultCognitoClient } from "../../permissions/CognitoClient";
import { TeamRepository } from "../../TeamRepository";
import { Team } from "../..";
import { TeamPermissionRepository } from "../../permissions/TeamPermissionRepository";
import { TeamPermission } from "../../permissions";
import { DDBTeamRepository } from "../../../../integration/persistence/team/DDBTeamRepository";
import { DDBTeamPermissionRepository } from "../../../../integration/persistence/team/permissions/DDBTeamPermissionRepository";

let teamsPermissions: TeamPermissions;

let cognitoClient: Mock<DefaultCognitoClient>;
let teamsRepository: Mock<TeamRepository>;
let teamPermissionsRepository: Mock<TeamPermissionRepository>;

beforeEach(() => {
  cognitoClient = mock(DefaultCognitoClient);
  teamsRepository = mock(DDBTeamRepository);
  teamPermissionsRepository = mock(DDBTeamPermissionRepository);

  teamsPermissions = new TeamPermissions(
    cognitoClient.instance,
    "test-pool-id",
    teamPermissionsRepository.instance
  );
});

test("Test add user to team", async () => {
  // given:
  const teamId = "some-team-id";
  const username = "danny.sullivan";
  const ownerPermission: TeamPermission = {
    action: "*",
    resource: "*",
    teamId,
    username,
  };
  when(cognitoClient.mock.adminGetUser(anything())).thenResolve({
    Username: username,
    UserAttributes: [],
  });

  when(cognitoClient.mock.adminUpdateUserAttributes(anything())).thenResolve(
    {}
  );
  when(teamPermissionsRepository.mock.save(anything())).thenResolve(
    ownerPermission
  );

  // when:
  const result = teamsPermissions.addUserToTeam(username, teamId);

  // then:
  await expect(result).resolves.toBeTruthy();

  const [updateRequest] = capture(
    cognitoClient.mock.adminUpdateUserAttributes
  ).first();
  expect(updateRequest).toMatchObject({
    UserPoolId: "test-pool-id",
    Username: username,
    UserAttributes: [
      {
        Name: "custom:teams",
        Value: teamId,
      },
    ],
  });

  const [savedPermission] = capture(
    teamPermissionsRepository.mock.save
  ).first();
  expect(savedPermission).toMatchObject(ownerPermission);
});

test("Test add already assigned user to another team", async () => {
  // given:
  const teamId = "some-team-id";
  const otherTeamId = "other-team-id";
  const username = "danny.sullivan";

  const ownerPermission: TeamPermission = {
    action: "*",
    resource: "*",
    teamId,
    username,
  };
  when(teamPermissionsRepository.mock.save(anything())).thenResolve(
    ownerPermission
  );

  when(cognitoClient.mock.adminGetUser(anything())).thenResolve({
    Username: username,
    UserAttributes: [
      {
        Name: "custom:teams",
        Value: otherTeamId,
      },
    ],
  });

  when(cognitoClient.mock.adminUpdateUserAttributes(anything())).thenResolve(
    {}
  );

  // when:
  const result = teamsPermissions.addUserToTeam(username, teamId);

  // then:
  await expect(result).resolves.toBeTruthy();

  const [updateRequest] = capture(
    cognitoClient.mock.adminUpdateUserAttributes
  ).first();
  expect(updateRequest).toMatchObject({
    UserPoolId: "test-pool-id",
    Username: username,
    UserAttributes: [
      {
        Name: "custom:teams",
        Value: otherTeamId + "," + teamId,
      },
    ],
  });
});

test("Test positive team permission verification", async () => {
  // given:
  const teamId = "some-team-id";
  const username = "danny.sullivan";
  const ownerPermission: TeamPermission = {
    action: "*",
    resource: "*",
    teamId,
    username,
  };
  when(
    teamPermissionsRepository.mock.findByTeamIdAndUsername(
      anything(),
      anything()
    )
  ).thenResolve([ownerPermission]);

  const team: Team = {
    id: teamId,
    name: "some team",
  };
  when(teamsRepository.mock.find(anything())).thenResolve(team);

  // when:
  const result = await teamsPermissions.hasAnyTeamPermission(teamId, username);

  // then:
  expect(result).toBe(true);
});

test("Test negative team permission verification", async () => {
  // given:
  const teamId = "some-team-id";
  const username = "danny.sullivan";
  when(
    teamPermissionsRepository.mock.findByTeamIdAndUsername(
      anything(),
      anything()
    )
  ).thenResolve([]);

  const team: Team = {
    id: teamId,
    name: "some team",
  };
  when(teamsRepository.mock.find(anything())).thenResolve(team);

  // when:
  const result = await teamsPermissions.hasAnyTeamPermission(teamId, username);

  // then:
  expect(result).toBe(false);
});
