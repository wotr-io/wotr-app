import { anything, capture, verify, when } from "ts-mockito";
import { WotrUser } from "../../../domain/security/WotrUser";
import { DetailedJsonTeam, JsonTeam, Team } from "../../../domain/teams";
import { TeamRepository } from "../../../domain/teams/TeamRepository";
import { TeamsService } from "../../../domain/teams/TeamsService";
import { TeamPermissions } from "../../../domain/teams/permissions/TeamPermissions";
import { TeamPermission } from "../../../domain/teams/permissions";
import { DDBTeamRepository } from "../../../integration/persistence/team/DDBTeamRepository";
import { mock, Mock } from "../../../__tests__/libs/mocks/mocks";

let teamsService: TeamsService;

let teamsRepository: Mock<TeamRepository>;
let teamPermissions: Mock<TeamPermissions>;

beforeEach(() => {
  teamsRepository = mock(DDBTeamRepository);
  teamPermissions = mock(TeamPermissions);
  teamsService = new TeamsService(
    teamsRepository.instance,
    teamPermissions.instance,
    {
      avatarsUrl: "/{0}.svg",
      table: "teams",
    }
  );
});

test("Test list teams", async () => {
  // given:
  const team1: Team = {
    id: "team-1",
    name: "Team 1",
  };
  const team2: Team = {
    id: "team-2",
    name: "Team 2",
  };
  const user = someWotrUser([team1.id, team2.id]);
  when(teamsRepository.mock.findByIds(anything())).thenResolve([team1, team2]);

  // when:
  const teams = await teamsService.listTeams(user);

  // then:
  const item1: DetailedJsonTeam = {
    id: team1.id,
    name: team1.name,
    avatar: "/avatar-00039.svg",
  };
  const item2: DetailedJsonTeam = {
    id: team2.id,
    name: team2.name,
    avatar: "/avatar-00038.svg",
  };
  expect(teams).toEqual([item1, item2]);
});

test("Test save team", async () => {
  // given:
  const payload: JsonTeam = {
    name: "Some Team",
  };
  const user = someWotrUser();
  when(teamsRepository.mock.save(anything())).thenResolve(
    Object.assign({ id: "__team-id__" }, payload, { permissions: [] })
  );
  when(teamPermissions.mock.addUserToTeam(anything(), anything())).thenResolve(
    {}
  );

  // when:
  const createdTeam = await teamsService.createTeam(payload, user);

  // then:
  expect(createdTeam.id).not.toBeNull();
  expect(createdTeam).toMatchObject(payload);

  const [record] = capture(teamsRepository.mock.save).first();
  expect(record).toMatchObject({
    name: payload.name,
  });
});

test("Test save team adds user to the cognito group", async () => {
  // given:
  const payload: JsonTeam = { name: "Some Team" };
  const user = someWotrUser();
  when(teamPermissions.mock.addUserToTeam(anything(), anything())).thenResolve(
    {}
  );

  // when:
  const team = await teamsService.createTeam(payload, user);

  // then:
  const [username, group] = capture(teamPermissions.mock.addUserToTeam).first();
  expect(username).toEqual(user.username);
  expect(group).toEqual(team.id);
});

test("Test failure of adding user to the cognito group", async () => {
  // given:
  const payload: JsonTeam = { name: "Some Team" };
  const user = someWotrUser();
  when(teamPermissions.mock.addUserToTeam(anything(), anything())).thenReject(
    new Error("AWS error")
  );
  expect.hasAssertions();

  // when & then:
  try {
    await teamsService.createTeam(payload, user);
  } catch (err) {
    expect(err).toBeInstanceOf(Error);
    verify(teamsRepository.mock.delete(anything())).once();
  }
});

function someWotrUser(teamIds: string[] = []): WotrUser {
  return {
    sub: "__user-id__",
    username: "danny.sullivan",
    email: "danny.sullivan@wotr.test.io",
    teams: teamIds,
    permissions: teamIds.map(
      (teamId) =>
        ({
          teamId,
          action: "*",
          resource: "*",
          username: "danny.sullivan@wotr.test.io",
        } as TeamPermission)
    ),
  };
}
