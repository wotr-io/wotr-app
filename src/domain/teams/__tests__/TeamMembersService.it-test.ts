import { Team } from "../../../domain/teams";
import { WotrUser } from "../../../domain/security/WotrUser";
import nanoid = require("nanoid");
import { TeamMember } from "../../../domain/teams/members";
import { User } from "../../../domain/users";
import { testContext } from "../../../__tests__/test-context";
import { resetTime } from "../../../libs/time/__tests__/test-time";

const {
  teamsRepository,
  usersRepository,
  teamMembersService,
  teamMembersRepository,
} = testContext;

test("Test list team members", async () => {
  // given:
  const team: Team = someTeam();
  await teamsRepository.save(team);

  const user: WotrUser = someLoggedUser(team);

  const user1 = await usersRepository.save(someUser("john.doe"));
  const user2 = await usersRepository.save(someUser("trevor.noah"));

  const teamMember1: TeamMember = { username: user1.username };
  const teamMember2: TeamMember = { username: user2.username };

  await teamMembersRepository.save(team.id, teamMember1);
  await teamMembersRepository.save(team.id, teamMember2);

  // when:
  const listedTeamMembers = await teamMembersService.listTeamMembers(
    team.id,
    user
  );

  // then:
  expect(listedTeamMembers).toContainEqual({
    avatarUrl: "http://avatars.com/avatar/john.doe",
    email: "john.doe@email.test",
    username: "john.doe",
  });
  expect(listedTeamMembers).toContainEqual({
    avatarUrl: "http://avatars.com/avatar/trevor.noah",
    email: "trevor.noah@email.test",
    username: "trevor.noah",
  });
  expect(listedTeamMembers.length).toBe(2);
});

afterAll(() => {
  resetTime();
});

function someLoggedUser(team: Team): WotrUser {
  return {
    email: "john.doe@email.com",
    permissions: [
      { teamId: team.id, username: "john.doe", action: "*", resource: "*" },
    ],
    sub: nanoid(),
    teams: [team.id],
    username: "john.doe",
  };
}

function someTeam(): Team {
  return {
    id: nanoid(),
    name: "Team Black",
  };
}

function someUser(username: string): User {
  return {
    username,
    email: `${username}@email.test`,
    avatarUrl: "http://avatars.com/avatar/" + username,
  };
}
