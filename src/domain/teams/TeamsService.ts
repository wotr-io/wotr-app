import { WotrUser } from "../security/WotrUser";
import { TeamPermissions } from "./permissions/TeamPermissions";
import { DetailedJsonTeam, JsonTeam, Team } from ".";
import { TeamsProperties } from "./TeamsProperties";
import { TeamRepository } from "./TeamRepository";

import nanoid = require("nanoid");

export class TeamsService {
  constructor(
    private readonly teamsRepository: TeamRepository,
    private readonly teamsPermissions: TeamPermissions,
    private readonly teamsEnvironment: TeamsProperties
  ) {}

  public listTeams(user: WotrUser): Promise<DetailedJsonTeam[]> {
    console.log("Listing teams for user: " + user.username);
    return this.teamsRepository
      .findByIds(user.teams)
      .then((teams) => teams.map((team) => this.asDetailedJsonTeam(team)));
  }

  public async findTeam(
    teamId: string,
    user: WotrUser
  ): Promise<DetailedJsonTeam> {
    console.log("Reading team: " + teamId + " by user: " + user.username);
    const team = await this.teamsRepository.find(teamId);
    return this.asDetailedJsonTeam(team);
  }

  public async createTeam(
    jsonTeam: JsonTeam,
    user: WotrUser
  ): Promise<DetailedJsonTeam> {
    const team: Team = {
      id: nanoid(),
      name: jsonTeam.name,
    };
    console.log(`Creating team: ${team.name} with id: ${team.id}`);
    await this.teamsRepository.save(team);

    console.debug(`Adding user: ${user.username} to team: ${team.id}`);
    return this.teamsPermissions
      .addUserToTeam(user.username, team.id)
      .catch((err) => {
        console.error(
          `Error ocurred when adding user: ${user.username} to team: ${team.id}. Rolling back...`,
          err
        );
        this.teamsRepository
          .delete(team.id)
          .catch((error) =>
            console.error(
              `FATAL. Error on rolling back team: ${team.id} creation.`,
              error
            )
          );
        return Promise.reject(err);
      })
      .then(() => this.asDetailedJsonTeam(team));
  }

  private asDetailedJsonTeam(team: Team): DetailedJsonTeam {
    return {
      id: team.id,
      name: team.name,
      avatar: formatString(
        this.teamsEnvironment.avatarsUrl,
        avatarFilename(team.id)
      ),
    };
  }
}

function avatarFilename(value: string): string {
  const counter = Math.abs(hashcode(value) % 100);
  return `avatar-${counter.toString().padStart(5, "0")}`;
}

function formatString(value: string, ...args: any[]): string {
  let result = value;
  for (const [i, arg] of args.entries()) {
    result = result.replace("{" + i + "}", !!arg ? arg.toString() : "null");
  }
  return result;
}

function hashcode(s: string): number {
  let h: number = 0;
  for (let i = 0; i < s.length; i++) {
    // tslint:disable-next-line: no-bitwise
    h = (Math.imul(31, h) + s.charCodeAt(i)) | 0;
  }
  return h;
}
