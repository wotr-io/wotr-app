import { WotrUser } from "../../security/WotrUser";
import { AuthorizationError } from "../../security/AuthorizationError";

export interface TeamPermission {
  teamId: string;
  username: string;
  resource: string;
  action: string;
}

export function checkPermission(
  resource: string,
  action: string,
  user: WotrUser
): void {
  const matchedPermissions = user.permissions
    .filter(
      (permission) =>
        permission.resource === "*" || permission.resource === resource
    )
    .filter(
      (permission) => permission.action === "*" || permission.action === action
    );

  if (!matchedPermissions.length) {
    throw new AuthorizationError("missing-permission", [
      resource + ":" + action,
    ]);
  }
}
