import { CognitoIdentityServiceProvider } from "aws-sdk";

export interface CognitoClient {
  adminGetUser(
    params: CognitoIdentityServiceProvider.Types.AdminGetUserRequest
  ): Promise<CognitoIdentityServiceProvider.Types.AdminGetUserResponse>;
  adminUpdateUserAttributes(
    params: CognitoIdentityServiceProvider.Types.AdminUpdateUserAttributesRequest
  ): Promise<CognitoIdentityServiceProvider.Types.AdminUpdateUserAttributesResponse>;
}

export class DefaultCognitoClient implements CognitoClient {
  constructor(private readonly cognito: CognitoIdentityServiceProvider) {}

  public adminGetUser(
    params: CognitoIdentityServiceProvider.Types.AdminGetUserRequest
  ): Promise<CognitoIdentityServiceProvider.Types.AdminGetUserResponse> {
    return this.cognito.adminGetUser(params).promise();
  }

  public adminUpdateUserAttributes(
    params: CognitoIdentityServiceProvider.Types.AdminUpdateUserAttributesRequest
  ): Promise<CognitoIdentityServiceProvider.Types.AdminUpdateUserAttributesResponse> {
    return this.cognito.adminUpdateUserAttributes(params).promise();
  }
}
