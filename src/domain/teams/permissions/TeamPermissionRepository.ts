import { TeamPermission } from ".";

export interface TeamPermissionRepository {
  save(item: TeamPermission): Promise<TeamPermission>;
  findByTeamIdAndUsername(
    teamId: string,
    username: string
  ): Promise<TeamPermission[]>;
}
