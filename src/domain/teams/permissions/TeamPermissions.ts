import { AdminGetUserResponse } from "aws-sdk/clients/cognitoidentityserviceprovider";
import { CognitoClient } from "./CognitoClient";
import { TeamPermissionRepository } from "./TeamPermissionRepository";

export class TeamPermissions {
  constructor(
    private readonly cognitoClient: CognitoClient,
    private readonly userPoolId: string,
    private readonly teamPermissionsRepository: TeamPermissionRepository
  ) {}

  public async hasAnyTeamPermission(
    teamId: string,
    username: string
  ): Promise<boolean> {
    const permissions =
      await this.teamPermissionsRepository.findByTeamIdAndUsername(
        teamId,
        username
      );
    return permissions.length > 0;
  }

  public async addUserToTeam(username: string, teamId: string): Promise<any> {
    const user = await this.cognitoClient.adminGetUser({
      UserPoolId: this.userPoolId,
      Username: username,
    });

    const teams = extractTeams(user);
    await this.cognitoClient.adminUpdateUserAttributes({
      UserPoolId: this.userPoolId,
      Username: username,
      UserAttributes: [
        {
          Name: "custom:teams",
          Value: teams.concat([teamId]).join(","),
        },
      ],
    });
    return this.teamPermissionsRepository.save({
      action: "*",
      resource: "*",
      teamId,
      username,
    });
  }
}

function extractTeams(user: AdminGetUserResponse) {
  const teamsAttribute = user.UserAttributes!.find(
    (attribute) => attribute.Name === "custom:teams"
  );
  if (!teamsAttribute) {
    return [];
  }
  return teamsAttribute
    .Value!.split(",")
    .map((team) => team.trim())
    .filter((team) => !!team);
}
