export interface ConfigMap {
  web: WebProperties;
  iam?: IamProperties;
  dynamodb: DDBProperties;
  cognito: CognitoProperties;
  elasticsearch: ElasticSearchProperties;
  teams: TeamsProperties;
  xray: XRayProperties;
}

export interface IamProperties {
  policyArn: string;
}

export interface DDBProperties {
  endpoint?: string;
  tableName: string;
  parentResourceIndex: string;
  targetResourceIndex: string;
  parentTargetIndex: string;
}

export interface CognitoProperties {
  userpoolArn?: string;
  userpoolId: string;
}

export interface ElasticSearchProperties {
  url: string;
  indexName: string;
}

export interface TeamsProperties {
  avatarsUrlTemplate: string;
}

export interface XRayProperties {
  enabled: boolean;
}

export interface WebProperties {
  enabled: boolean;
  jwtIssuerUrl: string;
  jwtAudience: string;
}
