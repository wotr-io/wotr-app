import { ConfigMap } from ".";

export function findConfiguration(stage?: string, ci?: boolean): ConfigMap;
