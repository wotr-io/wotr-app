const stagingCofig = require("./staging.config.json");
const devConfig = require("./dev.config.json");
const ciConfig = require("./ci.config.json");
const testConfig = require("./test.config.json");

/**
 * @returns {import(".").ConfigMap} config map
 */
function findConfiguration(
  stage = process.env.STAGE || "dev",
  ci = process.env.CI
) {
  if (ci === "true") {
    return ciConfig;
  }

  if (stage === "dev") {
    return devConfig;
  }
  if (stage === "staging") {
    return stagingCofig;
  }
  if (stage === "test") {
    return testConfig;
  }
  throw new Error("Unknown stage: " + stage);
}

module.exports = { findConfiguration };
