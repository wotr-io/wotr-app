/**
 * @apiDefine JsonProject
 * @apiParam (Request Body) {String} name self-explainatory
 * @apiParam (Request Body) {String} slug Semantic project identifier.
 */
export interface JsonProject {
  name: string;
  slug: string;
}

/**
 * @apiDefine DetailedJsonProject
 * @apiSuccess (Response Body) {String} id self-explainatory
 * @apiSuccess (Response Body) {String} name self-explainatory
 * @apiSuccess (Response Body) {String} slug Semantic project identifier.
 */
export interface DetailedJsonProject extends JsonProject {
  id: string;
}

/**
 * @apiDefine JsonProjectStatistics
 * @apiSuccess (Response Body) {String} projectId self-explainatory
 * @apiSuccess (Response Body) {Number} totalTasks self-explainatory
 */
export interface JsonProjectStatistics {
  projectId: string;
  totalTasks: number;
}

export interface Project {
  id: string;
  name: string;
  slug: string;
  teamId: string;
}

export interface ProjectStats {
  projectId: string;
  totalTasks: number;
}
