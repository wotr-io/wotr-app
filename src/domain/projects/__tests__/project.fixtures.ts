import { Project } from "../../../domain/projects";

export function someProject(): Project {
  return {
    id: "project:BLU",
    name: "Project Blu",
    slug: "BLU",
    teamId: "j2bxaiRNBevV4ThL8KWSe",
  };
}
