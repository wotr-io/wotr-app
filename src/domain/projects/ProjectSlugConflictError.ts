import { BusinessBoundaryError } from "../errors/BusinessBoundaryError";

export class ProjectSlugConflictError extends BusinessBoundaryError {
  constructor() {
    super("project.non-unique-slug", []);
    Object.setPrototypeOf(this, ProjectSlugConflictError.prototype);
  }
}
