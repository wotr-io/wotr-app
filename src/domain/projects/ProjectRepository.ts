import { Project, ProjectStats } from ".";

export interface ProjectRepository {
  save(item: Project): Promise<Project>;
  find(id: string): Promise<Project>;
  tryFind(id: string): Promise<Project | null>;
  findByIds(ids: string[]): Promise<Project[]>;
  delete(id: string): Promise<void>;
  findBySlug(teamId: string, slug: string): Promise<Project>;
  findStatsByTeamId(teamId: string): Promise<ProjectStats[]>;
  findByTeamId(teamId: string): Promise<Project[]>;
  findStatsByProjectId(projectId: string): Promise<ProjectStats>;
  projectExists(teamId: string, slug: string): Promise<boolean>;
}
