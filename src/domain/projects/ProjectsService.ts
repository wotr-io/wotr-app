import { EntityNotFoundError } from "../errors/EntityNotFoundError";
import { AuthorizationError } from "../security/AuthorizationError";
import { WotrUser } from "../security/WotrUser";
import { TeamRepository } from "../teams/TeamRepository";
import {
  DetailedJsonProject,
  JsonProject,
  Project,
  JsonProjectStatistics,
} from ".";
import { ProjectSlugConflictError } from "./ProjectSlugConflictError";
import nanoid = require("nanoid");
import { TeamPermission } from "../teams/permissions";
import { Team } from "../teams";
import { ProjectRepository } from "./ProjectRepository";

export class ProjectsService {
  constructor(
    private readonly projectsRepository: ProjectRepository,
    private readonly teamsRepository: TeamRepository
  ) {}

  public async listProjects(teamId: string): Promise<DetailedJsonProject[]> {
    const projects = await this.projectsRepository.findByTeamId(teamId);
    return projects.map((project) => asDetailedJsonProject(project));
  }

  public async listProjectStatistics(
    teamId: string
  ): Promise<JsonProjectStatistics[]> {
    return this.projectsRepository.findStatsByTeamId(teamId);
  }

  public async findProject(
    teamId: string,
    projectId: string
  ): Promise<DetailedJsonProject> {
    const team = await this.teamsRepository.find(teamId);
    const project = await this.projectsRepository.find(projectId);

    if (team.id !== project.teamId) {
      throw new EntityNotFoundError("projects", project.id);
    }
    return asDetailedJsonProject(project);
  }

  public async findProjectStats(
    teamId: string,
    projectId: string
  ): Promise<JsonProjectStatistics> {
    const team = await this.teamsRepository.find(teamId);
    const project = await this.projectsRepository.find(projectId);

    if (team.id !== project.teamId) {
      throw new EntityNotFoundError("projects", project.id);
    }

    return this.projectsRepository.findStatsByProjectId(projectId);
  }

  public async saveProject(
    teamId: string,
    jsonProject: JsonProject,
    user: WotrUser
  ): Promise<DetailedJsonProject> {
    const team = await this.teamsRepository.find(teamId);

    if (!findCreateProjectPermission(user)) {
      throw new AuthorizationError("missing-permission", ["project:create"]);
    }

    if (await this.isSlugTaken(team, jsonProject)) {
      throw new ProjectSlugConflictError();
    }

    const project: Project = {
      id: nanoid(),
      name: jsonProject.name,
      slug: jsonProject.slug,
      teamId: team.id,
    };

    const createdProject = await this.projectsRepository.save(project);
    return asDetailedJsonProject(createdProject);
  }

  private isSlugTaken(team: Team, project: JsonProject): Promise<boolean> {
    return this.projectsRepository.projectExists(team.id, project.slug);
  }
}

function findCreateProjectPermission(
  user: WotrUser
): TeamPermission | undefined {
  return user.permissions.find((permission) =>
    isCreateProjectPermission(permission)
  );
}

function asDetailedJsonProject(project: Project): DetailedJsonProject {
  return {
    id: project.id,
    name: project.name,
    slug: project.slug,
  };
}

function isCreateProjectPermission(permission: TeamPermission): boolean {
  return (
    ["project", "*"].includes(permission.resource) &&
    ["create", "*"].includes(permission.action)
  );
}
