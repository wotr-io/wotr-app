import { User } from "../../../domain/users";
import nanoid = require("nanoid");
import { WotrUser } from "../../../domain/security/WotrUser";

export function someLoggedUser(): WotrUser {
  return {
    email: "john.doe@email.test",
    permissions: [
      {
        teamId: "HcIv_xm4skvtU1Rx3JrDO",
        username: "john.doe",
        action: "*",
        resource: "*",
      },
    ],
    sub: nanoid(),
    teams: ["HcIv_xm4skvtU1Rx3JrDO"],
    username: "john.doe",
  };
}

export function someUser(username: string): User {
  return {
    username,
    email: `${username}@email.test`,
    avatarUrl: "http://avatars.com/avatar/" + username,
  };
}
