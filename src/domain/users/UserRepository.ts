import { User } from ".";

export interface UserRepository {
  save(item: User): Promise<User>;
  find(id: string): Promise<User>;
  tryFind(id: string): Promise<User | null>;
  findByIds(ids: string[]): Promise<User[]>;
  delete(id: string): Promise<void>;
}
