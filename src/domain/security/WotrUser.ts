import { TeamPermission } from "../teams/permissions";

export interface WotrUser {
  sub: string;
  username: string;
  email: string;
  teams: string[];
  permissions: TeamPermission[];
}
