export class AuthorizationError extends Error {
  public readonly code: string;
  public readonly permissions: string[];

  constructor(code: string, permissions: string[]) {
    super(
      `Authorization error: ${code}. Missing permissions: [${permissions.join(
        ", "
      )}]`
    );
    this.code = code;
    this.permissions = permissions;
    Object.setPrototypeOf(this, AuthorizationError.prototype);
  }
}
