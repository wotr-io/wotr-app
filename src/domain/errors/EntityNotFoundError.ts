export class EntityNotFoundError extends Error {
  public readonly entity: string;
  public readonly identifier: string;

  constructor(entity: string, identifier: string) {
    super();
    this.entity = entity;
    this.identifier = identifier;
    Object.setPrototypeOf(this, EntityNotFoundError.prototype);
  }
}
