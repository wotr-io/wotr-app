type ErrorDetail = string | number | boolean;

export class BusinessBoundaryError extends Error {
  public readonly code: string;
  public readonly details: ErrorDetail[];

  constructor(code: string, details: ErrorDetail[]) {
    super("Business boundary error: " + code);
    this.code = code;
    this.details = details;
    Object.setPrototypeOf(this, BusinessBoundaryError.prototype);
  }
}
