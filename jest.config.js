module.exports = {
  "testEnvironment": "node",
  "testTimeout": 10000,
  "transform": {
    "^.+\\.ts$": "ts-jest"
  },
  "moduleFileExtensions": [
    "ts",
    "js",
    "json",
    "node"
  ],
  "testRegex": "/__tests__/.*\\.(test|it-test)\\.ts$",
  "coverageDirectory": "coverage",
  "collectCoverageFrom": [
    "src/**/*.{ts,js}",
    "!src/**/*.d.ts"
  ],
  "moduleNameMapper": {
    "^@/(.*)$": "<rootDir>/src/$1"
  }
};