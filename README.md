# Wotr.io Backend application

## Used tools:

1. Serverless Framework
2. TypeScript
3. ajv
4. uuid

## Running autmatic tests:

Run `npm test`.

## Generating documentation

Run `npm generate-docs`.

## Required environment variables

Please provide values for following variables in `.env` file.

| Variable name     | Value source                                                                                                                                          |
| ----------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| WOTR_USER_POOL_ID | Value is Cognito User pool at Wotr aws account                                                                                                        |
| WOTR_AVATARS_URL  | Value is url pattern for building avatars links. Use `{0}` interpolation to put filename (without extension) in the link.                             |
| AUTHORIZER        | Value can be generated with `node dev-resources/authorizer-output.js`. Remember to put generated content within single quotes like `AUTHORIZER='...'` |
