import { findConfiguration } from "./src/domain/env/Environment";
import { buildApplicationContext } from "./src/integration/contexts/context";

const foundConfigMap = findConfiguration("dev");
const applicationContext = buildApplicationContext(foundConfigMap);

applicationContext.server!.listen(8080, () =>
  console.log("Listening at: 8080")
);
