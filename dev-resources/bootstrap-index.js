const { Client } = require("@elastic/elasticsearch");
const chalk = require("chalk");
const { findConfiguration } = require("../src/domain/env/Environment");

const testConfig = findConfiguration();
console.log("Using configuration: " + JSON.stringify(testConfig, undefined, 2));

const client = new Client({
  node: testConfig.elasticsearch.url,
});

const index = testConfig.elasticsearch.indexName;

(async () => {
  await waitForElasticsearch(client);
  await client.indices
    .get({ index })
    .then(() => {
      info(`Index [${index}] already exists.`);
    })
    .catch(() => {
      return client.indices
        .create({ index })
        .then(() => confirm(`Index [${index}] created.`));
    });

  await client.indices
    .putMapping({
      index,
      body: {
        properties: {
          gid: { type: "keyword" },
          resource: { type: "keyword" },
          "task.id": { type: "keyword" },
          "task.type": { type: "keyword" },
          "task.summary": { type: "text" },
          "task.resolved": { type: "boolean" },
          "task.content": { type: "text" },
          "task.participants": {
            type: "nested",
            properties: {
              userGid: { type: "keyword" },
              role: { type: "keyword" },
            },
          },
          "task.fts_content": { type: "text", analyzer: "english" },
          "task.fts_summary": { type: "text", analyzer: "english" },
          "task.createdAt": { type: "date" },
          "task.project.id": { type: "keyword" },
          "task.project.slug": { type: "keyword" },
          "task.team.id": { type: "keyword" },
        },
      },
    })
    .then(() => confirm(`Mappings are present.`));
})().catch((err) => {
  console.log(JSON.stringify(err.body, undefined, 2));
});

function confirm(message) {
  console.log(chalk.green(`  OK. `) + message);
}
function info(message) {
  console.log(chalk.green(`  OK. `) + chalk.grey(message));
}

function waitForElasticsearch() {
  return client.ping({}).then(
    () => confirm("Elasticsearch instance ready."),
    (err) => {
      console.error(err);
      console.log(
        chalk.grey(
          `    Waiting for elasticsearch... Message: ${err.message}, status code: ${err.status}`
        )
      );
      return new Promise((resolve) => setTimeout(resolve, 2000)).then(
        waitForElasticsearch
      );
    }
  );
}
