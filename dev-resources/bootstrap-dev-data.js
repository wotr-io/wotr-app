const AWS = require("aws-sdk");
const elastic = require("@elastic/elasticsearch");
const databaseDump = require("./dumps/wotr-database-dump.json");
const indexDump = require("./dumps/wotr-index-dump.json");
const { findConfiguration } = require("../src/domain/env/Environment");

const testConfig = findConfiguration();

console.log("Using configuration: " + JSON.stringify(testConfig, undefined, 2));

const client = new elastic.Client({
  node: testConfig.elasticsearch.url,
});
const ddb = new AWS.DynamoDB({
  endpoint: testConfig.dynamodb.endpoint,
});

ddb
  .batchWriteItem({
    RequestItems: {
      [testConfig.dynamodb.tableName]: databaseDump.map((item) => ({
        PutRequest: {
          Item: item,
        },
      })),
    },
  })
  .promise()
  .then((dynamoResponse) => {
    console.log(
      `Database restored. Saved records count: ${databaseDump.length}`,
      dynamoResponse
    );

    let promise = Promise.resolve();
    for (const item of indexDump) {
      console.log("Scheduling indexing of document: " + item.gid);
      promise = promise.then(() => {
        client.index({
          index: testConfig.elasticsearch.indexName,
          id: item.gid,
          body: item,
        });
      });
    }
  });
