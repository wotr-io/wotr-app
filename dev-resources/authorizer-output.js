#!/usr/bin/env node
const authorizerOutput = {
  claims: {
    sub: "5211d849-2eb5-480d-b713-237f4ae123ef",
    aud: "6fmjsaerb1vgpkq5ha5gbhn0iu",
    email_verified: "true",
    event_id: "58faa40b-4d64-11e9-ad40-b7195b6124f0",
    token_use: "id",
    auth_time: "1553342899",
    iss: "https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_SnIid0NYp",
    "cognito:username": "kamil.perczynski",
    exp: "Sat Mar 23 13:08:19 UTC 2019",
    iat: "Sat Mar 23 12:08:19 UTC 2019",
    "custom:teams": "_oQkc_ueWxtX4cZgrZhzl,p7AD9Id9OjSr4dQ9AphHN",
    email: "kamil.perczynski.95@gmail.com",
  },
};

console.log(JSON.stringify(authorizerOutput));
