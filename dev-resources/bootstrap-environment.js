const AWS = require("aws-sdk");
const chalk = require("chalk");
const cloudFormationTemplate = require("./cloudformation-stack.json");

const [, , ...programArgs] = process.argv;

const stage = extractStage();
const confirmedUpfront = isConfirmedUpfront();
const stackName = `wotr-${stage}`;

const cloudFormation = new AWS.CloudFormation();

console.log(`Provisioning CloudFormation stack [${stackName}]:`);
function confirm() {
  console.log(`Please confirm provisioning of the stack [${stackName}]? [y/n]`);
  return new Promise((resolve, reject) => {
    if (confirmedUpfront) {
      resolve();
    }

    process.stdin.resume();
    process.stdin.once("data", (data) => {
      const input = data.toString("utf-8").trim();
      if (input === "y") {
        resolve();
      } else {
        process.exit(0);
      }
    });
  }).then(() =>
    console.log(
      chalk.green("  OK. ") +
        `Provisioning of the stack [${stackName}] confirmed.`
    )
  );
}

confirm()
  .then(() => {
    return provideStack(stackName).catch((err) => {
      console.log(JSON.stringify(err));
      console.error(err);
      return Promise.reject(err);
    });
  })
  .then(
    () => process.exit(0),
    () => process.exit(1)
  );

function provideStack(stack) {
  console.log(
    chalk.green("  OK. ") +
      `Verifing CloudFormation stack [${stack}] existence.`
  );
  console.log("Describe stacks");
  return cloudFormation
    .listStacks({
      StackStatusFilter: [
        "CREATE_COMPLETE",
        "UPDATE_COMPLETE",
        "UPDATE_ROLLBACK_COMPLETE",
      ],
    })
    .promise()
    .then((output) => {
      console.log(JSON.stringify(output, undefined, 2));
      const existingStack = output.StackSummaries.find(
        (stackSummary) => stackSummary.StackName === stack
      );
      if (existingStack) {
        console.log(
          chalk.green("  OK. ") +
            `CloudFormation stack [${stack}] exists. Updating...`
        );
        return cloudFormation
          .updateStack({
            StackName: stack,
            TemplateBody: JSON.stringify(cloudFormationTemplate),
            Capabilities: ["CAPABILITY_IAM"],
          })
          .promise()
          .then(
            (stackUpdateOutput) => {
              console.log(JSON.stringify(stackUpdateOutput, undefined, 2));
              return pollStackUntilCompleted(stack, "update");
            },
            (error) => {
              if (
                error.code === "ValidationError" &&
                error.message === "No updates are to be performed."
              ) {
                console.log(
                  chalk.green("  OK. ") +
                    `CloudFormation stack [${stack}] requires no updates.`
                );
                return Promise.resolve();
              }
              return Promise.reject(error);
            }
          );
      }
      console.log(
        chalk.green("  OK. ") +
          `CloudFormation stack [${stack}] needs creation.`
      );
      return cloudFormation
        .createStack({
          StackName: stackName,
          TemplateBody: JSON.stringify(cloudFormationTemplate),
          Capabilities: ["CAPABILITY_IAM"],
        })
        .promise()
        .then((output) => {
          console.log(JSON.stringify(output, undefined, 2));
          return pollStackUntilCompleted(stack, "creation");
        });
    });
}

function pollStackUntilCompleted(stack, action) {
  return new Promise((resolve, reject) => {
    cloudFormation
      .describeStacks({ StackName: stack })
      .promise()
      .then(
        (output) => {
          console.log(JSON.stringify(output, undefined, 2));
          const stackStatus = output.Stacks[0].StackStatus;
          if (!stackStatus || stackStatus.endsWith("_COMPLETE")) {
            console.log(
              chalk.green("  OK. ") +
                `CloudFormation stack [${stack}] ${action} completed: [${stackStatus}]`
            );
            resolve(output);
          } else if (stackStatus.endsWith("_IN_PROGRESS")) {
            console.log(
              chalk.grey(
                `  Stack ${action} is in progress: [${stackStatus}]. Waiting 10 seconds for next check...`
              )
            );
            setTimeout(
              () =>
                pollStackUntilCompleted(stack, action).then(
                  (result) => resolve(result),
                  (error) => reject(error)
                ),
              10000
            );
          } else {
            console.error(
              chalk.red("  ERROR. ") +
                `CloudFormation stack [${stack}] ${action} ended with status [${stackStatus}]`
            );
            reject(stackStatus);
          }
        },
        (error) => {
          console.log(
            "Received error while polling for stack status:",
            JSON.stringify(error, undefined, 2)
          );
          cloudFormation
            .describeStackEvents({ StackName: stack })
            .promise()
            .then((output) =>
              console.log(
                "Listing stack events: ",
                JSON.stringify(output, undefined, 2)
              )
            );
          if (error.retryable) {
            setTimeout(
              () =>
                pollStackUntilCompleted(stack, action).then(
                  (result) => resolve(result),
                  (error) => reject(error)
                ),
              10000
            );
          } else {
            reject(error);
          }
        }
      );
  });
}

function extractStage() {
  if (!process.env.STAGE) {
    throw new Error("Missing environment variable STAGE [dev|staging|test]");
  }
  return process.env.STAGE;
}
function isConfirmedUpfront() {
  return process.env.CONFIRMED === "true";
}
