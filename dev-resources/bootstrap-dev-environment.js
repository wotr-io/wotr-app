const AWS = require("aws-sdk");
const chalk = require("chalk");
const cloudformationTemplate = require("./cloudformation-stack.json");
const { findConfiguration } = require("../src/domain/env/Environment");

const testConfig = findConfiguration();
console.log("Using configuration: " + JSON.stringify(testConfig, undefined, 2));

const dynamoDb = new AWS.DynamoDB({
  endpoint: testConfig.dynamodb.endpoint,
});

const allNeededTables = [testConfig.dynamodb.tableName];

console.log("Provisioning DynamoDb tables:");
findExistingTables().then((existingTables) => {
  let promise = Promise.resolve([]);
  for (const tableName of allNeededTables) {
    if (existingTables.indexOf(tableName) !== -1) {
      console.log(
        chalk.grey(
          `  OK. DynamDB table [${tableName}] already exists. Skipping...`
        )
      );
      continue;
    }
    promise = promise.then((createdTables) =>
      createDatabaseTable(tableName).then(() => {
        console.log(
          chalk.green(`  OK.`) + ` DynamoDB table [${tableName}] was created.`
        );
        return createdTables.concat([tableName]);
      })
    );
  }
  return promise;
});

function findExistingTables() {
  return dynamoDb
    .listTables()
    .promise()
    .then((output) => output.TableNames);
}

function createDatabaseTable(tableName) {
  return dynamoDb
    .createTable({
      ...cloudformationTemplate.Resources.database.Properties,
      TableName: tableName,
    })
    .promise()
    .then(() => console.log("database table created"));
}
